#!/usr/bin/env bash

# Returns the TestRunner and Tags to be used in the subsequent CI Pipeline's mvn test command

# Branch name patterns to identify which TestRunner to use in MR events
PATTERN_SIMPL_OPEN_API="SOAPI"
PATTERN_SIMPL_LABS_UI="SLUI"
PATTERN_SIMPL_LABS_API="SLAPI"
REGEX_TICKET=( SIMPL-[0-9]+ )

TEST_RUNNER=""
TEST_TAGS=""
PROJECT_NAME=""

if [[ "${CI_PIPELINE_SOURCE}" == "merge_request_event" ]]; then
    # Identify Test Runner and Tags to use from branch name for MR events
    branch_name=${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME^^}  # Convert to uppercase
    if [[ "${branch_name}" =~ ${PATTERN_SIMPL_OPEN_API} ]]; then
        TEST_RUNNER="ApiSimplOpenRunner"
        PROJECT_NAME="SIMPL-OPEN"
    elif [[ "${branch_name}" =~ ${PATTERN_SIMPL_LABS_UI} ]]; then
        TEST_RUNNER="UiSimplLabsRunner"
        PROJECT_NAME="SIMPL-LABS"
    elif [[ "${branch_name}" =~ ${PATTERN_SIMPL_LABS_API} ]]; then
        TEST_RUNNER="ApiSimplLabsRunner"
        PROJECT_NAME="SIMPL-LABS"
    else
        TEST_RUNNER="UiSimplOpenRunner"  # Use UiSimplOpenRunner as default for any other branch names
        PROJECT_NAME="SIMPL-OPEN"
    fi

    if [[ "${branch_name}" =~ ${REGEX_TICKET[*]} ]]; then
            ticket=${BASH_REMATCH[0]}
    #       TEST_TAGS="@smoke or @${ticket}"  # If branch name contains ticket info, tag of the same name will also be tested
            TEST_TAGS="@regression"  # Use smoke tag as default for MRs
        else
            TEST_TAGS="@regression"  # Use smoke tag as default for MRs
    fi
else
    # Identify Test Runner and Tags to use for Scheduled Pipeline events
    TEST_RUNNER="${RUNNER_TO_USE}"  # Use TestRunner specified in the Scheduled Pipeline(s) variable
    if [[ -z "${TAGS_TO_USE}" ]]; then
        TEST_TAGS="@smoke or @${ticket}"  # If branch name contains ticket info, tag of the same name will also be tested
#        TEST_TAGS="@regression"  # Use default tag if variable is not defined in the Scheduled Pipeline(s)
    else
        TEST_TAGS="${TAGS_TO_USE}"  # Use Tags specified in the Scheduled Pipeline(s) variable
    fi
fi

# Return the TestRunner and Tags to use
echo "${TEST_RUNNER} ${TEST_TAGS} ${PROJECT_NAME}"