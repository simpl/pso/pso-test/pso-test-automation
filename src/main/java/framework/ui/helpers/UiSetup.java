package framework.ui.helpers;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Browser.NewContextOptions;
import com.microsoft.playwright.options.HttpCredentials;
import framework.common.Config;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import java.awt.Toolkit;
import java.awt.Dimension;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class UiSetup {
    private static final Logger LOGGER = Logger.getLogger(UiSetup.class.getName());
    private static final int DEFAULT_VIEWPORT_WIDTH = 1480;
    private static final int DEFAULT_VIEWPORT_HEIGHT = 920;
    private static Playwright playwright;
    private static Scenario scenario;
    private static Browser browser;
    public static Page page;

    public static Page getPage() {
        return page;
    }

    public static Scenario getScenario() {
        return scenario;
    }

    public static void attachScreenshot() {
        byte[] screenshot = page.screenshot(new Page.ScreenshotOptions());
        scenario.attach(screenshot, "image/png", "Evidence");
    }

    @Before("@ui")
    public static void setUp(Scenario scenario) {
        UiSetup.scenario = scenario;
        playwright = Playwright.create();
        BrowserType chrome = playwright.chromium();
        boolean isHeadless = Boolean.parseBoolean(Config.get("IS_HEADLESS"));
        browser = chrome.launch(new BrowserType.LaunchOptions()
                .setHeadless(isHeadless)
                .setArgs(List.of("--ignore-certificate-errors"))
        );
        page = browser.newContext(new NewContextOptions().setLocale("en-US")).newPage();
        setViewportSize(isHeadless);
    }

    public static Page httpLogIn(String username, String password) {
        HttpCredentials httpCredentials = new HttpCredentials(username, password);
        NewContextOptions contextOptions = new NewContextOptions().setHttpCredentials(httpCredentials);
        BrowserContext browserContext = browser.newContext(contextOptions);
        page = browserContext.newPage();
        boolean isHeadless = Boolean.parseBoolean(Config.get("IS_HEADLESS"));
        setViewportSize(isHeadless);
        return page;
    }

    private static void setViewportSize(boolean isHeadless) {
        if (isHeadless) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            page.setViewportSize(screenSize.width, screenSize.height);
        } else {
            page.setViewportSize(DEFAULT_VIEWPORT_WIDTH, DEFAULT_VIEWPORT_HEIGHT);
        }
    }

    @After(value = "@ui", order = 1)
    public void tearDown() {
        try {
            attachScreenshot();
        } catch (RuntimeException e) {
            LOGGER.log(Level.SEVERE, "Failed to attach screenshot", e);
        } finally {
            if (page != null) {
                page.close();
            }
            if (browser != null) {
                browser.close();
            }
            if (playwright != null) {
                playwright.close();
            }
        }
    }
}
