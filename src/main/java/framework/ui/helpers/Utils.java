package framework.ui.helpers;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.PlaywrightException;
import com.microsoft.playwright.assertions.PlaywrightAssertions;
import com.microsoft.playwright.options.AriaRole;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import framework.common.Assertions;

import java.util.List;

import static configuration.ui.data.simplLabs.LoginConfig.SECRET_KEY;
import static configuration.ui.data.simplOpen.Authority.*;
import static configuration.ui.data.simplOpen.Participant.*;
import static framework.common.Assertions.assertElementIsVisibleByLocator;
import static framework.ui.helpers.UiSetup.page;


public final class Utils {

    private Utils() {
        throw new UnsupportedOperationException(
                "Utils is a utility class and cannot be instantiated");
    }

    public static String getGAuthCode() {
        GoogleAuthenticator gAuth = new GoogleAuthenticator();
        return String.format("%06d", gAuth.getTotpPassword(SECRET_KEY));
    }

    /**
     * Returns a LocatorOptions object with the specified text filter.
     *
     * @param text The text to filter the locator by.
     * @return LocatorOptions with the text filter applied.
     */
    public static Page.LocatorOptions getLocatorOptionsWithText(String text) {
        return new Page.LocatorOptions().setHasText(text);
    }

    /**
     * Returns a Locator object that filters by the specified text.
     *
     * @param page     The Playwright Page object.
     * @param selector The CSS selector to locate the element.
     * @param text     The text to filter the locator by.
     * @return Locator with the text filter applied.
     */
    public static Locator getLocatorWithText(Page page, String selector, String text) {
        return page.locator(selector).filter(new Locator.FilterOptions().setHasText(text));
    }

    /**
     * Clicks a button or any other element identified by the specified selector that contains the given text.
     * <p>
     * This method is particularly useful when there are multiple elements matching the same selector,
     * and you need to interact with the one that has a specific text label.
     * </p>
     *
     * @param page     The Playwright {@link Page} object representing the browser page.
     * @param selector The CSS selector used to locate the target element(s).
     * @param text     The text that the target element should contain. The method will filter elements matching
     *                 the selector to find the one that contains this text.
     * @throws PlaywrightException if no element matching the selector with the specified text is found,
     *                             or if the element is not interactable (e.g., hidden or disabled).
     */
    public static void clickButtonWithText(Page page, String selector, String text) {
        page.locator(selector).filter(new Locator.FilterOptions().setHasText(text)).click();
    }

    /**
     * Utility method to check if an element with a specific selector contains the specified text.
     *
     * @param page     The Playwright {@link Page} object representing the browser page.
     * @param selector The CSS selector used to locate the target element(s).
     * @param text     The text that the target element should contain.
     */
    public static void checkElementHasText(Page page, String selector, String text) {
        PlaywrightAssertions.assertThat(
                page.locator(selector).filter(new Locator.FilterOptions().setHasText(text))
        ).isVisible();
    }

    /**
     * Utility method to check if an input field contains the specified text.
     *
     * @param page         The Playwright {@link Page} object representing the browser page.
     * @param selector     The CSS selector used to locate the input field.
     * @param expectedText The text that should be present in the input field.
     */
    public static void checkInputFieldHasText(Page page, String selector, String expectedText) {
        Locator inputField = page.locator(selector);

        PlaywrightAssertions.assertThat(inputField)
                .hasValue(expectedText);
    }

    /**
     * This method verifies the number of results shown in page are displayed in the locator table
     * It should be equal or less than the number expected
     *
     * @param numberValue: Number of items expected
     */
    public static void verifyTableListDisplayTheResultsExpected(Integer numberValue, Locator locatorValue) {
        List<String> stringsInColumns = locatorValue.allTextContents();
        Assertions.assertElementEqualsOrGreater(stringsInColumns.size(), numberValue);
    }

    /**
     * Method to get credentials for the different keycloak users needed for tests.
     *
     * @param role The role of the user in keycloak.
     * @return LogInDetails
     */
    public static LogInDetails getUserAndPasswordByRole(String role) {
        String user = "";
        String password = "";
        switch (role.toUpperCase()) {
            case "NOTARY":
                user = AUTHORITY_NOTARY_USER;
                password = GENERAL_PASSWORD;
                break;
            case "IATTR_M":
                user = AUTHORITY_IATTR_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "AUTHORITY_T1UAR_M":
                user = AUTHORITY_T1UAR_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "T2IAA_M":
                user = AUTHORITY_T2IAA_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "CONSUMER_T1UAR_M":
                user = CONSUMER_T1UAR_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "CONSUMER_ONBOARDER_M":
                user = CONSUMER_ONBOARDER_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "CONSUMER_SD_CONSUMER":
                user = CONSUMER_SD_CONSUMER_USER;
                password = GENERAL_PASSWORD;
                break;
            case "NO ROLE":
                user = AUTHORITY_NO_ROLES_USER;
                password = GENERAL_PASSWORD;
                break;
            case "CONSUMER NO ROLE":
                user = CONSUMER_NO_ROLES_USER;
                password = GENERAL_PASSWORD;
                break;
            case "DATA PROVIDER NO ROLE":
                user = DATA_PROVIDER_NO_ROLES_USER;
                password = GENERAL_PASSWORD;
                break;
            case "DATA_PROVIDER_T1UAR_M":
                user = DATA_PROVIDER_T1UAR_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "DATA_PROVIDER_ONBOARDER_M":
                user = DATA_PROVIDER_ONBOARDER_M_USER;
                password = GENERAL_PASSWORD;
                break;
            case "DATA_PROVIDER_SD_PUBLISHER":
                user = DATA_PROVIDER_SD_PUBLISHER_USER;
                password = GENERAL_PASSWORD;
                break;
            default:
                throw new IllegalArgumentException("Unknown role: " + role);
        }
        LogInDetails logInDetails = new LogInDetails();
        logInDetails.setUsername(user);
        logInDetails.setPassword(password);
        return logInDetails;
    }

    /**
     * Method to get credentials for the different keycloak users needed for tests.
     *
     * @param user The role of the user in keycloak.
     */
    public static LogInDetails getUserNameAndPassword(String user) {
        String username = "";
        String password = "";
        switch (user.toUpperCase()) {
            case "ONBOARDING_DATAPARTICIPANT_REQUEST_INPROGRESS":
                username = ONBOARDING_DATAPARTICIPANT_REQUEST_INPROGRESS_USER;
                password = ONBOARDING_DATAPARTICIPANT_PASSWORD;
                break;
            case "ONBOARDING_DATAPARTICIPANT_REQUEST_SUBMITTED":
                username = ONBOARDING_DATAPARTICIPANT_REQUEST_SUBMITTED_USER;
                password = ONBOARDING_DATAPARTICIPANT_PASSWORD;
                break;
            case "ONBOARDING_DATAPARTICIPANT_REQUEST_ACCEPTED":
                username = ONBOARDING_DATAPARTICIPANT_REQUEST_ACCEPTED_USER;
                password = ONBOARDING_DATAPARTICIPANT_PASSWORD;
                break;
            case "ONBOARDING_DATAPARTICIPANT_REQUEST_REJECTED":
                username = ONBOARDING_DATAPARTICIPANT_REQUEST_REJECTED_USER;
                password = ONBOARDING_DATAPARTICIPANT_PASSWORD;
                break;
            case "INVALID":
                username = "invalid_username";
                password = "invalid_password";
                break;
            default:
                throw new IllegalArgumentException("Unknown user: " + user);
        }
        LogInDetails logInDetails = new LogInDetails();
        logInDetails.setUsername(username);
        logInDetails.setPassword(password);
        return logInDetails;
    }

    /**
     * POJO class to store users and passwords.
     */
    public static class LogInDetails {
        private static String username = "";
        private static String password = "";

        public void setUsername(String username) {
            LogInDetails.username = username;
        }

        public String getUsername() {
            return username;
        }

        public void setPassword(String password) {
            LogInDetails.password = password;
        }

        public String getPassword() {
            return password;
        }
    }

    public static void clickButtonByLocator(String locator) {
        assertElementIsVisibleByLocator(page.locator(locator));
        page.locator(locator).click();
    }

    public static void clickButtonByAriaText(String text) {
        Locator rejectButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName(text));
        assertElementIsVisibleByLocator(rejectButton);
        rejectButton.click();
    }
}
