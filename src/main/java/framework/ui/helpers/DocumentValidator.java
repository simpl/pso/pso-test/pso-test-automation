package framework.ui.helpers;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class DocumentValidator {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    /**
     * Retrieves the JSON content as a JsonObject from a file.
     *
     * @param filePath The path of the saved file.
     * @return The JsonObject parsed from the file.
     */
    public static JsonObject getJsonObjectFromFile(String filePath) {
        Path path = Paths.get(filePath);
        try (Reader reader = Files.newBufferedReader(path, DEFAULT_CHARSET)) {
            return JsonParser.parseReader(reader).getAsJsonObject();
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load or parse file at path: " + filePath, ex);
        }
    }

    /**
     * Deletes a file if it exists.
     *
     * @param filePath The path of the file to be deleted.
     */
    public static void deleteDownloadedFile(String filePath) {
        Path path = Paths.get(filePath);
        try {
            Files.deleteIfExists(path);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to delete file at path: " + filePath, ex);
        }
    }

    /**
     * Retrieves the value of a specified field from a JsonObject using a JSON path.
     *
     * @param jsonObject The JsonObject to search in.
     * @param jsonPath   The dot-separated path to the desired value.
     * @return The value as a String.
     */
    public static String getJsonValueByPath(JsonObject jsonObject, String jsonPath) {
        String[] pathSegments = jsonPath.split("\\.");
        JsonObject current = jsonObject;

        for (int i = 0; i < pathSegments.length - 1; i++) {
            current = current.getAsJsonObject(pathSegments[i]);
        }

        return current.get(pathSegments[pathSegments.length - 1]).getAsString();
    }

    /**
     * Converts a JSON file into a Map<String, String>.
     *
     * @param filePath The path of the saved JSON file.
     * @return A map representation of the JSON file.
     */
    public static Map<String, String> getMapFromJsonPath(String filePath) {
        JsonObject json = getJsonObjectFromFile(filePath);
        Map<String, String> map = new HashMap<>();
        json.entrySet().forEach(entry -> map.put(entry.getKey(), entry.getValue().getAsString()));
        return map;
    }

    /**
     * Validates that the value of a specific key in a JsonObject matches an expected value.
     *
     * @param key           The key to validate.
     * @param expectedValue The expected value for the key.
     * @param valuePath     The JSON path to the key in the JsonObject.
     * @param jsonObject    The JsonObject to search in.
     */
    public static void validateValueInJson(String key, String expectedValue, String valuePath, JsonObject jsonObject) {
        if (key == null || key.isEmpty()) {
            throw new IllegalArgumentException("Key cannot be null or empty.");
        }
        if (expectedValue == null) {
            throw new IllegalArgumentException("Expected value for key '" + key + "' cannot be null.");
        }
        if (jsonObject == null) {
            throw new IllegalArgumentException("JsonObject cannot be null.");
        }

        String actualValue = getJsonValueByPath(jsonObject, valuePath);
        assertEquals("Value mismatch for property: " + key, expectedValue, actualValue);
    }

    /**
     * This method validates that a JsonObject has a certain not null field inside.
     *
     * @param field      field wanted inside the JsonObject
     * @param jsonObject JsonObject to verify
     */
    public static void validateFieldIsPresentInJson(String field, JsonObject jsonObject) {
        String[] pathSegments = field.split("\\.");
        JsonObject current = jsonObject;

        for (int i = 0; i < pathSegments.length - 1; i++) {
            current = current.getAsJsonObject(pathSegments[i]);
        }

        String innerField = pathSegments[pathSegments.length - 1];

        assertTrue("The field " + innerField + " was not found in the Json", current.has(innerField));
        assertFalse("The field " + innerField + " was not informed in the Json",
                current.get(innerField).isJsonNull());
    }

    /**
     * This method reads a document from a URL and stores it as a temp file. The file does not get deleted.
     *
     * @param documentUrl URL where the document is, e.g., "https://some-url.com/file.pdf"
     * @return path for the temp file
     */
    public static Path readDocumentFromURL(String documentUrl) throws IOException {
        URL url = new URL(documentUrl);
        ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
        Path file = Files.createTempFile("temp", "file");
        FileOutputStream fileOutputStream = new FileOutputStream(file.toFile());
        FileChannel fileChannel = fileOutputStream.getChannel();
        fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        fileOutputStream.close();
        return file;
    }

    /**
     * This method compares an expected hash value for a file from a URL with a calculated one. The calculated hash
     * is encoded with hex (lower case) to make the comparison.
     *
     * @param documentUrl  URL where the document is, e.g., "https://some-url.com/file.pdf"
     * @param algorithm    algorithm used to calculate the hash, e.g., "SHA-256"
     * @param expectedHash expected hash value
     */
    public static void validateDocumentHashFromDocumentURL(String documentUrl, String algorithm, String expectedHash) throws IOException,
            NoSuchAlgorithmException {
        Path file = readDocumentFromURL(documentUrl);
        byte[] data = Files.readAllBytes(file);
        byte[] hash = MessageDigest.getInstance(algorithm).digest(data);
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : hash) {
            stringBuilder.append(String.format("%02x", b));
        }
        String actualHash = stringBuilder.toString();
        deleteDownloadedFile(file.toString());
        assertEquals("Invalid hash for document " + documentUrl, expectedHash, actualHash);
    }

}
