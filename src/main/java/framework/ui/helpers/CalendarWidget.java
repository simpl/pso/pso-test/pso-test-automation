package framework.ui.helpers;

import com.microsoft.playwright.Page;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Locale;

public class CalendarWidget {

    public static final String OPEN_CALENDAR_LABEL = "Open calendar";
    public static final String CHOOSE_MONTH_AND_YEAR_LABEL = "Choose month and year";
    public static final DateTimeFormatter YEAR_FORMAT = DateTimeFormatter.ofPattern("yyyy", Locale.ENGLISH);
    public static final DateTimeFormatter MONTH_FORMAT = DateTimeFormatter.ofPattern("LLLL yyyy", Locale.ENGLISH);
    public static final DateTimeFormatter DAY_FORMAT = DateTimeFormatter.ofPattern("LLLL d, yyyy", Locale.ENGLISH);

    /**
     * This method selects a date from a calendar widget.
     *
     * @param page Playwright page.
     * @param date wished date.
     */
    public static void selectDate(Page page, LocalDate date) {
        page.getByLabel(OPEN_CALENDAR_LABEL).click();
        selectFromYear(page, date);
    }

    /**
     * This method selects a date range from a calendar widget.
     *
     * @param page      Playwright page.
     * @param startDate wished start date.
     * @param endDate   wished end date.
     */
    public static void selectDateRange(Page page, LocalDate startDate, LocalDate endDate) {
        selectDate(page, startDate);
        selectFromYear(page, endDate);
    }

    public static DateTimeFormatter getDateFormat() {
        return new DateTimeFormatterBuilder()
                .appendValue(ChronoField.MONTH_OF_YEAR)
                .appendLiteral('/')
                .appendValue(ChronoField.DAY_OF_MONTH)
                .appendLiteral('/')
                .appendValue(ChronoField.YEAR)
                .toFormatter();
    }

    private static void selectFromYear(Page page, LocalDate date) {
        page.getByLabel(CHOOSE_MONTH_AND_YEAR_LABEL).click();
        page.getByLabel(date.format(YEAR_FORMAT)).click();
        page.getByLabel(date.format(MONTH_FORMAT)).click();
        page.getByLabel(date.format(DAY_FORMAT)).click();
    }
}
