package framework.ui.locators.simplLabs;

public final class ConformanceTest {
    public static final String OPEN_CONFORMANCE_TEST_ARROW = ".mat-column-detailView button";
    public static final String TEST_SUITE_CHECKBOX = ".test-suite-row .mdc-checkbox";
    public static final String START_CONFORMANCE_TEST_BUTTON = "mat-toolbar > button.mat-primary";
    public static final String CONFORMANCE_TEST_APP = "mat-sidenav-content app-conformance-tests";
    public static final String COMPONENTS_CARD_BUTTON = "mat-card:nth-child(1) button";

    private ConformanceTest() {
        throw new UnsupportedOperationException(
                "ConformanceTest is a utility class and cannot be instantiated");
    }
}
