package framework.ui.locators.simplLabs;

public final class DataSpaceConditions {
    public static final String DATASPACE_CONDITIONS_APP = "app-data-space-conditions";
    public static final String NEXT_BUTTON = ".next-button";
    public static final String ACCEPT_ALL_CHECKBOX = ".all-selection mat-checkbox";
    public static final String COMPONENT_CHECKBOX = "mat-accordion mat-checkbox";
    public static final String CREATE_DATA_SPACE_BUTTON = "button.mat-mdc-unelevated-button";

    private DataSpaceConditions() {
        throw new UnsupportedOperationException(
                "DataSpaceConditions is a utility class and cannot be instantiated");
    }
}
