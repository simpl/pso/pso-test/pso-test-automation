package framework.ui.locators.simplLabs;

public final class DataSpaces {
    public static final String STATUS_FILTER = "mat-select[aria-haspopup='listbox']";
    public static final String DATA_SPACE_STATUS_LABEL = ".slabs-chip";
    public static final String INTERACT_BUTTON = "mat-card .mat-mdc-outlined-button";
    public static final String CREATE_DATASPACE_BUTTON = ".mdc-button.mdc-button--unelevated";
    public static final String STATUS_DROPDOWN_ITEM = ".mat-mdc-option  .mdc-list-item__primary-text";
    public static final String LOGOUT_ICON = "[aria-label='Logout icon']";
    public static final String DATA_SPACE_CREATION_LABEL = "mat-dialog-content h2";
    public static final String GO_BACK_BUTTON = "mat-dialog-content button";
    public static final String DATA_SPACE_OPTIONS_BUTTON = "mat-card-title-group button";
    public static final String DELETE_DATA_SPACE_BUTTON = "button.mat-mdc-menu-item";
    public static final String YES_BUTTON = "mat-dialog-actions .mat-primary";
    public static final String COMPONENTS_MENU_ITEM = "mat-list-option:nth-child(4)";
    public static final String CONFORMANCE_MENU_ITEM = "mat-list-option:nth-child(5)";

    private DataSpaces() {
        throw new UnsupportedOperationException(
                "DataSpaces is a utility class and cannot be instantiated");
    }
}
