package framework.ui.locators.simplLabs;

public final class SelectTemplate {
    public static final String SELECT_TEMPLATE_APP = "app-templates";
    public static final String SELECT_TEMPLATE_ICON = ".preview-btn";
    public static final String NEXT_BUTTON = ".next-button";
    public static final String USE_THIS_TEMPLATE_BUTTON = "button.mdc-button--unelevated";

    private SelectTemplate() {
        throw new UnsupportedOperationException(
                "SelectTemplate is a utility class and cannot be instantiated");
    }
}
