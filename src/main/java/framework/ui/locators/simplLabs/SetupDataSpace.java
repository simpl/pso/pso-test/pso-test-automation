package framework.ui.locators.simplLabs;

public final class SetupDataSpace {
    public static final String SETUP_DATASPACE_APP = "app-data-space-setup";
    public static final String ACTIVE_TAB_LABEL = "[role=\"tab\"][aria-selected=\"true\"] .mdc-tab__text-label";
    public static final String NEXT_BUTTON = ".next-button";
    public static final String BACK_BUTTON = "button.mdc-button--outlined";
    public static final String ADD_NODES_BUTTON = ".ds-graph-chart-wrapper mat-toolbar button.mat-primary";
    public static final String AVAILABLE_NODES_ICON = ".ds-nodes-list mat-icon";
    public static final String SELECTED_NODES_ITEM = ".ds-nodes-list .ds-nodes-box.bg-primary-100";
    public static final String ADD_NODES_MODAL_ADD_NODES_BUTTON = "mat-dialog-actions button:nth-child(2)";
    public static final String NODE_ITEM_IN_NODES_LIST = "mat-tree-node";

    private SetupDataSpace() {
        throw new UnsupportedOperationException(
                "SetupDataSpace is a utility class and cannot be instantiated");
    }
}
