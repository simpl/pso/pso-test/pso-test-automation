package framework.ui.locators.simplLabs;

public final class Login {
    public static final String USERNAME = "id=username";
    public static final String PASSWORD = "id=password";
    public static final String LOGIN_BUTTON = "id=kc-login";
    public static final String AUTH_CODE_FIELD = "id=otp";
    public static final String WRONG_CODE_ERROR = "id=input-error-otp-code";

    private Login() {
        throw new UnsupportedOperationException(
                "Login is a utility class and cannot be instantiated");
    }
}
