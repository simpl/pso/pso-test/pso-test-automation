package framework.ui.locators.simplLabs;

public final class DataSpaceDetails {
    public static final String DATASPACE_DETAILS_APP = "app-data-space-details";
    public static final String NEXT_BUTTON = "xpath=//div[contains(@class, 'create-data-space-btns-wrapper') and following-sibling::mat-stepper//app-data-space-details]//button[contains(@class, 'next-button')]";

    private DataSpaceDetails() {
        throw new UnsupportedOperationException(
                "DataSpaceDetails is a utility class and cannot be instantiated");
    }
}
