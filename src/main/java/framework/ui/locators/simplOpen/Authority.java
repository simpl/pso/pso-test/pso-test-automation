package framework.ui.locators.simplOpen;

public class Authority {
    /********************************** Common Locators ************************************************************************/
    public static final String LOGOUT_BUTTON = "//span[contains(text(), 'Logout' )]";
    public static final String CLOSE_ALERT_MESSAGE = "button[aria-label='Close']";
    public static final String KEYCLOAK_USERNAME_INPUT_LOCATOR = "input[id='username']";
    public static final String KEYCLOAK_PASSWORD_INPUT_LOCATOR = "input[id='password']";
    public static final String KEYCLOAK_SIGN_IN_BUTTON_LOCATOR = "input[id='kc-login']";
    public static final String FILTER_APPLIED_VALUE_LOCATOR = "lib-filter > div > span";
    public static final String MULTIPLE_FILTER_APPLIED_VALUE_LOCATOR = "#requestListFilters > div > span";
    /********************************** Onboarding Participant: Form Locators ***************************************************/
    public static final String ONBOARDING_INFO_REGISTER_BUTTON_LOCATOR = "//span[contains(text(), 'Register for this dataspace' )]";
    public static final String ONBOARDING_FORM_LOCATOR = "form[id='applicationForm']";
    public static final String EMAIL_ADDRESS_LOCATOR = "input[id='email']";
    public static final String ORGANISATION_INPUT_LOCATOR = "input[id='organization']";
    public static final String PARTICIPANT_TYPE_DROPDOMN_MENU_LOCATOR = "#mat-mdc-form-field-label-4";
    public static final String PARTICIPANT_TYPE_SELECTED_LOCATOR = "//*[@id=\"mat-select-value-1\"]/span/span";
    public static final String PARTICIPANT_TYPE_OPTIONS_LOCATOR = "[role=option]";
    public static final String PARTICIPANT_TYPE_INPUT_LOCATOR = "#participantType";
    public static final String CREATE_CREDENTIALS_BUTTON_LOCATOR = "//button[.//span[text()='Create credentials']]";
    public static final String SUCCESSFUL_MESSAGE_LOCATOR = "h6";
    public static final String SURNAME_INPUT_LOCATOR = "input[id='surname']";
    public static final String NAME_INPUT_LOCATOR = "input[id='name']";
    public static final String USERNAME_INPUT_LOCATOR = "input[id='username']";
    public static final String PASSWORD_INPUT_LOCATOR = "input[id='password']";
    public static final String CONFIRM_PASSWORD_INPUT_LOCATOR = "label[for='confirmPassword']";
    public static final String USER_GENEREATED_LOCATOR = "div.lead:nth-child(1)";
    public static final String PASSWORD_GENEREATED_LOCATOR = "div.lead:nth-child(2)";
    public static final String SUBMISSION_FORM_BUTTON_LOCATOR = "//button[.//span[text()='Go to the application submission form →']]";
    public static final String SUBMIT_APPLICATION_POPUP_LOCATOR = "//*[@id=\"mat-mdc-dialog-title-0\"]";
    public static final String ACCOUNT_CIRCLE_ICON = "//mat-icon[contains(text(), 'account_circle' )]";
    public static final String ERROR_MESSAGE_LOCATOR = "//span[@id='input-error']";
    public static final String PARTICIPANT_LIST_TABLE_LOCATOR = "table[role='table']";
    public static final String ONBOARDING_STATUS_PANEL_LOCATOR = "//div/app-onboarding-status/div/div";
    public static final String ONBOARDING_REQUESTS_PANEL_LOCATOR = "//div[contains(text(), 'Request submitted' )]";
    public static final String GREEN_ICON_LOCATOR = ".mat-icon:nth-child(1)";
    public static final String GREY_ICON_LOCATOR_2 = "//div[contains(text(), '2' )]";
    public static final String GREY_ICON_LOCATOR_3 = "//div[contains(text(), '3' )]";
    public static final String SEE_REQUEST_DETAILS_BUTTON_LOCATOR = "//span[contains(text(), 'See request details' )]";
    public static final String PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR = "mat-option[role='option']";
    public static final String PARTICIPANT_LIST_FILTER_INPUT_LOCATOR = "input[matinput]";
    public static final String PARTICIPANT_LIST_PARTICIPANT_NAME_COLUMN_LOCATOR = "//td[contains(@class, " +
            "'cdk-column-organization')]";
    public static final String PARTICIPANT_DETAILS_HEADER_LOCATOR = "//h1[contains(text(), 'Participant Details' )]";
    public static final String PARTICIPANT_DETAILS_COLUMNS_LOCATOR = "div[class='col']";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_HEADER_COLUMNS_LOCATOR = "th[role='columnheader']";
    public static final String PARTICIPANT_DETAILS_ATTRIBUTES_TABLE_LOCATOR = "table[class='mat-mdc-table mdc-data-table__table cdk-table mat-sort ng-star-inserted']";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_NAME_COLUMN_LOCATOR = "//td[contains(@class, " +
            "'cdk-column-name')]";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_CODE_COLUMN_LOCATOR = "//td[contains(@class,'cdk-column-code')]";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_LAST_UPDATE_DATE_COLUMN_LOCATOR = "//td[contains" +
            "(@class, 'cdk-column-updateTimestamp')]";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_LOCATOR = "tbody[role='rowgroup']";
    public static final String PARTICIPANT_DETAILS_VALUE_ROW_lOCATOR = " table > tbody >tr > td ";
    public static final String PARTICIPANT_DETAILS_IDENTIFIER_LABEL_LOCATOR = "#label-id";
    public static final String PARTICIPANT_DETAILS_IDENTIFIER_VALUE_LOCATOR = "#value-id";
    public static final String PARTICIPANT_DETAILS_NAME_LABEL_LOCATOR = "#label-participantName";
    public static final String PARTICIPANT_DETAILS_NAME_VALUE_LOCATOR = "#value-participantName";
    public static final String PARTICIPANT_DETAILS_TYPE_LABEL_LOCATOR = "#label-participantType";
    public static final String PARTICIPANT_DETAILS_TYPE_VALUE_LOCATOR = "//div[contains(text(),'GOVERNANCE_AUTHORITY')]";
    public static final String PARTICIPANT_DETAILS_ONBOARDING_DATE_LABEL_LOCATOR = "#label-onboardingDate";
    public static final String PARTICIPANT_DETAILS_ONBOARDING_DATE_VALUE_LOCATOR = "#value-onboardingDate";
    public static final String PARTICIPANT_DETAILS_CREDENTIAL_EXPIRATION_DATE_LABEL_LOCATOR = "#label-expiryDate";
    public static final String PARTICIPANT_DETAILS_CREDENTIAL_EXPIRATION_DATE_VALUE_LOCATOR = "#value-expiryDate";
    public static final String FILTER_FORM_BUTTON_LOCATOR = "span.mdc-button__label";
    public static final String FIRST_FILTER_DROP_DOWN_ARROW_LOCATOR = "#filterColumn > div > " +
            "div.mat-mdc-select-arrow-wrapper";
    public static final String SECOND_FILTER_DROP_DOWN_ARROW_LOCATOR = "#filterStatusSelection > div > " +
            "div.mat-mdc-select-arrow-wrapper";
    public static final String PARTICIPANT_SECOND_FILTER_DROP_DOWN_ARROW_LOCATOR = "#mat-select-value-5";
    public static final String FILTER_TABLE = "tbody.mdc-data-table__content";
    public static final String REQUEST_STATUS_COLUMN_FROM_REQUEST_LIST_TABLE_LOCATOR = "td.cdk-cell.cdk-column-status.mat-column-status";
    public static final String REQUEST_EMAIL_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR = "//td[contains(translate(@class, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', " +
            "'abcdefghijklmnopqrstuvwxyz'), 'email')]";
    public static final String REQUEST_LAST_CHANGE_DATE_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR = "td.mat-mdc-cell.mdc-data-table__cell.cdk-cell" +
            ".cdk-column-updateTimestamp.mat-column-updateTimestamp";
    public static final String REQUEST_DATE_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR = "td.cdk-column-creationTimestamp.mat-column-creationTimestamp";
    public static final String PARTICIPANT_TYPE_COLUMN_FROM_PARTICIPANT_LIST_TABLE_LOCATOR = "td.mat-mdc-cell.mdc-data-table__cell.cdk-cell." +
            "cdk-column-participantType.mat-column-participantType";
    public static final String ONBOARDING_DATE_COLUMN_FROM_PARTICIPANT_LIST_TABLE_LOCATOR = " td.mat-mdc-cell.mdc-data-table__cell.cdk-cell." +
            "cdk-column-updateTimestamp.mat-column-updateTimestamp";
    /********************************** Onboardimg : Request List Locators ***************************************************/
    public static final String REQUEST_LIST_TABLE_CONTENT_PANEL_LOCATOR = "//app-requests/div/div/div/div/div[2]/app-request-list/table/tbody";
    public static final String REQUEST_STATUS_DETAILS_DIALOG_LOCATOR = "//mat-dialog-content[contains(@class, 'mat-mdc-dialog-content')]";
    public static final String REQUEST_DATE_HEADER_FORM_REQUEST_LIST_LOCATOR = "//div[contains(text(), ' Request date ' )]";
    public static final String REQUEST_DATE_HEADER_FORM_LAST_CHANGE_DATE_LOCATOR = "//div[contains(text(), ' Last change date ' )]";
    public static final String REQUEST_DATA_HEADER_FORM_REQUEST_LIST_ARROW_LOCATOR = "//th[contains(@class,'mat-sort-header')]";
    public static final String REQUEST_DATA_HEADER_FORM_LAST_CHANGE_DATE_ARROW_LOCATOR = "//th[contains(@class,'mat-column-up')]";
    public static final String PARTICIPANT_lIST_HEADER_PARTICIPANT_TYPE_LOCATOR = "//div[contains(text(), ' Participant type')]";
    public static final String PARTICIPANT_lIST_HEADER_PARTICIPANT_NAME_LOCATOR = "//div[contains(text(), ' Participant Name')]";
    public static final String PARTICIPANT_lIST_HEADER_ONBOARDING_DATE_LOCATOR = "//div[contains(text(), ' Onboarding Date')]";
    public static final String PARTICIPANT_LIST_SECOND_FILTER_LOCATOR = "#mat-input-0";
    public static final String PARTICIPANT_lIST_HEADER_PARTICIPANT_TYPE_ARROW_LOCATOR = "//th[contains(@class,' mat-column-participantType')]";
    public static final String PARTICIPANT_lIST_HEADER_ONBOARDING_DATE_ARROW_LOCATOR = "//th[contains(@class,'mat-column-up')]";
    public static final String APPROVE_REQUEST_BUTTON_LOCATOR = ".col-lg-2.mx-2.mb-1.btn.btn-primary";
    public static final String REJECT_REQUEST_BUTTON_LOCATOR = ".col-lg-2.mx-2.mb-1.btn.btn-secondary";
    public static final String CONFIRM_DETAILS_DIALOG = ".mdc-dialog__container";
    public static final String TITLE_DIALOG_LOCATOR = "//*[contains(@id,'mat-mdc-dialog-title')]";
    public static final String MESSAGE_WITHIN_APPROVAL_DIALOGUE_LOCATOR = "p[class='ng-star-inserted']";
    public static final String MESSAGE_WITHIN_REJECT_DIALOGUE_LOCATOR = "//app-confirm-dialog/div[1]/p";
    public static final String REJECT_REQUEST_TYPE_REASON_FIELD_LOCATOR = "//app-confirm-dialog/div[1]/mat-form-field/div[1]/div/div[2]/textarea";
    public static final String CHOOSE_FILE_BUTTON_LOCATOR = "input[type='file']";
    /*************************************************** Sap: Identity Attributes Locators ***************************************************/
    public static final String IDENTITY_ATTRIBUTE_NAME_FIELD_LOCATOR = "input[name='name']";
    public static final String IDENTITY_ATTRIBUTE_CODE_FIELD_LOCATOR = "input[name='code']";
    public static final String IDENTITY_ATTRIBUTE_ASSIGNABLE_TO_TIER_1_CHECKBOX_LOCATOR = "//label[contains(text(), 'Assignable to Tier 1' )]/preceding-sibling::input";
    public static final String IDENTITY_ATTRIBUTE_EDITION_ENABLED_CHECKBOX_LOCATOR = "//input[@id=\"input-enabled\"]";
    public static final String IDENTITY_ATTRIBUTE_CREATION_SAVE_BUTTON_LOCATOR = "//span[text()=' Save']";
    public static final String IDENTITY_ATTRIBUTE_CREATION_CANCEL_BUTTON_LOCATOR = "//span[text()=' Cancel']";
    public static final String IDENTITY_ATTRIBUTE_LIST_ASSIGNABLE_TO_ROLE_COLUMN_LOCATOR = ".cdk-column-assignableToRoles [data-testid='text-element']";
    public static final String IDENTITY_ATTRIBUTE_LIST_IN_USE_COLUMN_LOCATOR = ".cdk-column-used [data-testid='text-element']";
    public static final String IDENTITY_ATTRIBUTE_LIST_IDENTIFIER_HEADER_LOCATOR = "//div[contains(text(),'Identifier')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_CODE_HEADER_LOCATOR = "//div[contains(text(),'Code')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_ASSIGNABLE_TO_ROLE_HEADER_LOCATOR = "//div[contains(text(),'Assignable to Role')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_ENABLED_HEADER_LOCATOR = "//div[contains(text(),'Enabled')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_IN_USE_HEADER_LOCATOR = "//div[contains(text(),'In use')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_CREATION_DATE_HEADER_LOCATOR = "//div[contains(text(),'Creation date')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_LAST_CHANGE_DATE_HEADER_LOCATOR = "//div[contains(text(),'Last change date')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_ACTIONS_HEADER_LOCATOR = "//div[contains(text(),'Actions')]";
    public static final String IDENTITY_ATTRIBUTE_LIST_DELETE_COLUMN_LOCATOR = "td > div > button > mat-icon";
    public static final String IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_LOCATOR = "[role=dialog]";
    public static final String IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_TITLE_LOCATOR = "h2[mat-dialog-title]";
    public static final String IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_MESSAGE_LOCATOR = "mat-dialog-content p";
    public static final String IDENTITY_ATTRIBUTE_REMOVE_BUTTON_LOCATOR = "//mat-icon[normalize-space()='delete']";
    public static final String IDENTITY_ATTRIBUTE_DELETE_BUTTON_LOCATOR = "//span[normalize-space()='Delete']";
    public static final String IDENTITY_ATTRIBUTE_DELETE_CANCEL_BUTTON_LOCATOR = "//span[normalize-space()='Cancel']";
    public static final String EMPTY_LIST_MESSAGE_LOCATOR = "div.p-5.text-center";
    public static final String LIST_IDENTITY_ATTRIBUTE_ID_LOCATOR = "//td[@class='mat-mdc-cell mdc-data-table__cell cdk-cell cdk-column-id mat-column-id ng-star-inserted']";
    public static final String IDENTITY_ATTRIBUTE_EDIT_BUTTON_LOCATOR = "//span[normalize-space()='Edit Attribute']";
    public static final String IDENTITY_ATTRIBUTE_DETAILS_COLUMNS_LOCATOR = "div[class='col']";
    public static final String NEW_ATTRIBUTE_BUTTON_LOCATOR = "//span[normalize-space()='New Attribute']";
    public static final String ITEMS_PER_PAGE_DROPDOWN_LOCATOR = "#mat-select-2 > div > div.mat-mdc-select-arrow-wrapper > div";
    public static final String ITEMS_PER_PAGE_TEXT_FIELD_LOCATOR = ".mat-mdc-paginator-range-label";
    public static final String NEXT_PAGE_BUTTON_LOCATOR = "button[aria-label='Next page'] span[class='mat-mdc-button-touch-target']";
    public static final String PREVIOUS_PAGE_BUTTON_LOCATOR = "button[aria-label='Previous page'] span[class='mat-mdc-button-touch-target']";
    public static final String COLUMN_HEADING_LOCATOR = "thead th div.mat-sort-header-content";
    public static final String PARTICIPANT_LIST_LOCATOR = "td.mat-mdc-cell.mdc-data-table__cell.cdk-cell.cdk-column-organization.mat-column-organization.ng-star-inserted";
    public static final String REQUEST_STATUS_LOCATOR = "#statusFieldBadge";
    public static final String IDENTITY_ATTRIBUTES_LIST_LOCATOR = "//td[contains(@class, 'mat-mdc-cell mdc-data-table__cell cdk-cell cdk-column-id mat-column-id ng-star-inserted')]/span[@data-testid='text-element']";

    public Authority() {

    }
}
