package framework.ui.locators.simplOpen;

public final class Wizard {
    public static final String GET_STARTED_BUTTON = "Get Started";
    public static final String SELECT_SHACL_TEXT = " Select a SHACL shape or ";
    public static final String UPLOAD_TEXT = "a[style='cursor: pointer;']:has-text('Upload')";
    public static final String UPLOAD_BUTTON_WIDGET = "//button[@class='uploadbutton']";
    public static final String WIDGET_CLASS = ".mat-card.mat-focus-indicator.info-message";
    public static final String CHOOSE_FILE_BUTTON = "input[type='file']";
    public static final String FILE_NAME_DISPLAYED = "//input[contains(@class, 'ng-valid')] ";
    public static final String UPLOAD_BUTTON = "//button[contains(@class, 'upload-button')]";
    public static final String COMPLETE_PERSON_FORM = "//b[contains(text(), 'Person')]";
    public static final String AGE_FIELD = "//input[@id='mat-input-0']";
    public static final String NAME_FIELD = "//input[@id='mat-input-1']";
    public static final String GENDER_FIELD = "//span[@class='mat-select-placeholder mat-select-min-line "
            + "ng-tns-c73-3 ng-star-inserted']";
    public static final String MALE_OPTION = "//span[@class='mat-option-text'][normalize-space()='male']";
    public static final String BIRTH_DATE_FIELD = "//input[@id='mat-input-2']";
    public static final String SAVE_BUTTON = "//span[contains(text(), 'Save')]";
    public static final String SELECT_ECOSYSTEM_DROPDOWN_MENU = "//button[@id='dropbtn']";
    public static final String DROPDOWN_SIMPL_OPTION = "//a[.=\" SIMPL \"]";
    public static final String PARTICIPANT_SHAPES_SECTION = "//span[normalize-space()='Participant shapes']";
    public static final String SERVICE_OFFERING_SHAPES_SECTION = "//span[normalize-space()='Service Offering shapes']";
    public static final String RESOURCES_SHAPES_SECTION = "//span[normalize-space()='Resource shapes']";
    public static final String DATA_OFFERING_SHAPE = "//span[normalize-space()='Data Offering']";
    public static final String APPLICATION_OFFERING_SHAPE = "//span[normalize-space()='Application Offering']";
    public static final String ECOSYSTEM_DROPDOWN_CONTENTS = "div[class='dropdown-content']";
    public static final String SELECT_CONTRACT_DROPDOWN_OPTIONS = "//*[@id=\"mat-select-6-panel\"]";
    public static final String BILLING_SCHEMA_DROPDOWN_OPTIONS = "//*[@id=\"mat-select-8-panel\"]";
    public static final String SLA_AGREEMENTS_DROPDOWN_OPTIONS = "//*[@id=\"mat-select-10-panel\"]";

    private Wizard() {
        throw new UnsupportedOperationException(
                "Wizard is a utility class and cannot be instantiated");
    }
}
