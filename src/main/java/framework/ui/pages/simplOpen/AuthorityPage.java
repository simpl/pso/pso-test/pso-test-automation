package framework.ui.pages.simplOpen;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import framework.common.Assertions;
import framework.ui.helpers.UiSetup;
import framework.ui.helpers.Utils;
import io.cucumber.datatable.DataTable;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static configuration.ui.data.simplOpen.Authority.*;
import static framework.common.Assertions.*;
import static framework.ui.locators.simplOpen.Authority.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AuthorityPage {
    private final Page page = UiSetup.getPage();

    /***
     * This method selects a value from the second filter.
     * For example in "Dashboard Requests List" : There are 4 options: In progress|Approved|Rejected|Inserted
     * The status to be selected is sending in the step
     *
     * @param filter
     */
    public void verifyUserSelectOptionFromSecondFilter(String filter, Locator locatorValue, String list) {
        Locator dropdownArrow = switch (list) {
            case "Request list" -> page.locator(SECOND_FILTER_DROP_DOWN_ARROW_LOCATOR);
            case "Participant list" -> page.locator(PARTICIPANT_SECOND_FILTER_DROP_DOWN_ARROW_LOCATOR);
            default -> throw new IllegalArgumentException("Invalid list type: " + list);
        };

        dropdownArrow.click();
        page.waitForTimeout(500);

        int countElements = locatorValue.count();
        for (int i = 0; i < countElements; i++) {
            assertElementIsVisibleByLocator(locatorValue.nth(i));
            String valueElement = locatorValue.nth(i).innerText();
            if (valueElement != null && !valueElement.equals("")) {
                if (valueElement.equals(filter)) {
                    locatorValue.nth(i).click();
                    break;
                }
            } else {
                throw new IllegalArgumentException("The ".concat(filter).concat(" element does not exist"));
            }
        }
    }

    /***
     * This method selects the status to press within the first filter found in "Dashboard Requests List"
     * Search filter by value
     * The status to be selected is sending in the step
     *
     * @param statusReq  There are two options: status|email
     */
    public void verifyUserSelectOptionFromFirstFilter(String statusReq, Locator locatorValue) {
        assertElementIsVisibleByLocator(page.locator(FIRST_FILTER_DROP_DOWN_ARROW_LOCATOR));
        page.locator(FIRST_FILTER_DROP_DOWN_ARROW_LOCATOR).click();
        int countElements = locatorValue.count();
        for (int i = 0; i < countElements; i++) {
            String valueElement = locatorValue.nth(i).getAttribute("value");
            if (valueElement.equals(statusReq)) {
                locatorValue.nth(i).click();
                break;
            }
        }
    }

    /**
     * This method verifies that all values in the specified column match the expected value.
     *
     * @param expectedValue The value to verify in the table (e.g., email or status).
     * @param locatorValue  The locator for the column containing the values.
     * @param valueType     Specifies the type of value ("Status" or "Email").
     */
    public void verifyInRequestTableListDisplayTheFiltersValueSelected(String valueType, String expectedValue, Locator locatorValue) {
        itemsPerPageSelectionInTableDropdown(100);
        assertElementIsVisibleByLocator(page.locator(REQUEST_LIST_TABLE_CONTENT_PANEL_LOCATOR));

        String formattedExpectedValue = valueType.equalsIgnoreCase("status")
                ? expectedValue.toUpperCase()
                : expectedValue;

        do {
            List<String> stringsInColumns = locatorValue.allTextContents();
            for (String actualValue : stringsInColumns) {
                if (!actualValue.trim().equals(formattedExpectedValue)) {
                    throw new AssertionError(
                            String.format("Values mismatch: expected '%s', but found '%s'.", formattedExpectedValue, actualValue.trim())
                    );
                }
            }
            clickNextOrPreviousPageButton("Next page", "request list", true);
        } while (page.locator(NEXT_PAGE_BUTTON_LOCATOR).isEnabled());
    }


    /**
     * This method selects which user should access the onboarding status page, depending on their status
     *
     * @param isAcceptedUser : If user has been already accepted
     * @param isPDFSubmitted : If user has uploaded onboarding documentation after request
     */
    public String determineApplicantUser(boolean isPDFSubmitted, boolean isAcceptedUser, boolean isRejectedUser) {
        return !isPDFSubmitted
                ? "ONBOARDING_DATAPARTICIPANT_REQUEST_SUBMITTED"
                : isAcceptedUser
                ? "ONBOARDING_DATAPARTICIPANT_REQUEST_ACCEPTED"
                : isRejectedUser
                ? "ONBOARDING_DATAPARTICIPANT_REQUEST_REJECTED"
                : "ONBOARDING_DATAPARTICIPANT_REQUEST_INPROGRESS";
    }

    /**
     * This method checks if the button "" is visible and enabled or not displayed at all.
     *
     * @param isTheElementVisible
     */
    public void verifyDownloadCredentialsButtonIsVisibleOrNot(String isTheElementVisible) {
        Locator downloadButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("download"));
        switch (isTheElementVisible) {
            case "is visible":
                assertElementIsVisibleByLocator(downloadButton);
                assertThat(downloadButton).isEnabled();
                break;
            case "is not visible":
                assertElementIsNotVisibleByLocator(downloadButton);
                break;
        }
    }

    /**
     * This method fills the form for creating or editing an Identity Attribute.
     *
     * @param name       name of the identity attribute.
     * @param code       code of the identity attribute.
     * @param assignable whether the identity attribute will be assigned to Tier 1.
     */
    public void fillIdentityAttributeForm(String name, String code, boolean assignable) {
        page.locator(IDENTITY_ATTRIBUTE_NAME_FIELD_LOCATOR).fill(name);
        page.locator(IDENTITY_ATTRIBUTE_CODE_FIELD_LOCATOR).fill(code);

        Locator assignableCheckbox = UiSetup.page.locator(IDENTITY_ATTRIBUTE_ASSIGNABLE_TO_TIER_1_CHECKBOX_LOCATOR);

        if (assignableCheckbox.isChecked() != assignable) {
            assignableCheckbox.click();
        }
    }

    /***
     * This class represents a model for managing Identity Attributes methods.
     * It provides methods to set and retrieve the code, name, and Tier 1 assignability of an identity attribute.
     */
    public static class IdentityAttribute {
        private String code;
        private String name;
        private Boolean assignableToTier1;

        public IdentityAttribute setCode(String code) {
            this.code = code;
            return this;
        }

        public String getCode() {
            return code;
        }

        public IdentityAttribute setName(String name) {
            this.name = name;
            return this;
        }

        public String getName() {
            return name;
        }

        public IdentityAttribute setAssignableToTier1(Boolean assignableToTier1) {
            this.assignableToTier1 = assignableToTier1;
            return this;
        }

        public Boolean getAssignableToTier1() {
            return assignableToTier1;
        }
    }

    /***
     * The method sorts the values of a particular column from a table in ascending or descending.
     *
     * @param sortDirection   Attribute name, to select ascending or descending sort direction
     * @param columnToSort  Attribute name, to select the column to sort
     */
    public void sortsTheValuesOfAColumnTable(String sortDirection, String columnToSort) {
        switch (columnToSort) {
            case "Request Date":
                selectAscendOrDescend(sortDirection, page.locator(REQUEST_DATE_HEADER_FORM_REQUEST_LIST_LOCATOR),
                        page.locator(REQUEST_DATA_HEADER_FORM_REQUEST_LIST_ARROW_LOCATOR).first());
                break;
            case "Last change date":
                selectAscendOrDescend(sortDirection, page.locator(REQUEST_DATE_HEADER_FORM_LAST_CHANGE_DATE_LOCATOR),
                        page.locator(REQUEST_DATA_HEADER_FORM_LAST_CHANGE_DATE_ARROW_LOCATOR));
                break;
            case "Participant Type":
                selectAscendOrDescend(sortDirection, page.locator(PARTICIPANT_lIST_HEADER_PARTICIPANT_TYPE_LOCATOR),
                        page.locator(PARTICIPANT_lIST_HEADER_PARTICIPANT_TYPE_ARROW_LOCATOR));
                break;
            case "Onboarding Date":
                selectAscendOrDescend(sortDirection, page.locator(PARTICIPANT_lIST_HEADER_ONBOARDING_DATE_LOCATOR),
                        page.locator(PARTICIPANT_lIST_HEADER_ONBOARDING_DATE_ARROW_LOCATOR));
                break;
            default:
                throw new IllegalArgumentException("Unknown column to be sorted: ".concat(columnToSort));
        }
    }

    /**
     * This method verifies the column does not contain any sort filter applied at the beginning
     * Also, this method selects whether to sort ascending or descending.
     * This method is called by sortsTheValuesOfAColumnTable()
     *
     * @param sortDirection      Attribute name, to select ascending or descending sort direction
     * @param columnLocatorValue Attribute name, to identify the locator of the column to sort
     * @param arrowLocatorValue  Attribute name, to identify the locator of the arrow of the column to sort
     */
    public void selectAscendOrDescend(String sortDirection, Locator columnLocatorValue, Locator arrowLocatorValue) {
        String sortOptionSelected = arrowLocatorValue.getAttribute("aria-sort");
        if ("none".equals(sortOptionSelected.trim())) {
            if ("ascending".equals(sortDirection.trim())) {
                sortAscending(columnLocatorValue, arrowLocatorValue);
            } else if ("descending".equals(sortDirection.trim())) {
                sortDescending(columnLocatorValue, arrowLocatorValue);
            } else {
                throw new IllegalArgumentException("Unknown sort direction: ".concat(sortDirection));
            }
        }
    }

    /***
     *  This method verifies the elements belonging to the pagination are visible.
     */
    public void verifyThePaginationElementsAreVisible() {
        assertElementIsVisibleByLocator(page.locator(ITEMS_PER_PAGE_DROPDOWN_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(ITEMS_PER_PAGE_TEXT_FIELD_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(NEXT_PAGE_BUTTON_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(PREVIOUS_PAGE_BUTTON_LOCATOR));
    }

    /***
     * This method selects the number of values displayed per page
     *
     * @param numberValueByPage
     */
    public void itemsPerPageSelectionInTableDropdown(Integer numberValueByPage) {
        Locator numberOfResultsLocator = page.locator(ITEMS_PER_PAGE_DROPDOWN_LOCATOR);
        assertElementIsVisibleByLocator(numberOfResultsLocator);
        numberOfResultsLocator.click();
        int countElements = page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR).count();
        for (int i = 0; i < countElements; i++) {
            String valueElement = page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR).nth(i).innerText();
            if (valueElement.equals(numberValueByPage.toString())) {
                page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR).nth(i).click();
                break;
            }
        }
    }

    /**
     * This method verifies the number of results shown in the specified table.
     * It should be equal to or less than the number selected in "Items per page".
     *
     * @param listType          The type of list to verify (e.g., "Dashboard Requests List", "Participant List").
     * @param numberValueByPage Number of items expected.
     */
    public void verifyNumberOfResultsDisplayedInTheList(String listType, Integer numberValueByPage) {
        Locator locatorValue = switch (listType) {
            case "Dashboard Requests List" -> page.locator(REQUEST_STATUS_COLUMN_FROM_REQUEST_LIST_TABLE_LOCATOR);
            case "Participant List" -> page.locator(PARTICIPANT_LIST_LOCATOR);
            case "Identity Attributes List" -> page.locator(IDENTITY_ATTRIBUTES_LIST_LOCATOR);
            default -> throw new IllegalArgumentException("Unknown list type: " + listType);
        };
        Utils.verifyTableListDisplayTheResultsExpected(numberValueByPage, locatorValue);
    }

    /***
     * This method orders a certain column in ascending order
     *
     * @param columnLocatorValue
     * @param arrowLocatorValue
     */
    private void sortAscending(Locator columnLocatorValue, Locator arrowLocatorValue) {
        columnLocatorValue.click();
        String sortOptionSelected = arrowLocatorValue.getAttribute("aria-sort");
        if (!"ascending".equals(sortOptionSelected.trim())) {
            throw new IllegalArgumentException("The column is not sorted in ascending order.");
        }
    }

    /***
     * This method orders a certain column in descending order
     *
     * @param columnLocatorValue
     * @param arrowLocatorValue
     */
    private void sortDescending(Locator columnLocatorValue, Locator arrowLocatorValue) {
        columnLocatorValue.click();
        columnLocatorValue.click();
        String sortOptionSelected = arrowLocatorValue.getAttribute("aria-sort");
        if (!"descending".equals(sortOptionSelected.trim())) {
            throw new IllegalArgumentException("The column is not sorted in descending order.");
        }
    }

    /***
     * This method checks if the displayed table list was sorted according to the column selected.
     * This method calls compareTwoListsAreEquals
     *
     * @param columnToSort
     */
    public void verifyColumnSorting(String sortDirection, String columnToSort) {
        Map<String, Locator> columnLocators = Map.of(
                "Last change date", page.locator(REQUEST_LAST_CHANGE_DATE_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR),
                "Request Date", page.locator(REQUEST_DATE_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR),
                "Participant Type", page.locator(PARTICIPANT_TYPE_COLUMN_FROM_PARTICIPANT_LIST_TABLE_LOCATOR),
                "Onboarding Date", page.locator(ONBOARDING_DATE_COLUMN_FROM_PARTICIPANT_LIST_TABLE_LOCATOR)
        );

        Locator columnLocator = columnLocators.get(columnToSort);

        if (columnLocator == null) {
            throw new IllegalArgumentException("Unknown column to verify: " + columnToSort);
        }

        verifyColumnSortingByLocator(sortDirection, columnLocator);
    }

    /***
     * Verify that the value of two lists are the same.
     * The method use the Collections Api library
     * In this case the locator is passed to obtain the values of the first list
     */
    public void verifyColumnSortingByLocator(String sortDirection, Locator locatorValue) {
        List<String> listFromLocator = locatorValue.allTextContents();

        if (listFromLocator.isEmpty()) {
            throw new IllegalArgumentException("The column list is empty. Cannot verify sorting.");
        }

        List<String> sortedList = new ArrayList<>(listFromLocator);

        if ("ascending".equalsIgnoreCase(sortDirection)) {
            sortedList.sort(String::compareTo);
        } else if ("descending".equalsIgnoreCase(sortDirection)) {
            sortedList.sort(Collections.reverseOrder(String::compareTo));
        } else {
            throw new IllegalArgumentException("Invalid sort direction: " + sortDirection + ". Use 'ascending' or 'descending'.");
        }

        if (!listFromLocator.equals(sortedList)) {
            throw new AssertionError("The column is not sorted in " + sortDirection + " order.");
        }
    }

    /**
     * Clicks the next or previous page button and/or verifies that the table is updated correctly.
     *
     * @param buttonToPress The button to be pressed ("Next page" or "Previous page").
     * @param compareLists  Whether to compare the lists before and after pagination.
     * @param listToReview  The list in which the review is performed.
     */
    public void clickNextOrPreviousPageButton(String buttonToPress, String listToReview, boolean compareLists) {
        Locator buttonLocator = switch (buttonToPress) {
            case "Next page" -> page.locator(NEXT_PAGE_BUTTON_LOCATOR);
            case "Previous page" -> page.locator(PREVIOUS_PAGE_BUTTON_LOCATOR);
            default -> throw new IllegalArgumentException("Invalid buttonToPress value: " + buttonToPress);
        };

        assertElementIsVisibleByLocator(buttonLocator);

        String columnLocator = switch (listToReview) {
            case "request list" -> REQUEST_EMAIL_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR;
            case "participant list" -> PARTICIPANT_LIST_PARTICIPANT_NAME_COLUMN_LOCATOR;
            case "identity attributes list" -> IDENTITY_ATTRIBUTES_LIST_LOCATOR;
            default -> throw new IllegalArgumentException("Invalid list provided value: " + listToReview);
        };
        page.waitForSelector(columnLocator);

        List<String> previousStringsInColumns = compareLists
                ? page.locator(columnLocator).allTextContents()
                : Collections.emptyList();

        if (buttonLocator.isEnabled()) {
            buttonLocator.click();
            if (compareLists) {
                page.waitForSelector(columnLocator);
                List<String> newStringsInColumns = page.locator(columnLocator).allTextContents();

                if (buttonToPress.equals("Next page")) {
                    Assertions.assertElementEqualsOrLess(previousStringsInColumns.size(), newStringsInColumns.size());
                } else {
                    Assertions.assertElementEqualsOrGreater(previousStringsInColumns.size(), newStringsInColumns.size());
                }

                if (previousStringsInColumns.equals(newStringsInColumns)) {
                    throw new AssertionError("After pagination, the items in the lists are the same; it has not been updated correctly.");
                }
            }
        }
    }

    /***
     * This method checks that the approval or reject button is visible and then proceeds with the action
     * of approving or rejecting the request.
     * If the request was successfully approved, it is checked that the "APPROVED" status is visible
     * If the request was successfully rejected, it is first necessary to fill out the text message,
     * accept it and then check that the "REJECTED" status is visible
     *
     * @param action
     */
    public void theUserProcessActionValue(String action) throws InterruptedException {
        switch (action) {
            case "Confirm Approval" -> processApproval();
            case "Reject Request" -> processRejection();
            default -> throw new IllegalArgumentException("Invalid action value: " + action);
        }
    }

    /***
     * This method checks for elements or status that should be visible in the UI after the request is rejected.
     * This method is called by theUserProcessActionValue(String action)
     *
     * @throws InterruptedException
     */
    private void processApproval() throws InterruptedException {
        Utils.clickButtonByAriaText("Approve");
        assertElementIsVisibleByLocator(page.getByText(ALERT_DISPLAYING_SUCCESSFUL_APPROVED));
        Thread.sleep(100);
        waitForStatusToBeDisplayed("APPROVED");
    }

    /***
     * This method checks for elements or status that should be visible in the UI after the request is approved.
     * This method is called by theUserProcessActionValue(String action)
     *
     * @throws InterruptedException
     */
    private void processRejection() throws InterruptedException {
        Locator reasonField = page.locator(REJECT_REQUEST_TYPE_REASON_FIELD_LOCATOR);
        assertElementIsVisibleByLocator(reasonField);
        reasonField.fill("Automation reject test");

        Utils.clickButtonByAriaText("Reject");
        assertElementIsVisibleByLocator(page.getByText(ALERT_DISPLAYING_SUCCESSFUL_REJECTED));
        Thread.sleep(100);
        waitForStatusToBeDisplayed("REJECTED");
    }

    /***
     * This method shows the status of the request once the request was approved or rejected.
     * The method is called by processRejection()
     *
     * @param expectedStatus    APPROVED | REJECTED  1
     */
    private void waitForStatusToBeDisplayed(String expectedStatus) {
        assertElementIsVisibleByLocator(page.locator(REQUEST_STATUS_LOCATOR));
        String actualStatus = page.locator(REQUEST_STATUS_LOCATOR).textContent().trim();
        if (!actualStatus.contains(expectedStatus)) {
            throw new AssertionError("The status: " + expectedStatus + " is not displayed. Found: " + actualStatus);
        }
    }

    /***
     *Validates values in an Identity Attribute table column.
     * Basically, it validates that for the "Assignable to Role" and "In Use" columns boolean values are displayed
     * and for the "Action" column the button with an image is displayed to delete the row.
     *
     * @param columnName
     */
    public void validateColumnValueForEachIdentityAttribute(String columnName) {

        Locator columnLocator = switch (columnName) {
            case "Assignable to Role" -> page.locator(IDENTITY_ATTRIBUTE_LIST_ASSIGNABLE_TO_ROLE_COLUMN_LOCATOR);
            case "In use" -> page.locator(IDENTITY_ATTRIBUTE_LIST_IN_USE_COLUMN_LOCATOR);
            case "Action" -> page.locator(IDENTITY_ATTRIBUTE_LIST_DELETE_COLUMN_LOCATOR);
            default -> throw new IllegalArgumentException("Unknown column: " + columnName);
        };

        List<String> columnValues = columnLocator.allTextContents();
        int rowSize = columnLocator.count();
        if (columnName.equals("Action")) {
            for (int i = 0; i < rowSize; i++) {
                String spanValue = columnValues.get(i).trim();
                String roleValue = columnLocator.nth(i).getAttribute("role");
                assertTrue(String.format("No 'Delete' buttons found in column '%s'", columnName), "delete".equals(spanValue));
                assertTrue(String.format("The role attribute is not correct '%s'", columnName), "img".equals(roleValue));
            }
        } else if (columnName.equals("Assignable to Role") || columnName.equals("In use")) {
            for (String value : columnValues) {
                value = value.trim();
                assertTrue(String.format("Invalid boolean value found."), "true".equals(value) || "false".equals(value));
            }
        }
    }

    /***
     * This method compares the Identifier ID from the URL with the ID that is displayed on the
     * Participant Detail page
     * @return
     */
    public String verifyIdInUrl() {
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_IDENTIFIER_LABEL_LOCATOR));
        String currentUrl = page.url();
        String urlIdentifier = currentUrl.substring(currentUrl.lastIndexOf("/") + 1);
        String pageIdentifier = page.locator(PARTICIPANT_DETAILS_IDENTIFIER_VALUE_LOCATOR).textContent().trim();
        if (!pageIdentifier.equals(urlIdentifier)) {
            throw new AssertionError(" The Identifiers do no match: " + urlIdentifier + " but found: " + pageIdentifier);
        }
        return urlIdentifier;
    }

    /***
     *This method parses from date format "Feb 11, 2025"  into "Tue Feb 11 2025"
     *
     * @param dateToBeParsed
     * @return String
     */
    public String parseDate(String dateToBeParsed) {
        Date date = new Date(dateToBeParsed);
        SimpleDateFormat df = new SimpleDateFormat("EEE MMM dd yyyy", Locale.ENGLISH);
        return df.format(date);
    }

    /***
     *  This method compares the values obtained dynamically from the first row of the Participant List page
     *  and compares them with the values obtained dynamically from the Participant Detail page.
     *
     * @param dataTable        The values of this table are obtained from the features
     * @param participantDetailNameValue   This is a String value obtained before to select the first row
     * @param participantDetailTypeValue   This is a String value obtained before to select the first row
     * @param participantDetailOnboardingValue  This is a String value obtained before to select the first row
     */
    public void verifyTheValuesObtainedMatchWithValueDisplayed(DataTable dataTable, String participantDetailNameValue, String participantDetailTypeValue, String participantDetailOnboardingValue) {

        Map<String, String> map = dataTable.asMap(String.class, String.class);
        HashMap<String, String> details = new HashMap<>(map);

        String identifier = details.get("Identifier");
        String participantName = details.get("Participant Name");
        String participantType = details.get("Participant type");
        String onboardingDate = details.get("Onboarding Date");
        String credentialExpirationDate = details.get("Participant Credentials Expiration Date");

        if ("IDENTIFIER_OBTAINED_DYNAMICALLY_FROM_URL".equals(identifier)) {
            details.put("Identifier", verifyIdInUrl());
        }
        if ("PARTICIPANT_NAME_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE".equals(participantName)) {
            assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_NAME_LABEL_LOCATOR));
            assertEquals(page.locator(PARTICIPANT_DETAILS_NAME_VALUE_LOCATOR).textContent(), participantDetailNameValue);
            details.put("Participant Name", page.locator(PARTICIPANT_DETAILS_NAME_VALUE_LOCATOR).textContent());
        }
        if ("PARTICIPANT_TYPE_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE".equals(participantType)) {
            assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_TYPE_LABEL_LOCATOR));
            assertEquals(page.locator(PARTICIPANT_DETAILS_TYPE_VALUE_LOCATOR).textContent(), participantDetailTypeValue);
            details.put("Participant type", page.locator(PARTICIPANT_DETAILS_TYPE_VALUE_LOCATOR).textContent());
        }
        if ("ONBOARDING_DATE_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE".equals(onboardingDate)) {
            assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_ONBOARDING_DATE_LABEL_LOCATOR));
            assertEquals(page.locator(PARTICIPANT_DETAILS_ONBOARDING_DATE_VALUE_LOCATOR).textContent(), participantDetailOnboardingValue);
            details.put("Onboarding Date", page.locator(PARTICIPANT_DETAILS_ONBOARDING_DATE_VALUE_LOCATOR).textContent());
        }
        if ("CREDENTIALS_EXPIRATION_DATE_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE".equals(credentialExpirationDate)) {
            assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_CREDENTIAL_EXPIRATION_DATE_LABEL_LOCATOR));
            details.put("Participant Credentials Expiration Date", page.locator(PARTICIPANT_DETAILS_CREDENTIAL_EXPIRATION_DATE_VALUE_LOCATOR).textContent());
        }

        List<String> expectedValues = new ArrayList<>(details.values());
        List<String> realValues = page.locator(PARTICIPANT_DETAILS_COLUMNS_LOCATOR).allTextContents();
        realValues.replaceAll(String::strip);
        assertEqualCollections(expectedValues, realValues);
    }

    /***
     * The method verifies if the tag or label to the right of the "Filter" button was applied successfully.
     *
     * @param filter
     * @param valueFilter
     * @param dateParsed  ; this value is not taken from the Authority.feature as a parameter, it is calculated and parsed as String
     */
    public void verifyFilterIsAppied(String filter, String valueFilter, String dateParsed) {
        String filterValueApplied = "null";
        if (filter.equals("Onboarding Date")) {
            filterValueApplied = filter.concat(":").concat(" ").concat(dateParsed).concat(";");
        } else {
            filterValueApplied = filter.concat(":").concat(" ").concat(valueFilter.toUpperCase()).concat(";");
        }
        String filterAppliedLocatorValue = page.locator(FILTER_APPLIED_VALUE_LOCATOR).innerText().trim();
        if (!filterValueApplied.equals(filterAppliedLocatorValue)) {
            throw new AssertionError("The filter: " + filter + " with the value " + valueFilter + " is not displayed.");
        }
    }

    /***
     *  The method verifies if the tag or label to the right of the "Filter" button was applied successfully.
     *  This method is used to validate when one more than one filter is applied
     *
     * @param filter1
     * @param valueFilter1
     * @param filter2
     * @param valueFilter2
     */
    public void verifyFiltersAreAppied(String filter1, String valueFilter1, String filter2, String valueFilter2) {
        String filter1ValueApplied = null;
        String filter2ValueApplied = null;
        String filterValueApplied = null;
        filter1ValueApplied = changeFirstLetterToUpperCase(filter1).concat(": ").concat(valueFilter1);
        filter2ValueApplied = changeFirstLetterToUpperCase(filter2).concat(": ").concat(valueFilter2);
        filterValueApplied = filter2ValueApplied.concat("; ").concat(filter1ValueApplied).concat(";");
        String filterAppliedLocatorValue = page.locator(MULTIPLE_FILTER_APPLIED_VALUE_LOCATOR).innerText().trim();
        if (!filterValueApplied.equals(filterAppliedLocatorValue)) {
            throw new AssertionError("The filter: " + filter1 + " with the value " + valueFilter1 + " and " + filter2 + " with the value " + valueFilter2 + " are not displayed.");
        }
    }

    private String changeFirstLetterToUpperCase(String filter) {
        return (filter.toUpperCase().charAt(0) + filter.substring(1, filter.length()).toLowerCase());
    }
}
