package framework.ui.pages.simplOpen;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

import static configuration.ui.data.simplOpen.Wizard.*;
import static framework.common.Assertions.assertElementIsVisibleByLocator;

public class WizardPage {
    private String suggestedFilename;
    private final Page page;

    public void setSuggestedFilename(String suggestedFilename) {
        this.suggestedFilename = suggestedFilename;
    }

    public String getSuggestedFilename() {
        return suggestedFilename;
    }


    public WizardPage(Page page) {
        this.page = page;
    }

    /**
     * This method fills the fields in the General Service Properties box in the Data Offering form.
     *
     * @param generalServiceProperties general service properties.
     */
    public void fillGeneralServicePropertiesFields(GeneralServicePropertiesShape generalServiceProperties) {
        page.getByText(GENERAL_SERVICE_PROPERTIES_SHAPE_DROPDOWN).click();
        String name = generalServiceProperties.name;
        String description = generalServiceProperties.description;
        String serviceAccessPoint = generalServiceProperties.serviceAccessPoint;
        String keywords = generalServiceProperties.keywords;
        String inLanguage = generalServiceProperties.inLanguage;
        if (name != null) {
            page.getByPlaceholder(GENERAL_SERVICE_PROPERTIES_NAME_PLACEHOLDER).fill(name);
        }
        if (description != null) {
            page.getByPlaceholder(GENERAL_SERVICE_PROPERTIES_DESCRIPTION_PLACEHOLDER).fill(description);
        }
        if (serviceAccessPoint != null) {
            page.getByPlaceholder(GENERAL_SERVICE_PROPERTIES_SERVICE_ACCESS_POINT_PLACEHOLDER).fill(serviceAccessPoint);
        }
        if (keywords != null) {
            page.getByPlaceholder(GENERAL_SERVICE_PROPERTIES_KEYWORDS_PLACEHOLDER).fill(keywords);
        }
        if (inLanguage != null) {
            page.getByText(GENERAL_SERVICE_PROPERTIES_IN_LANGUAGE_PLACEHOLDER)
                    .click(new Locator.ClickOptions().setForce(true));
            assertElementIsVisibleByLocator(page.getByText(inLanguage, new Page.GetByTextOptions().setExact(true)));
            page.getByText(inLanguage, new Page.GetByTextOptions().setExact(true)).click();
        }
    }

    /**
     * This method fills the fields in the Data Properties box in the Data Offering form.
     *
     * @param dataProperties data properties.
     */
    public void fillDataPropertiesFields(DataPropertiesShape dataProperties) {
        page.getByText(DATA_PROPERTIES_SHAPE_DROPDOWN).click();
        String producedBy = dataProperties.producedBy;
        String format = dataProperties.format;
        String additionalInfo = dataProperties.additionalInfo;
        String relatedDatasets = dataProperties.relatedDatasets;
        String targetUsers = dataProperties.targetUsers;
        String dataQuality = dataProperties.dataQuality;
        String encryption = dataProperties.encryption;
        String anonymization = dataProperties.anonymization;
        if (producedBy != null) {
            page.getByPlaceholder(DATA_PROPERTIES_PRODUCED_BY_PLACEHOLDER).fill(producedBy);
        }
        if (format != null) {
            page.getByPlaceholder(DATA_PROPERTIES_FORMAT_PLACEHOLDER).fill(format);
        }
        if (additionalInfo != null) {
            page.getByPlaceholder(DATA_PROPERTIES_ADDITIONAL_INFO_PLACEHOLDER).fill(additionalInfo);
        }
        if (relatedDatasets != null) {
            page.getByPlaceholder(DATA_PROPERTIES_RELATED_DATASETS_PLACEHOLDER).fill(relatedDatasets);
        }
        if (targetUsers != null) {
            page.getByPlaceholder(DATA_PROPERTIES_TARGET_USERS_PLACEHOLDER).fill(targetUsers);
        }
        if (dataQuality != null) {
            page.getByPlaceholder(DATA_PROPERTIES_DATA_QUALITY_PLACEHOLDER).fill(dataQuality);
        }
        if (encryption != null) {
            page.getByPlaceholder(DATA_PROPERTIES_ENCRYPTION_PLACEHOLDER).fill(encryption);
        }
        if (anonymization != null) {
            page.getByPlaceholder(DATA_PROPERTIES_ANONYMIZATION_PLACEHOLDER).fill(anonymization);
        }
    }

    /**
     * This method fills the fields in the Provider Information box in the Data Offering form.
     *
     * @param providerInformation provider information.
     */
    public void fillProviderInformationFields(ProviderInformationShape providerInformation) {
        page.getByText(PROVIDER_INFORMATION_SHAPE_DROPDOWN).click();
        String providedBy = providerInformation.providedBy;
        String contact = providerInformation.contact;
        String signature = providerInformation.signature;
        if (providedBy != null) {
            page.getByPlaceholder(PROVIDER_INFORMATION_PROVIDED_BY_PLACEHOLDER).fill(providedBy);
        }
        if (contact != null) {
            page.getByPlaceholder(PROVIDER_INFORMATION_CONTACT_PLACEHOLDER).fill(contact);
        }
        if (signature != null) {
            page.getByPlaceholder(PROVIDER_INFORMATION_SIGNATURE_PLACEHOLDER).fill(signature);
        }
    }

    /**
     * This method fills the fields in the Offering Price box in the Data Offering form.
     *
     * @param offeringPrice offering price.
     */
    public void fillOfferingPriceFields(OfferingPriceShape offeringPrice) {
        page.getByText(OFFERING_PRICE_SHAPE_DROPDOWN).click();
        String license = offeringPrice.license;
        String currency = offeringPrice.currency;
        String price = offeringPrice.price;
        String priceType = offeringPrice.priceType;
        if (license != null) {
            page.getByPlaceholder(OFFERING_PRICE_LICENSE_PLACEHOLDER).fill(license);
        }
        if (currency != null) {
            page.getByText(OFFERING_PRICE_CURRENCY_PLACEHOLDER).click(new Locator.ClickOptions().setForce(true));
            assertElementIsVisibleByLocator(page.getByText(currency, new Page.GetByTextOptions().setExact(true)));
            page.getByText(currency, new Page.GetByTextOptions().setExact(true)).click();
        }
        if (price != null) {
            page.getByPlaceholder(OFFERING_PRICE_PRICE_PLACEHOLDER).fill(price);
        }
        if (priceType != null) {
            page.getByText(OFFERING_PRICE_PRICE_TYPE_PLACEHOLDER).click(new Locator.ClickOptions().setForce(true));
            assertElementIsVisibleByLocator(page.getByText(priceType, new Page.GetByTextOptions().setExact(true)));
            page.getByText(priceType, new Page.GetByTextOptions().setExact(true)).click();
        }
    }

    /**
     * This method fills the fields in the Service Policy box in the Data Offering form.
     *
     * @param servicePolicy service policy.
     */
    public void fillServicePolicyFields(ServicePolicyShape servicePolicy) {
        page.getByText(SERVICE_POLICY_SHAPE_DROPDOWN).click();
        String accessPolicy = servicePolicy.accessPolicy;
        String usagePolicy = servicePolicy.usagePolicy;
        String dataProtectionRegime = servicePolicy.dataProtectionRegime;
        if (accessPolicy != null) {
            page.getByPlaceholder(SERVICE_POLICY_ACCESS_POLICY_PLACEHOLDER).fill(accessPolicy);
        }
        if (usagePolicy != null) {
            page.getByPlaceholder(SERVICE_POLICY_USAGE_POLICY_PLACEHOLDER).fill(usagePolicy);
        }
        if (dataProtectionRegime != null) {
            page.getByPlaceholder(SERVICE_POLICY_DATA_PROTECTION_REGIME_PLACEHOLDER).fill(dataProtectionRegime);
        }
    }

    /**
     * This method fills the Contract Template box in the Data Offering form.
     *
     * @param contractTemplate contract template.
     */
    public void fillContractTemplate(String contractTemplate) {
        page.getByText(CONTRACT_TEMPLATE_SHAPE_DROPDOWN).click();
        Locator contractTemplateDocument = page.getByText(CONTRACT_TEMPLATE_DOCUMENT_PLACEHOLDER);
        Locator saveButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save"));
        if (contractTemplate != null) {
            saveButton.scrollIntoViewIfNeeded();
            contractTemplateDocument.click(new Locator.ClickOptions().setForce(true));
            assertElementIsVisibleByLocator(page.getByText(contractTemplate, new Page.GetByTextOptions().setExact(true)));
            page.getByText(contractTemplate, new Page.GetByTextOptions().setExact(true)).click();
        }
    }

    /**
     * This method fills the Billing Schema box in the Data Offering form.
     *
     * @param billingSchema billing schema.
     */
    public void fillBillingSchema(String billingSchema) {
        page.getByText(BILLING_SCHEMA_SHAPE_DROPDOWN).click();
        Locator billingSchemaDocument = page.getByText(BILLING_SCHEMA_DOCUMENT_PLACEHOLDER);
        Locator saveButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save"));
        if (billingSchema != null) {
            saveButton.scrollIntoViewIfNeeded();
            billingSchemaDocument.click(new Locator.ClickOptions().setForce(true));
            assertElementIsVisibleByLocator(page.getByText(billingSchema, new Page.GetByTextOptions().setExact(true)));
            page.getByText(billingSchema, new Page.GetByTextOptions().setExact(true)).click();
        }
    }

    /**
     * This method fills the Sla Agreements box in the Data Offering form.
     *
     * @param slaAgreements sla agreements.
     */
    public void fillSlaAgreements(String slaAgreements) {
        page.getByText(SLA_AGREEMENTS_SHAPE_DROPDOWN).click();
        Locator slaAgreementsDocument = page.getByText(SLA_AGREEMENTS_DOCUMENT_PLACEHOLDER);
        Locator saveButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save"));
        if (slaAgreements != null) {
            saveButton.scrollIntoViewIfNeeded();
            slaAgreementsDocument.click(new Locator.ClickOptions().setForce(true));
            assertElementIsVisibleByLocator(page.getByText(slaAgreements, new Page.GetByTextOptions().setExact(true)));
            page.getByText(slaAgreements, new Page.GetByTextOptions().setExact(true)).click();
        }
    }

    /**
     * POJO classes for the fields to fill in the Data Offering form.
     */
    public static class GeneralServicePropertiesShape {
        private String name;
        private String description;
        private String serviceAccessPoint;
        private String keywords;
        private String inLanguage;

        public GeneralServicePropertiesShape setName(String name) {
            this.name = name;
            return this;
        }

        public GeneralServicePropertiesShape setDescription(String description) {
            this.description = description;
            return this;
        }

        public GeneralServicePropertiesShape setServiceAccessPoint(String serviceAccessPoint) {
            this.serviceAccessPoint = serviceAccessPoint;
            return this;
        }

        public GeneralServicePropertiesShape setKeywords(String keywords) {
            this.keywords = keywords;
            return this;
        }

        public GeneralServicePropertiesShape setInLanguage(String inLanguage) {
            this.inLanguage = inLanguage;
            return this;
        }

    }

    public static class DataPropertiesShape {
        private String producedBy;
        private String format;
        private String additionalInfo;
        private String relatedDatasets;
        private String targetUsers;
        private String dataQuality;
        private String encryption;
        private String anonymization;

        public DataPropertiesShape setProducedBy(String producedBy) {
            this.producedBy = producedBy;
            return this;
        }

        public DataPropertiesShape setFormat(String format) {
            this.format = format;
            return this;
        }

        public DataPropertiesShape setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
            return this;
        }

        public DataPropertiesShape setRelatedDatasets(String relatedDatasets) {
            this.relatedDatasets = relatedDatasets;
            return this;
        }

        public DataPropertiesShape setTargetUsers(String targetUsers) {
            this.targetUsers = targetUsers;
            return this;
        }

        public DataPropertiesShape setDataQuality(String dataQuality) {
            this.dataQuality = dataQuality;
            return this;
        }

        public DataPropertiesShape setEncryption(String encryption) {
            this.encryption = encryption;
            return this;
        }

        public DataPropertiesShape setAnonymization(String anonymization) {
            this.anonymization = anonymization;
            return this;
        }

    }

    public static class ProviderInformationShape {
        private String providedBy;
        private String contact;
        private String signature;

        public ProviderInformationShape setProvidedBy(String providedBy) {
            this.providedBy = providedBy;
            return this;
        }

        public ProviderInformationShape setContact(String contact) {
            this.contact = contact;
            return this;
        }

        public ProviderInformationShape setSignature(String signature) {
            this.signature = signature;
            return this;
        }

    }

    public static class OfferingPriceShape {
        private String license;
        private String currency;
        private String price;
        private String priceType;

        public OfferingPriceShape setLicense(String license) {
            this.license = license;
            return this;
        }

        public OfferingPriceShape setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public OfferingPriceShape setPrice(String price) {
            this.price = price;
            return this;
        }

        public OfferingPriceShape setPriceType(String priceType) {
            this.priceType = priceType;
            return this;
        }

    }

    public static class ServicePolicyShape {
        private String accessPolicy;
        private String usagePolicy;
        private String dataProtectionRegime;

        public ServicePolicyShape setAccessPolicy(String accessPolicy) {
            this.accessPolicy = accessPolicy;
            return this;
        }

        public ServicePolicyShape setUsagePolicy(String usagePolicy) {
            this.usagePolicy = usagePolicy;
            return this;
        }

        public ServicePolicyShape setDataProtectionRegime(String dataProtectionRegime) {
            this.dataProtectionRegime = dataProtectionRegime;
            return this;
        }

    }

}
