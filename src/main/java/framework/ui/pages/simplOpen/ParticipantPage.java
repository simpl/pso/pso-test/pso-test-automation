package framework.ui.pages.simplOpen;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import framework.ui.helpers.UiSetup;

import java.util.List;

import static configuration.ui.data.simplOpen.Participant.*;
import static framework.common.Assertions.*;
import static framework.ui.locators.simplOpen.Participant.*;

public class ParticipantPage {

    private final Page page = UiSetup.getPage();

    /**
     * This method verifies the information displayed for a user in the Echo Page.
     *
     * @param information user information.
     */
    public void verifyUserInformationInEchoPage(UserInformation information) {

        String commonName = information.commonName;
        if ("ONBOARDED_CONSUMER_ID".equals(commonName)) {
            commonName = ONBOARDED_CONSUMER_ID;
        } else if ("ONBOARDED_DATA_PROVIDER_ID".equals(commonName)) {
            commonName = ONBOARDED_DATA_PROVIDER_ID;
        }

        List<String> pageInformation = page.locator(ECHO_PAGE_USER_INFORMATION_DIVS).allTextContents();
        pageInformation.replaceAll(string -> string.replaceAll("\u00a0", ""));

        if (information.username != null) {
            assertElementIsPresentInCollection("Username:" + information.username, pageInformation);
        }

        if (information.emailAddress != null) {
            assertElementIsPresentInCollection("Email Address:" + information.emailAddress, pageInformation);
        }

        if (commonName != null) {
            assertElementIsPresentInCollection("Common name:" + commonName, pageInformation);
        }

        if (information.organisation != null) {
            assertElementIsPresentInCollection("Organization:" + information.organisation, pageInformation);
        }

        if (information.participantType != null) {
            assertElementIsPresentInCollection("Participant Type:" + information.participantType, pageInformation);
        }

        if (information.assignedIdentityAttributes != null) {
            assertElementIsPresentInCollection("Assigned identity attributes: " + information.assignedIdentityAttributes, pageInformation);
        }

    }

    /**
     * This method verifies the visibility of the Echo and Upload Credential buttons in the Participant Utility Page.
     *
     * @param buttonVisibility whether the button is visible
     * @param button           button
     */
    public void verifyVisibilityOfButtonsInParticipantUtilityPage(boolean buttonVisibility, String button) {
        switch (button) {
            case "Echo":
                Locator echoButton = page.getByText(ECHO_BUTTON_TEXT);
                if (buttonVisibility) {
                    assertElementIsVisibleByLocator(echoButton);
                } else {
                    assertElementIsNotVisibleByLocator(echoButton);
                }
                break;
            case "Upload credential":
                Locator uploadCredentialButton = page.getByRole(AriaRole.BUTTON,
                        new Page.GetByRoleOptions().setName(UPLOAD_CREDENTIAL_BUTTON_TEXT));
                if (buttonVisibility) {
                    assertElementIsVisibleByLocator(uploadCredentialButton);
                } else {
                    assertElementIsNotVisibleByLocator(uploadCredentialButton);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown button: " + button);
        }
    }

    /**
     * POJO class for a user information displayed in the Echo Page.
     */
    public static class UserInformation {
        private String username;
        private String emailAddress;
        private String commonName;
        private String organisation;
        private String participantType;
        private String assignedIdentityAttributes;

        public UserInformation setUsername(String username) {
            this.username = username;
            return this;
        }

        public UserInformation setEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
            return this;
        }

        public UserInformation setCommonName(String commonName) {
            this.commonName = commonName;
            return this;
        }

        public UserInformation setOrganisation(String organisation) {
            this.organisation = organisation;
            return this;
        }

        public UserInformation setParticipantType(String participantType) {
            this.participantType = participantType;
            return this;
        }

        public UserInformation setAssignedIdentityAttributes(String assignedIdentityAttributes) {
            this.assignedIdentityAttributes = assignedIdentityAttributes;
            return this;
        }

    }

}
