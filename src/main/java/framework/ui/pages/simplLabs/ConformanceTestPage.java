package framework.ui.pages.simplLabs;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;

import static framework.ui.locators.simplLabs.ConformanceTest.*;
import static framework.ui.locators.simplLabs.DataSpaces.COMPONENTS_MENU_ITEM;
import static framework.ui.locators.simplLabs.DataSpaces.CONFORMANCE_MENU_ITEM;

public class ConformanceTestPage {
    private final Page page;

    public ConformanceTestPage(Page page) {
        this.page = page;
    }

    public void clickComponentsMenuItem() {
        page.click(COMPONENTS_MENU_ITEM);
    }

    public void clickConformanceMenuItem() {
        page.click(CONFORMANCE_MENU_ITEM);
    }

    public void openFirstComponentConformanceTestPage() {
        page.locator(COMPONENTS_CARD_BUTTON).first().click();
        page.locator(OPEN_CONFORMANCE_TEST_ARROW).first().click();
    }

    public void clickCheckboxForTestcase() {
        page.locator(TEST_SUITE_CHECKBOX).click();
    }

    public void clickStartConformanceTestButton() {
        page.locator(START_CONFORMANCE_TEST_BUTTON).click();
    }

    public void checkConformanceTestPageOpened() {
        PlaywrightAssertions.assertThat(page.locator(CONFORMANCE_TEST_APP)).isVisible();
    }
}
