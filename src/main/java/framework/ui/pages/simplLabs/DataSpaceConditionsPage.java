package framework.ui.pages.simplLabs;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;

import static framework.ui.locators.simplLabs.DataSpaceConditions.*;

public class DataSpaceConditionsPage {
    private final Page page;

    public DataSpaceConditionsPage(Page page) {
        if (page == null) {
            throw new IllegalArgumentException("Page object cannot be null");
        }
        this.page = page;
    }

    public void checkDataSpaceConditionsPageOpened() {
        PlaywrightAssertions.assertThat(page.locator(DATASPACE_CONDITIONS_APP)).isVisible();
    }

    public void clickButtonNext() {
        page.locator(NEXT_BUTTON).click();
    }

    public void checkButtonNextEnabled() {
        Locator nextButton = page.locator(NEXT_BUTTON);
        PlaywrightAssertions.assertThat(nextButton).isEnabled();
    }

    public void clickAcceptAllCheckbox() {
        page.locator(ACCEPT_ALL_CHECKBOX).click();
    }

    public void clickNotAllCheckboxes() {
        Locator checkboxes = page.locator(COMPONENT_CHECKBOX);
        int count = checkboxes.count();
        for (int i = 0; i < count - 1; i++) {
            checkboxes.nth(i).click();
        }
    }

    public void clickCreateDataSpaceButton() {
        page.locator(CREATE_DATA_SPACE_BUTTON).click();
    }

    public void checkCreateDataSpaceButtonBlocked() {
        boolean isDisabled = page.locator(CREATE_DATA_SPACE_BUTTON).isDisabled();
        if (!isDisabled) {
            throw new AssertionError("Create Data Space button is not disabled as expected.");
        }
    }
}
