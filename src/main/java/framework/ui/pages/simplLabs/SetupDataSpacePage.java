package framework.ui.pages.simplLabs;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;
import com.microsoft.playwright.options.WaitForSelectorState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static framework.ui.helpers.Utils.checkElementHasText;
import static framework.ui.locators.simplLabs.SetupDataSpace.*;

public class SetupDataSpacePage {
    private final Page page;

    public SetupDataSpacePage(Page page) {
        this.page = page;
    }

    public void checkSetupDataSpacePageOpened() {
        PlaywrightAssertions.assertThat(page.locator(SETUP_DATASPACE_APP)).isVisible();
    }

    public void clickButtonNext() {
        page.locator(NEXT_BUTTON).click();
    }

    public void clickBackButton() {
        page.locator(BACK_BUTTON).click();
    }

    public void checkThePageDataSpaceArchitectureOpened() {
        checkElementHasText(page, ACTIVE_TAB_LABEL, "Data Space architecture");
    }

    public void checkNodesListContainsSpecificNodes(List<String> expectedNodes) {
        Locator nodesList = page.locator(NODE_ITEM_IN_NODES_LIST);

        int count = nodesList.count();
        if (count != 5) {
            throw new AssertionError("Expected 5 nodes, but found " + count);
        }

        List<String> actualNodes = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            String actualText = nodesList.nth(i).textContent().trim();
            String filteredText = actualText.replaceAll("(arrow_right|more_horiz)", "").trim();
            actualNodes.add(filteredText);
        }

        for (String expectedNode : expectedNodes) {
            if (!actualNodes.contains(expectedNode)) {
                throw new AssertionError("Expected node: " + expectedNode + " is not found in the actual nodes list.");
            }
        }
    }

    public void clickButtonAddNodes() {
        page.locator(ADD_NODES_BUTTON).click();
    }

    public void clickButtonAddNodesInAddNodesModal() {
        page.locator(ADD_NODES_MODAL_ADD_NODES_BUTTON).click();
        page.waitForSelector(ADD_NODES_MODAL_ADD_NODES_BUTTON, new Page.WaitForSelectorOptions().setState(WaitForSelectorState.HIDDEN));
    }

    public void clickSpecificNodes(List<String> nodeNames) {
        Map<String, Integer> nodeIndexMap = Map.of(
                "APPLICATION_PROVIDER", 2,
                "INFRASTRUCTURE_PROVIDER", 3
        );

        Locator nodes = page.locator(AVAILABLE_NODES_ICON);

        int count = nodes.count();
        if (count < 4) {
            throw new IllegalArgumentException("There are fewer than 4 elements to click.");
        }

        for (String nodeName : nodeNames) {
            if (nodeIndexMap.containsKey(nodeName)) {
                int index = nodeIndexMap.get(nodeName);
                Locator node = nodes.nth(index);

                node.waitFor(new Locator.WaitForOptions().setState(WaitForSelectorState.VISIBLE));
                node.click();
            } else {
                throw new IllegalArgumentException("Node " + nodeName + " does not exist in the map.");
            }
        }
    }

    public void checkSelectedNodesContainSpecificNodes(List<String> expectedNodes) {
        Locator selectedNodes = page.locator(SELECTED_NODES_ITEM);

        int count = selectedNodes.count();
        if (count != 2) {
            throw new AssertionError("Expected 3 elements, but found " + count);
        }

        for (int i = 0; i < 2; i++) {
            String actualText = selectedNodes.nth(i).textContent().trim();
            String expectedText = expectedNodes.get(i);
            if (!actualText.equals(expectedText)) {
                throw new AssertionError("Expected text: " + expectedText + ", but found: " + actualText);
            }
        }
    }
}
