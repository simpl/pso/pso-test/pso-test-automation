package framework.ui.pages.simplLabs;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.LocatorAssertions;
import com.microsoft.playwright.assertions.PlaywrightAssertions;
import framework.ui.helpers.Utils;

import java.time.Duration;
import java.time.Instant;

import static framework.ui.locators.simplLabs.DataSpaces.*;

public class DataSpacesPage {
    private final Page page;

    public DataSpacesPage(Page page) {
        this.page = page;
    }

    public void selectStatusFilter(String status) {
        page.click(STATUS_FILTER);
        page.waitForSelector(STATUS_DROPDOWN_ITEM);

        Locator statusLocator = Utils.getLocatorWithText(page, STATUS_DROPDOWN_ITEM, status);
        statusLocator.click();

        page.waitForLoadState();
    }

    public void checkDataSpacesDisplayedWithStatus(String status) {
        PlaywrightAssertions.assertThat(page.locator(DATA_SPACE_STATUS_LABEL, Utils.getLocatorOptionsWithText(status)))
                .isVisible(new LocatorAssertions.IsVisibleOptions().setTimeout(60000));
    }

    public void waitDataSpaceDeletion(int expectedAmount) {
        page.click(STATUS_FILTER);
        page.waitForSelector(STATUS_DROPDOWN_ITEM);
        String allDataspaces = "-- none --";
        Locator statusLocator = Utils.getLocatorWithText(page, STATUS_DROPDOWN_ITEM, allDataspaces);
        statusLocator.click();

        page.waitForLoadState();

        Duration timeout = Duration.ofSeconds(30);
        Instant startTime = Instant.now();

        while (Duration.between(startTime, Instant.now()).compareTo(timeout) < 0) {
            Locator dataSpaceStatuses = page.locator(DATA_SPACE_STATUS_LABEL);
            int count = dataSpaceStatuses.count();

            if (count == expectedAmount) {
                return;
            }
            page.waitForTimeout(500);
        }
        throw new AssertionError("Expected " + expectedAmount + " dataspaces, but found more or fewer.");
    }

    public void checkConfirmationPageDisplayed() {
        PlaywrightAssertions.assertThat(page.locator(DATA_SPACE_CREATION_LABEL)).isVisible();
    }

    public void checkTheUserIsLoggedIn() {
        PlaywrightAssertions.assertThat(page.locator(CREATE_DATASPACE_BUTTON)).isVisible();
    }

    public void clickCreateDataSpaceButton() {
        Utils.clickButtonWithText(page, CREATE_DATASPACE_BUTTON, "Create Data Space");
    }

    public void clickLogoutButton() {
        page.click(LOGOUT_ICON);
    }

    public void clickInteractButton() {
        page.click(INTERACT_BUTTON);
    }

    public void clickGoBackButton() {
        page.click(GO_BACK_BUTTON);
    }

    public void userDeletesDataSpace() {
        Locator optionsButton = page.locator(DATA_SPACE_OPTIONS_BUTTON).nth(0);

        if (optionsButton.isEnabled()) {
            optionsButton.click();
        } else {
            optionsButton = page.locator(DATA_SPACE_OPTIONS_BUTTON).nth(1);
            if (optionsButton.isEnabled()) {
                optionsButton.click();
            } else {
                throw new AssertionError("Both options buttons to delete dataspace are disabled.");
            }
        }

        page.click(DELETE_DATA_SPACE_BUTTON);
        page.click(YES_BUTTON);
    }
}
