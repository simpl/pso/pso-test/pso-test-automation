package framework.ui.pages.simplLabs;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;

import static framework.ui.locators.simplLabs.DataSpaceDetails.DATASPACE_DETAILS_APP;
import static framework.ui.locators.simplLabs.DataSpaceDetails.NEXT_BUTTON;

public class DataSpaceDetailsPage {
    private final Page page;

    public DataSpaceDetailsPage(Page page) {
        if (page == null) {
            throw new IllegalArgumentException("Page object cannot be null");
        }
        this.page = page;
    }

    public void checkDataSpaceDetailsPageOpened() {
        PlaywrightAssertions.assertThat(page.locator(DATASPACE_DETAILS_APP)).isVisible();
    }

    public void clickButtonNext() {
        page.locator(NEXT_BUTTON).click();
    }
}
