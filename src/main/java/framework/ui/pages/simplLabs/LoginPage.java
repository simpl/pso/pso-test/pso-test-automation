package framework.ui.pages.simplLabs;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;
import configuration.ui.data.simplLabs.LoginConfig;
import framework.ui.helpers.Utils;

import static framework.ui.locators.simplLabs.Login.*;

public class LoginPage {

    Page page;

    public LoginPage(Page page) {
        this.page = page;
    }

    private void fillGoogleAuthCode() {
        int retryCount = LoginConfig.RETRY_COUNT;
        for (int i = 0; i < retryCount; i++) {
            String totpCode = Utils.getGAuthCode();
            page.fill(AUTH_CODE_FIELD, totpCode);
            clickLoginButton();

            page.waitForTimeout(LoginConfig.SMALL_TIMEOUT);

            if (!page.isVisible(WRONG_CODE_ERROR)) {
                break;
            }

            page.waitForTimeout(LoginConfig.HIGH_TIMEOUT);
        }
    }

    public void loginIntoApplication(String email, String pass) {
        enterUserName(email);
        enterPassword(pass);
        clickLoginButton();
        fillGoogleAuthCode();
    }

    public void enterUserName(String email) {
        page.fill(USERNAME, email);
    }

    public void enterPassword(String pass) {
        page.fill(PASSWORD, pass);
    }

    public void clickLoginButton() {
        page.click(LOGIN_BUTTON);
    }

    public void checkTheLoginButtonIsVisible() {
        PlaywrightAssertions.assertThat(page.locator(LOGIN_BUTTON)).isVisible();
    }
}

