package framework.ui.pages.simplLabs;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;

import static framework.ui.locators.simplLabs.SelectTemplate.*;

public class SelectTemplatePage {
    private final Page page;

    public SelectTemplatePage(Page page) {
        this.page = page;
    }

    public void checkThePageSelectTemplateOpened() {
        PlaywrightAssertions.assertThat(page.locator(SELECT_TEMPLATE_APP)).isVisible();
    }

    public void selectFirstTemplate() {
        page.locator(SELECT_TEMPLATE_ICON).first().click();
    }

    public void clickButtonNext() {
        page.locator(NEXT_BUTTON).click();
    }

    public void clickUseThisTemplateButton() {
        page.locator(USE_THIS_TEMPLATE_BUTTON).click();
    }
}
