package framework.common;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.PlaywrightAssertions;
import com.microsoft.playwright.options.AriaRole;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Assertions {

    /**
     * This method asserts that two collections are equal.
     *
     * @param expected expected collection.
     * @param actual   actual collection.
     * @param <T>      collection type.
     */
    public static <T> void assertEqualCollections(Collection<T> expected, Collection<T> actual) {
        assertEquals("The collections are different in size: ", expected.size(), actual.size());
        expected.forEach(element -> assertTrue("Expected element " + element + " not found in actual collection",
                actual.contains(element)));
    }

    /**
     * This method verifies that a given date is in a wished range.
     *
     * @param startDate start date of the range.
     * @param endDate   end date of the range.
     * @param date      date that is wanted for verification.
     */
    public static void assertDateIsInRange(LocalDate startDate, LocalDate endDate, LocalDate date) {
        assertTrue("The chosen date is before the start of the range", date.isAfter(startDate) || date.equals(startDate));
        assertTrue("The chosen date is after the end of the range", date.isBefore(endDate) || date.equals(endDate));
    }

    /**
     * This method verifies that an element is visible.
     *
     * @param locator Locator object wanted for visibility verification.
     */
    public static void assertElementIsVisibleByLocator(Locator locator) {
        PlaywrightAssertions.assertThat(locator).isVisible();
    }

    /**
     * This method verifies that a page header has a certain text.
     *
     * @param page Playwright page.
     * @param text wished text.
     */
    public static void assertPageHeaderHasText(Page page, String text) {
        List<String> pageHeaders = page.getByRole(AriaRole.HEADING).allTextContents();
        assertTrue("Text " + text + " not present in any of the page headers",
                pageHeaders.stream().anyMatch(header -> header.equals(text)));
    }

    /**
     * This method asserts that an element is present in a collection.
     *
     * @param element    element expected to be in the collection.
     * @param collection collection where element is expected to be.
     * @param <T>        collection type.
     */
    public static <T> void assertElementIsPresentInCollection(T element, Collection<T> collection) {
        assertTrue("The element " + element + " is not present in the collection", collection.contains(element));
    }

    /**
     * This method verifies that an element is not visible.
     *
     * @param locator Locator object wanted for invisibility verification.
     */
    public static void assertElementIsNotVisibleByLocator(Locator locator) {
        PlaywrightAssertions.assertThat(locator).isHidden();
    }

    /**
     * This method verifies value expected is equals or less than the real value.
     *
     * @param valueExpected Value expected
     * @param value         Value retrieved from locator/element
     */
    public static void assertElementEqualsOrLess(Integer value, Integer valueExpected) {
        assertTrue("Value expected (" + valueExpected + ") is greater than " + value, valueExpected <= value);
    }

    /**
     * This method verifies value expected is equals or less than the real value.
     *
     * @param valueExpected Value expected
     * @param value         Value retrieved from locator/element
     */
    public static void assertElementEqualsOrGreater(Integer value, Integer valueExpected) {
        assertTrue("Value expected (" + valueExpected + ") is lesser than " + value, valueExpected >= value);
    }
}
