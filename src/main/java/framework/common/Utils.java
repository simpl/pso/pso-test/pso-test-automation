package framework.common;

import io.cucumber.java.Scenario;
import java.util.Random;

public final class Utils {

    private final Scenario scenario;

    public Utils(Scenario scenario) {
        this.scenario = scenario;
    }

    public static String generateRandomString() {
        final int letterA = 97;
        final int letterZ = 122;

        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(letterA, letterZ + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static String resolveRandomizedValue(String value) {
        if (value != null && value.startsWith("RANDOM_")) {
            return Utils.generateRandomString();
        }
        return value;
    }

    public static String modifyId(String originalId) {
        if (originalId == null || originalId.isEmpty()) {
            throw new IllegalArgumentException("Original ID is null or empty");
        }

        char lastChar = originalId.charAt(originalId.length() - 1);
        char newChar = (lastChar != '0') ? '0' : '1';
        return originalId.substring(0, originalId.length() - 1) + newChar;
    }
}
