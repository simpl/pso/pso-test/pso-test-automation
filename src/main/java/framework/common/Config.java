package framework.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Config {
    private static final Properties properties = new Properties();
    private static final Logger LOGGER = Logger.getLogger(Config.class.getName());

    private Config() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    static {
        try (InputStream input = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("config.properties")) {
            if (input == null) {
                throw new IllegalArgumentException(
                        "Sorry, unable to find config.properties");
            } else {
                properties.load(input);
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE,
                    "An error occurred while loading config.properties", ex);
        }
    }

    public static String get(String key) {
        return properties.getProperty(key);
    }
}
