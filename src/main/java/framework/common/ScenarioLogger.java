package framework.common;

import io.cucumber.java.Scenario;
import org.apache.commons.lang3.StringUtils;

import java.util.logging.Logger;

public class ScenarioLogger {

    private static final Logger LOGGER = Logger.getLogger(ScenarioLogger.class.getName());
    private final Scenario scenario;

    /**
     * Constructor to associate the logger with a specific Cucumber scenario.
     * @param scenario The Cucumber scenario to log to.
     */
    public ScenarioLogger(Scenario scenario) {
        this.scenario = scenario;
    }

    /**
     * Logs messages to the scenario report and console.
     * @param label The label for the log message.
     * @param message The actual log message.
     */
    public void logToScenario(String label, String message) {
        if (scenario != null) {
            scenario.attach(message, "text/plain", label);
        }
        LOGGER.info(() -> {
            assert scenario != null;
            return StringUtils.join(new String[]{label, message}, ": ");
        });
    }
}
