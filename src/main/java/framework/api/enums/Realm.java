package framework.api.enums;

public enum Realm {
    AUTHORITY("authority"),
    PARTICIPANT("participant");

    private final String name;

    Realm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
