package framework.api.enums;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum ApiEndpoint {

    // 🔹 Identity API
    IDENTITY_ATTRIBUTE("/sap-api/identity-attribute", true),
    IDENTITY_CERTIFICATE("/identity-api/certificate", true),
    PARTICIPANT_TYPE("/identity-api/participant", true),
    PARTICIPANT_SEARCH("/identity-api/participant/search", true),

    // 🔹 Onboarding API
    ONBOARDING_PARTICIPANT("/onboarding-api/participant-type", false),
    ONBOARDING_TEMPLATE("/onboarding-api/onboarding-template", true),
    ONBOARDING_REQUEST("/onboarding-api/onboarding-request", false),

    // 🔹 User API
    USER_ROLE("/user-api/role", true),
    USER_SEARCH("/user-api/user/search", true),
    USER_CREDENTIAL_MY_ID("/user-api/credential/my-id", true),
    USER_SESSION_CREDENTIAL("/user-api/session/credential", true),
    USER_CREDENTIAL_CREDENTIAL_ID("/user-api/credential/credential-id", true),
    USER_EPHFEMERAL_PROOF("/user-api/mtls/ephemeral-proof", true),

    // 🔹 Local Agent API
    IDENTITY_ATTRIBUTE_LOCAL_AGENT_SEARCH("/user-api/identity-attribute/search", true);

    private final String path;
    private final boolean isCrudTestable;
    private static final Map<String, ApiEndpoint> PATH_MAP = new HashMap<>();

    static {
        for (ApiEndpoint endpoint : values()) {
            PATH_MAP.put(endpoint.path.toLowerCase(), endpoint);
        }
    }

    ApiEndpoint(String path, boolean isCrudTestable) {
        this.path = path;
        this.isCrudTestable = isCrudTestable;
    }

    public String getPath() {
        return path;
    }

    public boolean isCrudTestable() {
        return isCrudTestable;
    }

    public static List<ApiEndpoint> getCrudTestableEndpoints() {
        return List.of(values()).stream()
                .filter(ApiEndpoint::isCrudTestable)
                .collect(Collectors.toList());
    }

    public static ApiEndpoint fromPath(String path) {
        ApiEndpoint endpoint = PATH_MAP.get(path.toLowerCase());
        if (endpoint == null) {
            throw new IllegalArgumentException("No endpoint found for path: " + path);
        }
        return endpoint;
    }

    @Override
    public String toString() {
        return path;
    }
}
