package framework.api.enums;

public enum MediaType {
    JSON("application/json"),
    TEXT("text/plain"),
    X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded");

    private final String value;

    MediaType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
