package framework.api.enums;

public enum HttpMethod {
    GET, POST, PUT, DELETE;

    public boolean isRequestBodyRequired() {
        return this == POST || this == PUT;
    }
}
