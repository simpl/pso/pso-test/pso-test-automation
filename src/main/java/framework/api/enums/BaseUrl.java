package framework.api.enums;

import framework.common.Config;

public final class BaseUrl {
    public static final String AUTHORITY = Config.get("URL_AUTHORITY_API");
    public static final String DATA_PROVIDER = Config.get("URL_DATA_PROVIDER_API");
    public static final String CONSUMER = Config.get("URL_CONSUMER_API");
}
