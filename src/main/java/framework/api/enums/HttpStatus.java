package framework.api.enums;

public enum HttpStatus {
    OK(200, "OK"),
    UPDATED_NO_CONTENT(204, "Updated"),
    CREATED(201, "Created"),
    DELETED(204, "Deleted"),
    BAD_REQUEST(400, "Bad Request"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not Found"),
    UNAUTHORIZED(401, "Unauthorized");

    private final int code;
    private final String description;

    HttpStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
