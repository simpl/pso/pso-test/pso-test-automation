package framework.api.services.securityattributesprovider.objects;

import java.util.List;
import java.util.Objects;

public class IdentityAttribute {

    private String id;
    private String code;
    private String name;
    private String description;
    private boolean assignableToRoles;
    private boolean enabled;
    private String creationTimestamp;
    private String updateTimestamp;
    private boolean used;
    private List<String> participantTypes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAssignableToRoles() {
        return assignableToRoles;
    }

    public void setAssignableToRoles(boolean assignableToRoles) {
        this.assignableToRoles = assignableToRoles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(String creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public String getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(String updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public List<String> getParticipantTypes() {
        return participantTypes;
    }

    public void setParticipantTypes(List<String> participantTypes) {
        this.participantTypes = participantTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentityAttribute that = (IdentityAttribute) o;
        return assignableToRoles == that.assignableToRoles && enabled == that.enabled && used == that.used &&
                Objects.equals(id, that.id) && Objects.equals(code, that.code) && Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(creationTimestamp, that.creationTimestamp) &&
                Objects.equals(updateTimestamp, that.updateTimestamp) &&
                Objects.equals(participantTypes, that.participantTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, description, assignableToRoles, enabled, creationTimestamp, updateTimestamp,
                used, participantTypes);
    }
}
