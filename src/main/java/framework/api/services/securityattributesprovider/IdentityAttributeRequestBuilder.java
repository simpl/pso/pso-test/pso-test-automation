package framework.api.services.securityattributesprovider;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import framework.common.Utils;

import java.util.List;
import java.util.Map;

public class IdentityAttributeRequestBuilder {

    private final JsonObject requestBody;

    public IdentityAttributeRequestBuilder() {
        this.requestBody = new JsonObject();
    }

    public IdentityAttributeRequestBuilder fromMap(Map<String, String> data) {
        String participantTypesRaw = data.getOrDefault("Participant Types", "");
        List<String> participantTypes = List.of(participantTypesRaw.split(";\\s*"));

        String code = Utils.resolveRandomizedValue(data.get("Code"));
        String name = Utils.resolveRandomizedValue(data.get("Name"));
        String description = Utils.resolveRandomizedValue(data.get("Description"));

        withCode(code)
                .withEnabled(Boolean.parseBoolean(data.getOrDefault("Enabled", "true")))
                .withAssignableToRoles(Boolean.parseBoolean(data.getOrDefault("Assignable to roles", "true")))
                .withName(name)
                .withDescription(description)
                .withParticipantTypes(participantTypes);

        return this;
    }

    public IdentityAttributeRequestBuilder withRandomData() {
        String randomCode = Utils.generateRandomString();
        String randomName = Utils.generateRandomString();
        String randomDescription = Utils.generateRandomString();
        List<String> randomParticipantTypes = List.of("DATA_PROVIDER", "CONSUMER");

        return this.withCode(randomCode)
                .withEnabled(true)
                .withAssignableToRoles(true)
                .withName(randomName)
                .withDescription(randomDescription)
                .withParticipantTypes(randomParticipantTypes);
    }

    public IdentityAttributeRequestBuilder withCode(String code) {
        requestBody.addProperty("code", code);
        return this;
    }

    public IdentityAttributeRequestBuilder withEnabled(boolean enabled) {
        requestBody.addProperty("enabled", enabled);
        return this;
    }

    public IdentityAttributeRequestBuilder withAssignableToRoles(boolean assignableToRoles) {
        requestBody.addProperty("assignableToRoles", assignableToRoles);
        return this;
    }

    public IdentityAttributeRequestBuilder withName(String name) {
        requestBody.addProperty("name", name);
        return this;
    }

    public IdentityAttributeRequestBuilder withDescription(String description) {
        requestBody.addProperty("description", description);
        return this;
    }

    public IdentityAttributeRequestBuilder withParticipantTypes(List<String> participantTypes) {
        JsonArray participantTypeArray = new JsonArray();
        participantTypes.forEach(participantTypeArray::add);
        requestBody.add("participantTypes", participantTypeArray);
        return this;
    }

    public JsonObject build() {
        return requestBody;
    }
}
