package framework.api.services.usersandroles;

import com.google.gson.JsonObject;
import framework.common.Utils;

import java.util.Map;

public class UserRolesRequestBuilder {

    private final JsonObject requestBody;

    public UserRolesRequestBuilder() {
        this.requestBody = new JsonObject();
    }

    public UserRolesRequestBuilder fromMap(Map<String, String> data) {
        String name = Utils.resolveRandomizedValue(data.get("name"));
        String description = Utils.resolveRandomizedValue(data.get("description"));

        withName(name).withDescription(description);

        return this;
    }

    public UserRolesRequestBuilder withName(String name) {
        requestBody.addProperty("name", name);
        return this;
    }

    public UserRolesRequestBuilder withDescription(String description) {
        requestBody.addProperty("description", description);
        return this;
    }

    public JsonObject build() {
        return requestBody;
    }
}
