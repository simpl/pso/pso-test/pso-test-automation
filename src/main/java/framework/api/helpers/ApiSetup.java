package framework.api.helpers;

import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.Playwright;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;

public class ApiSetup {

    private static Playwright playwright;
    private APIRequestContext apiContext;
    private Scenario scenario;

    public void setUp(String baseUrl, Scenario scenario) {
        this.scenario = scenario;
        this.playwright = Playwright.create();

        APIRequest.NewContextOptions options = new APIRequest.NewContextOptions().setBaseURL(baseUrl).setIgnoreHTTPSErrors(true);
        this.apiContext = playwright.request().newContext(options);

        if (scenario != null) {
            scenario.log("API base URL set to: " + baseUrl);
        }
    }

    public APIRequestContext getApiContext() {
        return apiContext;
    }

    public Scenario getScenario() {
        return scenario;
    }

    @After(value = "@api", order = 1)
    public void tearDown() {
        if (apiContext != null) {
            apiContext.dispose();
        }
        if (playwright != null) {
            playwright.close();
        }
    }
}
