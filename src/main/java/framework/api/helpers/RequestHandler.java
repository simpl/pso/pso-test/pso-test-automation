package framework.api.helpers;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.options.RequestOptions;
import framework.api.enums.HttpMethod;
import framework.api.enums.MediaType;
import framework.api.enums.Realm;
import framework.common.ScenarioLogger;
import io.cucumber.java.Scenario;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class RequestHandler {

    private final String baseUrl;
    private final APIRequestContext apiContext;
    private final Map<String, String> headers = new HashMap<>();
    private int statusCode;
    private final ScenarioLogger logger;
    private final List<Map<String, Object>> requestResponseLog;

    public RequestHandler(String baseUrl, APIRequestContext apiContext, Scenario scenario) {
        this.baseUrl = baseUrl;
        this.apiContext = apiContext;
        this.logger = new ScenarioLogger(scenario);
        this.requestResponseLog = new ArrayList<>();
        headers.put("Content-Type", MediaType.JSON.getValue());
    }

    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    public void removeHeader(String key) {
        headers.remove(key);
    }

    public String getAuthorizationToken(Realm realm, String username, String password) {
        Map<String, String> bodyParams = Map.of(
                "grant_type", "password",
                "client_id", "frontend-cli",
                "password", password,
                "username", username
        );
        String endpoint = String.format("/auth/realms/%s/protocol/openid-connect/token", realm);
        JsonObject response = this.sendUrlEncodedRequest(HttpMethod.POST, endpoint, bodyParams);

        if (response != null && response.has("access_token")) {
            return response.get("access_token").getAsString();
        } else {
            throw new RuntimeException("Access token not found in response");
        }
    }

    public JsonObject sendRequest(HttpMethod method, String endpoint) {
        return sendRequest(method, endpoint, null, MediaType.JSON);
    }

    public JsonObject sendRequest(HttpMethod method, String endpoint, JsonObject bodyRequest) {
        return sendRequest(method, endpoint, bodyRequest, MediaType.JSON);
    }

    public JsonObject sendRequest(HttpMethod method, String endpoint, String bodyRequest) {
        logger.logToScenario("Request Body", bodyRequest);
        return sendRequest(method, endpoint, bodyRequest, MediaType.JSON);
    }

    public JsonObject sendUrlEncodedRequest(HttpMethod method, String endpoint, Map<String, String> data) {
        String encodedData = encodeFormData(data);
        return sendRequest(method, endpoint, encodedData, MediaType.X_WWW_FORM_URLENCODED);
    }

    private JsonObject sendRequest(HttpMethod method, String endpoint, Object body, MediaType mediaType) {
        String url = baseUrl + endpoint;
        RequestOptions requestOptions = prepareRequestOptions(body, mediaType);

        APIResponse response = executeRequest(method, url, requestOptions);

        String responseBody = response.text();
        JsonObject jsonResponse = null;

        if (responseBody != null && !responseBody.isEmpty()) {
            jsonResponse = JsonParser.parseString(responseBody).getAsJsonObject();
        }

        Map<String, Object> logEntry = new HashMap<>();
        logEntry.put("method", method);
        logEntry.put("endpoint", endpoint);
        logEntry.put("requestBody", body);
        logEntry.put("responseBody", jsonResponse);
        logEntry.put("statusCode", response.status());
        requestResponseLog.add(logEntry);

        return jsonResponse;
    }

    private RequestOptions prepareRequestOptions(Object body, MediaType mediaType) {
        RequestOptions requestOptions = RequestOptions.create();
        headers.forEach(requestOptions::setHeader);
        requestOptions.setHeader("Content-Type", mediaType.getValue());

        if (body != null) {
            if (body instanceof String) {
                requestOptions.setData((String) body);
            } else if (body instanceof JsonObject) {
                requestOptions.setData((JsonObject) body);
            }
        }

        return requestOptions;
    }

    private String encodeFormData(Map<String, String> data) {
        return data.entrySet().stream()
                .map(entry -> URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8) + "=" +
                        URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8))
                .collect(Collectors.joining("&"));
    }

    public List<Map<String, Object>> getRequestResponseLog() {
        return requestResponseLog;
    }

    public Map<String, Object> getLastRequestResponse() {
        if (requestResponseLog.isEmpty()) {
            throw new IllegalStateException("No requests have been logged yet.");
        }
        return requestResponseLog.get(requestResponseLog.size() - 1);
    }

    public JsonObject getLastRequestBody() {
        if (requestResponseLog.isEmpty()) {
            throw new IllegalStateException("No requests have been logged yet.");
        }

        Map<String, Object> lastLog = requestResponseLog.get(requestResponseLog.size() - 1);
        return JsonParser.parseString(lastLog.get("requestBody").toString()).getAsJsonObject();
    }

    public JsonObject getLastResponseBody() {
        if (requestResponseLog.isEmpty()) {
            throw new IllegalStateException("No requests have been logged yet.");
        }

        return (JsonObject) requestResponseLog.get(requestResponseLog.size() - 1).get("responseBody");
    }

    public int getLastStatusCode() {
        if (requestResponseLog.isEmpty()) {
            throw new IllegalStateException("No requests have been logged yet.");
        }
        return (int) requestResponseLog.get(requestResponseLog.size() - 1).get("statusCode");
    }

    private APIResponse executeRequest(HttpMethod method, String endpoint, RequestOptions requestOptions) {
        logger.logToScenario("Request URL", endpoint);

        APIResponse response = switch (method) {
            case POST -> apiContext.post(endpoint, requestOptions);
            case GET -> apiContext.get(endpoint, requestOptions);
            case PUT -> apiContext.put(endpoint, requestOptions);
            case DELETE -> apiContext.delete(endpoint, requestOptions);
            default -> throw new IllegalArgumentException("Unsupported HTTP method: " + method);
        };
        statusCode = response.status();

        logger.logToScenario("Response Status", String.valueOf(response.status()));
        logger.logToScenario("Response Body", response.text());
        return response;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
