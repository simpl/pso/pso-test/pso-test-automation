package configuration.ui.data.simplLabs;
import framework.common.Config;

public final class LoginConfig {
    public static final String URL = Config.get("URL");
    public static final String USERNAME = Config.get("USERNAME");
    public static final String PASSWORD = Config.get("PASSWORD");
    public static final String SECRET_KEY = Config.get("SECRET_KEY");
    public static final int RETRY_COUNT = 5;
    public static final int SMALL_TIMEOUT = 1000;
    public static final int HIGH_TIMEOUT = 7000;

    private LoginConfig() {
        throw new UnsupportedOperationException(
                "LoginConfig is a utility class and cannot be instantiated");
    }
}
