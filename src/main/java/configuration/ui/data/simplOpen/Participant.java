package configuration.ui.data.simplOpen;

import framework.common.Config;

public class Participant {

    public static final String URL_CONSUMER = Config.get("URL_CONSUMER");
    public static final String URL_KEYCLOAK_CONSUMER = Config.get("URL_KEYCLOAK_CONSUMER");
    public static final String URL_DATA_PROVIDER = Config.get("URL_DATA_PROVIDER");
    public static final String URL_KEYCLOAK_DATA_PROVIDER = Config.get("URL_KEYCLOAK_DATA_PROVIDER");
    public static final String HOST_PARTICIPANT_UTILITY = "participant-utility";
    public static final String HOST_ECHO = "echo";
    public static final String CONSUMER_T1UAR_M_USER = "consumer_role_t1uar_m_auto";
    public static final String CONSUMER_ONBOARDER_M_USER = "consumer_role_onboarder_m_auto";
    public static final String CONSUMER_SD_CONSUMER_USER = "consumer_role_sd_consumer_auto";
    public static final String AUTHORITY_NO_ROLES_USER = "authority_role_no_roles_auto";
    public static final String CONSUMER_NO_ROLES_USER = "consumer_role_no_roles_auto";
    public static final String DATA_PROVIDER_NO_ROLES_USER = "dataprovider_role_no_roles_auto";
    public static final String ECHO_BUTTON_TEXT = "Echo";
    public static final String UPLOAD_CREDENTIAL_BUTTON_TEXT = "Upload credential";
    public static final String ECHO_PAGE_HEADER = "/echo";
    public static final String ONBOARDED_CONSUMER_ID = "01953d82-5efb-7fd9-97bd-074ced88b7b8";
    public static final String ONBOARDED_DATA_PROVIDER_ID = "01953d7a-a1cd-7781-81a0-463d2f009f91";
    public static final String DATA_PROVIDER_T1UAR_M_USER = "dataprovider_role_t1uar_m_auto";
    public static final String DATA_PROVIDER_ONBOARDER_M_USER = "dataprovider_role_onboarder_m_auto";
    public static final String DATA_PROVIDER_SD_PUBLISHER_USER = "dataprovider_role_sd_publisher_auto";

}
