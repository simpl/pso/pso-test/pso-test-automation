package configuration.ui.data.simplOpen;

import framework.common.Config;

public class Authority {
    public static final String URL_AUTHORITY = Config.get("URL_AUTHORITY");
    public static final String URL_KEYCLOAK_AUTHORITY = Config.get("URL_KEYCLOAK_AUTHORITY");
    public static final String PDF_FILE_PATH = "simplOpen/dummy_pdf.pdf";
    public static final String HOST_ONBOARDING = "onboarding/";
    public static final String HOST_ADMINISTRATION = "administration";
    public static final String HOST_APPLICATION = "application/info";
    public static final String HOST_APPLICATION_ONBOARDING_STATUS = "application/onboarding-status";
    public static final String HOST_ADDITIONAL_REQUEST = "application/additional-request";
    public static final String HOST_IDENTITY_ATTRIBUTE_CREATION = "_new";
    public static final String HOST_IDENTITY_ATTRIBUTE = "sap/identity-attributes/";
    public static final String ORGANISATION_INPUT_VALUE = "pso";
    public static final String SURNAME_INPUT_VALUE = "test";
    public static final String NAME_INPUT_VALUE = "automation";
    public static final String USERNAME_INPUT_VALUE = "testAutomation";
    public static final String PASSWORD_INPUT_VALUE = "Testing2@14";
    public static final String HOST_MANAGEMENT_PARTICIPANT = "management/participant";
    public static final String AUTHORITY_NOTARY_USER = "authority_role_notary_auto";
    public static final String AUTHORITY_IATTR_M_USER = "authority_role_iattr_m_auto";
    public static final String AUTHORITY_T1UAR_M_USER = "authority_role_t1uar_m_auto";
    public static final String AUTHORITY_T2IAA_M_USER = "authority_role_t2iaa_m_auto";
    public static final String GENERAL_PASSWORD = "password";
    public static final String ONBOARDING_DATAPARTICIPANT_REQUEST_SUBMITTED_USER = "dataparticipant_request_submitted@automation.com";
    public static final String ONBOARDING_DATAPARTICIPANT_REQUEST_INPROGRESS_USER = "dataparticipant_request_inprogress@psoautomation.com";
    public static final String ONBOARDING_DATAPARTICIPANT_REQUEST_ACCEPTED_USER = "dataparticipant_request_accepted@automation.com";
    public static final String ONBOARDING_DATAPARTICIPANT_REQUEST_REJECTED_USER = "dataparticipant_request_rejected@automation.com";
    public static final String ONBOARDING_DATAPARTICIPANT_PASSWORD = "Testing2@14";
    public static final String USER_PSO_SIMPL_3049_IDENTIFIER = "019546d1-b5d4-7932-a094-b9f81eb555c5";
    public static final String USER_PSO_SIMPL_3049_ONBOARDING_DATE = "Thu Feb 27 2025";
    public static final String USER_PSO_SIMPL_3049_CREDENTIALS_EXPIRATION_DATE = "-";
    public static final String REQUESTS_LIST_TITLE = "Request list";
    public static final String REQUESTS_LIST_TABLE_REQUEST_STATUS_COLUMN = "Request status";
    public static final String REQUESTS_LIST_TABLE_REQUESTER_EMAIL_COLUMN = "Requester email";
    public static final String REQUESTS_LIST_TABLE_PARTICIPANT_TYPE_COLUMN = "Participant type";
    public static final String REQUESTS_LIST_TABLE_REQUEST_DATE_COLUMN = "Request date";
    public static final String REQUESTS_LIST_TABLE_LAST_CHANGE_DATE_COLUMN = "Last change date";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_NAME_COLUMN = "Name";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_CODE_COLUMN = "Code";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_ASSIGNABLE_COLUMN = "Assignable";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_ENABLED_COLUMN = "Enabled";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_CREATION_DATE_COLUMN = "Creation date";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_LAST_CHANGE_DATE_COLUMN = "Last change date";
    public static final String PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_ACTIONS = "Actions";
    public static final String IDENTITY_ATTRIBUTES_DETAILS_HEADER = "Identity attributes details";
    public static final String IDENTITY_ATTRIBUTES_LIST_HEADER = "Attributes list";
    public static final String PARTICIPANT_PAGE_PARTICIPANT_LIST_HEADER = "Participant List";
    public static final String PARTICIPANT_PAGE_FILTER_BUTTON = "Filter";
    public static final String PARTICIPANT_PAGE_RESET_FILTER_BUTTON = "Reset";
    public static final String PARTICIPANT_PAGE_APPLY_FILTER_BUTTON = "Apply";
    public static final String PARTICIPANT_PAGE_PARTICIPANT_NAME_COLUMN = "Participant Name";
    public static final String PARTICIPANT_PAGE_PARTICIPANT_TYPE_COLUMN = "Participant type";
    public static final String PARTICIPANT_PAGE_ONBOARDING_DATE_COLUMN = "Onboarding Date";
    public static final String PARTICIPANT_PAGE_ACTIONS_COLUMN = "Actions";
    public static final String ERROR_MESSAGE = "Invalid username or password.";
    public static final String PARTICIPANT_PAGE_FILTER_SELECT_COLUMN = "Select column";
    public static final String PARTICIPANT_LIST_PAGE_HEADER = "Participant List";
    public static final String NEW_ATTRIBUTE_HEADER = "New attribute";
    public static final String SUCCESSFUL_IDENTITY_ATTRIBUTE_CREATION_ALERT_MESSAGE = "You successfully created an attribute";
    public static final String SUCCESSFUL_IDENTITY_ATTRIBUTE_EDITION_ALERT_MESSAGE = "You successfully edited an attribute";
    public static final String SUCCESSFUL_IDENTITY_ATTRIBUTE_DELETION_ALERT_MESSAGE = "identityAttributesManagementPage.deleteIdentityAttribueSuccess";
    public static final String SUCCESSFUL_REQUEST_SENT_ALERT_MESSAGE = "Request sent successfully.";
    public static final String ROLE_RESTRICTED = "Role Restricted Access";
    public static final String ACCESS_DENIED = "Access denied. Your role does not grant permission to view this content.";
    public static final String ALERT_ERROR_CREATING_CREDENTIAL_LOCATOR = "A participant with this data is already present, check email, organisation or username";
    public static final String ALERT_DISPLAYING_SUCCESSFUL_APPROVED = "You successfully approved this request. A set of credentials was created for the applicant.";
    public static final String ALERT_DISPLAYING_SUCCESSFUL_REJECTED = "You successfully reject this request.";

    public static final String[] PARTICIPANT_TYPES = {
        "Application Provider",
        "Data Provider",
        "Consumer",
        "Infrastructure Provider"
    };

    public Authority() {
    }
}
