package configuration.ui.data.simplOpen;

import framework.common.Config;

public final class Wizard {
    public static final String URL_WIZARD = Config.get("URL_WIZARD");
    public static final String FILE_PATH = "simplOpen/input-shacl-file.ttl";
    public static final String AGE = "30";
    public static final String NAME = "Peter";
    public static final String BIRTH_DATE = "01/01/1990";
    public static final String GENERAL_SERVICE_PROPERTIES_SHAPE_DROPDOWN = "general service properties - " +
            "GeneralServicePropertiesShape";
    public static final String GENERAL_SERVICE_PROPERTIES_NAME_PLACEHOLDER = "A short text title for the " +
            "dataset/application/infrastructure";
    public static final String GENERAL_SERVICE_PROPERTIES_DESCRIPTION_PLACEHOLDER = "A description for the " +
            "dataset/application/infrastructure";
    public static final String GENERAL_SERVICE_PROPERTIES_SERVICE_ACCESS_POINT_PLACEHOLDER = "The location where the " +
            "dataset/application/infrastructure can be found (likely an endpoint URL)";
    public static final String GENERAL_SERVICE_PROPERTIES_KEYWORDS_PLACEHOLDER = "Keywords";
    public static final String GENERAL_SERVICE_PROPERTIES_IN_LANGUAGE_PLACEHOLDER = "in language";
    public static final String DATA_PROPERTIES_SHAPE_DROPDOWN = "data properties - DataPropertiesShape";
    public static final String DATA_PROPERTIES_PRODUCED_BY_PLACEHOLDER = "Provenance";
    public static final String DATA_PROPERTIES_FORMAT_PLACEHOLDER = "Format under which the data is distributed (e.g." +
            " csv, xml, …).";
    public static final String DATA_PROPERTIES_ADDITIONAL_INFO_PLACEHOLDER = "Additional Information about the " +
            "dataset";
    public static final String DATA_PROPERTIES_RELATED_DATASETS_PLACEHOLDER = "Related datasets";
    public static final String DATA_PROPERTIES_TARGET_USERS_PLACEHOLDER = "Target users";
    public static final String DATA_PROPERTIES_DATA_QUALITY_PLACEHOLDER = "Data Quality (to include metrics such as " +
            "completeness, accuracy, timeliness and other)";
    public static final String DATA_PROPERTIES_ENCRYPTION_PLACEHOLDER = "Encryption: Describes the encryption " +
            "algorithms and keys used to secure the data";
    public static final String DATA_PROPERTIES_ANONYMIZATION_PLACEHOLDER = "Anonymization/pseudonymization: Indicates" +
            " whether sensitive information has been anonymized or pseudonymized to protect privacy";
    public static final String PROVIDER_INFORMATION_SHAPE_DROPDOWN = "provider information - ProviderInformationShape";
    public static final String PROVIDER_INFORMATION_PROVIDED_BY_PLACEHOLDER = "A reference to ID of the " +
            "Data/Application/Infrastructure Provider";
    public static final String PROVIDER_INFORMATION_CONTACT_PLACEHOLDER = "Who to contact in case of questions/issues";
    public static final String PROVIDER_INFORMATION_SIGNATURE_PLACEHOLDER = "A digital signature that ensures that " +
            "the provider is the one that uploaded the Self-description";
    public static final String OFFERING_PRICE_SHAPE_DROPDOWN = "offering price - OfferingPriceShape";
    public static final String OFFERING_PRICE_LICENSE_PLACEHOLDER = "URL to the used license";
    public static final String OFFERING_PRICE_CURRENCY_PLACEHOLDER = "currency";
    public static final String OFFERING_PRICE_PRICE_PLACEHOLDER = "Price and conditions";
    public static final String OFFERING_PRICE_PRICE_TYPE_PLACEHOLDER = "price type";
    public static final String SERVICE_POLICY_SHAPE_DROPDOWN = "service policy - ServicePolicyShape";
    public static final String SERVICE_POLICY_ACCESS_POLICY_PLACEHOLDER = "Access policy to define who can access the" +
            " dataset";
    public static final String SERVICE_POLICY_USAGE_POLICY_PLACEHOLDER = "Usage policy to define how a dataset can be" +
            " used";
    public static final String SERVICE_POLICY_DATA_PROTECTION_REGIME_PLACEHOLDER = "Indicates compliance with " +
            "relevant data protection regulations and standards";
    public static final String CONTRACT_TEMPLATE_SHAPE_DROPDOWN = "contract template - ContractTemplateShape";
    public static final String CONTRACT_TEMPLATE_DOCUMENT_PLACEHOLDER = "contract template document";
    public static final String BILLING_SCHEMA_SHAPE_DROPDOWN = "billing schema - BillingSchemaShape";
    public static final String BILLING_SCHEMA_DOCUMENT_PLACEHOLDER = "billing schema document";
    public static final String SLA_AGREEMENTS_SHAPE_DROPDOWN = "sla agreements - SlaAgreementsShape";
    public static final String SLA_AGREEMENTS_DOCUMENT_PLACEHOLDER = "sla agreements document";

    private Wizard() {
        throw new UnsupportedOperationException(
                "Wizard is a utility class and cannot be instantiated");
    }
}
