package testRunner.simplLabs;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/ui/simplLabs",
        glue = {"stepDefinitions/ui/simplLabs", "framework/ui"},
        plugin = {"pretty", "json:target/cucumber-reports/cucumber.json"},
        monochrome = true
)

public class UiSimplLabsRunner {
}
