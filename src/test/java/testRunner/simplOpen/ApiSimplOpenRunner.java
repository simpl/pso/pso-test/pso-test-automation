package testRunner.simplOpen;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/api/simplOpen",
        glue = {"stepDefinitions/api/simplOpen", "framework/api"},
        plugin = {"pretty", "json:target/cucumber-reports/cucumber.json"},
        monochrome = true
)

public class ApiSimplOpenRunner {
}
