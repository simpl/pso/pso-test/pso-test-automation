package testRunner.simplOpen;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features/ui/simplOpen",
        glue = {"stepDefinitions/ui/simplOpen", "framework/ui"},
        plugin = {"pretty", "json:target/cucumber-reports/cucumber.json"},
        monochrome = true
)
public class UiSimplOpenRunner {

}
