@Participant @regression @ui
Feature: Participant

  @TCP01 @SIMPL-1094 @SIMPL-5102 @SIMPL-672 @SIMPL-4439 @SIMPL-4841 @SIMPL-4842
  Scenario Outline: Display of information for a Participant in the Echo Page: <caseName>
    Given the user navigates to the "<participantUtility>" page
    And the user is redirected to the "<keycloak>" keycloak
    And the user logs in with a user with "<role>" role
    And the user sees the "Echo" button
    When the user clicks on the "Echo" button
    Then the Echo page is displayed
    And the following information about the user's identity token is displayed:
      | Username                     | <username>                   |
      | Email Address                | <emailAddress>               |
      | Common name                  | <commonName>                 |
      | Organisation                 | <organisation>               |
      | Participant Type             | <participantType>            |
      | Assigned identity attributes | <assignedIdentityAttributes> |
    And the connection status is "CONNECTED"
    And the mTLS status is "SECURED"

    Examples:
      | caseName                                    | participantUtility                | keycloak      | role                       | username                            | emailAddress                                 | commonName                 | organisation | participantType | assignedIdentityAttributes |
      | Consumer - User with role: ONBOARDER_M      | Consumer Participant Utility      | Consumer      | CONSUMER_ONBOARDER_M       | consumer_role_onboarder_m_auto      | consumer_role_onboarder_m_auto@test.com      | ONBOARDED_CONSUMER_ID      | PSO          | CONSUMER        |                            |
      | Consumer - User with role: CATALOG_R        | Consumer Participant Utility      | Consumer      | CONSUMER_SD_CONSUMER       | consumer_role_sd_consumer_auto      | consumer_role_sd_consumer_auto@test.com      |                            |              |                 |                            |
      | Consumer - User with role: T1UAR_M          | Consumer Participant Utility      | Consumer      | CONSUMER_T1UAR_M           | consumer_role_t1uar_m_auto          | consumer_role_t1uar_m_auto@test.com          |                            |              |                 |                            |
      | Consumer - User with no roles assigned      | Consumer Participant Utility      | Consumer      | CONSUMER NO ROLE           | consumer_role_no_roles_auto         | consumer_role_no_roles_auto@test.com         |                            |              |                 |                            |
      | Data Provider - User with role: ONBOARDER_M | Data Provider Participant Utility | Data Provider | DATA_PROVIDER_ONBOARDER_M  | dataprovider_role_onboarder_m_auto  | dataprovider_role_onboarder_m_auto@test.com  | ONBOARDED_DATA_PROVIDER_ID | PSO          | DATA PROVIDER   |                            |
      | Data Provider - User with role: CATALOG_R   | Data Provider Participant Utility | Data Provider | DATA_PROVIDER_SD_PUBLISHER | dataprovider_role_sd_publisher_auto | dataprovider_role_sd_publisher_auto@test.com |                            |              |                 |                            |
      | Data Provider - User with role: T1UAR_M     | Data Provider Participant Utility | Data Provider | DATA_PROVIDER_T1UAR_M      | dataprovider_role_t1uar_m_auto      | dataprovider_role_t1uar_m_auto@test.com      |                            |              |                 |                            |
      | Data Provider - User with no roles assigned | Data Provider Participant Utility | Data Provider | DATA PROVIDER NO ROLE      | dataprovider_role_no_roles_auto     | dataprovider_role_no_roles_auto@test.com     |                            |              |                 |                            |

  @TCP02 @SIMPL-1100
  Scenario Outline: Unauthorized user navigates to the Participant Utility Page: <caseName>
    Given the user navigates to the "<participantUtility>" page
    And the user is redirected to the "<keycloak>" keycloak
    When the user tries to log in with invalid login credentials
    Then the user sees an error message displayed

    Examples:
      | caseName                          | participantUtility                | keycloak      |
      | Consumer Participant Utility      | Consumer Participant Utility      | Consumer      |
      | Data Provider Participant Utility | Data Provider Participant Utility | Data Provider |