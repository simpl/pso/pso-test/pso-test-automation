@wizard @regression @ui
Feature: Wizard

  @TCW01 @SIMPL-1018 @smoke
  Scenario: The upload widget is visible successfully
    Given the user logs in to the SD Wizard
    When the user clicks on get started button
    Then upload widget should be visible
    And the information select a SHACL shape or Upload should be displayed
    And the Select Ecosystem dropdown menu options are displayed

  @TCW02 @SIMPL-1020 @SIMPL-1022 @smoke
  Scenario: File Upload and Displaying of Complete Person Form
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the file selection dialog box is visible
    And the user clicks the Upload button from the widget
    And the user clicks the Choose file button
    And the user selects a .ttl file
    And the file name is visible in the upload widget
    When the user clicks the upload button from the Upload Shape
    Then the Complete Person Form is displayed

  @TCW04 @SIMPL-1024 @SIMPL-1025
  Scenario: Complete The Compliance Review Body Form and The Save Button is Enabled
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the file selection dialog box is visible
    And the user clicks the Upload button from the widget
    And the user clicks the Choose file button
    And the user selects a .ttl file
    And the file name is visible in the upload widget
    When the user clicks the upload button from the Upload Shape
    And the Complete Person Form is displayed
    And the user fills in the Compliance Review Body Form
    Then the Save button is enabled

  @TCW05 @SIMPL-1019
  Scenario: Upload a File Using Upload Button Link on Select a SHACL or Upload Format
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the file selection dialog box is visible
    And the user clicks the Upload button link on Select a SHACL or Upload format
    And the user clicks the Choose file button
    And the user selects a .ttl file
    And the file name is visible in the upload widget
    When the user clicks the upload button from the Upload Shape
    Then the Complete Person Form is displayed

  @TCW06 @SIMPL-1050 @bug:SIMPL-6438
  Scenario: Check if the JSON file is downloaded
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the file selection dialog box is visible
    And the user clicks the Upload button from the widget
    And the user clicks the Choose file button
    And the user selects a .ttl file
    And the file name is visible in the upload widget
    When the user clicks the upload button from the Upload Shape
    And the Complete Person Form is displayed
    And the user fills in the Compliance Review Body Form
    And the Save button is enabled
    Then the user clicks the Save button
    And the JSON file is downloaded

  @TCW07 @SIMPL-3643 @smoke
  Scenario: Data Offering and Application Offering schema form are visible successfully
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And upload widget should be visible
    And the information select a SHACL shape or Upload should be displayed
    And the Select Ecosystem dropdown menu options are displayed
    When the user clicks the SIMPL option
    And the wizard displays on Service Offering shapes these shapes
    Then the user sees the Data Offering shape
    And the user sees the Application Offering shapes

  @TCW08 @SIMPL-3642 @smoke
  Scenario: The body of the upload widget inside the wizard is visible successfully
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And upload widget should be visible
    And the information select a SHACL shape or Upload should be displayed
    And the Select Ecosystem dropdown menu options are displayed
    When the user clicks the SIMPL option
    And the wizard displays Upload button on the right side
    Then the wizard displays the Participant shapes section
    And the wizard displays the Service Offering shapes section
    And the wizard displays the Resource shapes section

  @TCW09 @SIMPL-3877 @SIMPL-4162
  Scenario Outline: Validation of the fields for a Data Offering Self-Description form: <caseName>
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the user selects the "SIMPL" ecosystem
    And the user clicks on the "Data Offering" service offering shape
    When the user fills the Data Offering fields with the following information:
      | General Service Properties - Name                 | <name>                     |
      | General Service Properties - Description          | <description>              |
      | General Service Properties - Service Access Point | <serviceAccessPoint>       |
      | General Service Properties - Keywords             | <keywords>                 |
      | General Service Properties - In Language          | <inLanguage>               |
      | Data Properties - Produced By                     | <producedBy>               |
      | Data Properties - Format                          | <format>                   |
      | Data Properties - Additional Info                 | <additionalInfo>           |
      | Data Properties - Related Datasets                | <relatedDatasets>          |
      | Data Properties - Target Users                    | <targetUsers>              |
      | Data Properties - Data Quality                    | <dataQuality>              |
      | Data Properties - Encryption                      | <encryption>               |
      | Data Properties - Anonymization                   | <anonymization>            |
      | Provider Information - Provided By                | <providedBy>               |
      | Provider Information - Contact                    | <contact>                  |
      | Provider Information - Signature                  | <signature>                |
      | Offering Price - License                          | <license>                  |
      | Offering Price - Currency                         | <currency>                 |
      | Offering Price - Price                            | <price>                    |
      | Offering Price - Price Type                       | <priceType>                |
      | Service Policy - Access Policy                    | <accessPolicy>             |
      | Service Policy - Usage Policy                     | <usagePolicy>              |
      | Service Policy - Data Protection Regime           | <dataProtectionRegime>     |
      | Contract Template Document                        | <contractTemplateDocument> |
      | Billing Schema Document                           | <billingSchemaDocument>    |
      | SLA Agreements Document                           | <slaAgreementsDocument>    |
    Then the "Save" button is <enabled>

    Examples:
      | caseName                                                            | name | description       | serviceAccessPoint               | keywords | inLanguage | producedBy   | format | additionalInfo | relatedDatasets | targetUsers | dataQuality       | encryption     | anonymization | providedBy | contact      | signature | license                | currency | price | priceType | accessPolicy | usagePolicy | dataProtectionRegime | contractTemplateDocument | billingSchemaDocument | slaAgreementsDocument | enabled     |
      | Mandatory fields left empty                                         |      |                   |                                  | TEST     |            | pso@test.com |        |                | Test Dataset    | Test Users  | Test Data Quality | Test Algorithm | Yes           |            |              |           |                        |          |       |           |              |             | Test Regime          |                          |                       |                       | not enabled |
      | General Service Properties: Service Access Point different from URL | PSO  | PSO Data Offering | not-an-url                       | TEST     | en         | pso@test.com | json   |                | Test Dataset    | Test Users  | Test Data Quality | Test Algorithm | Yes           | PSO        | pso@test.com | PSO       | https://psolicense.com | EUR      | 0     | free      | Free         | Free        | Test Regime          | Contract Template 1      | Billing Schema 1      | Sla Agreements 1      | not enabled |
      | Data Properties: Produced By field different from e-mail            | PSO  | PSO Data Offering | http://psoserviceaccesspoint.com | TEST     | en         | not-an-email | json   |                | Test Dataset    | Test Users  | Test Data Quality | Test Algorithm | Yes           | PSO        | pso@test.com | PSO       | https://psolicense.com | EUR      | 0     | free      | Free         | Free        | Test Regime          | Contract Template 1      | Billing Schema 1      | Sla Agreements 1      | not enabled |
      | Provider Information: Contact field different from e-mail           | PSO  | PSO Data Offering | http://psoserviceaccesspoint.com | TEST     | en         | pso@test.com | json   |                | Test Dataset    | Test Users  | Test Data Quality | Test Algorithm | Yes           | PSO        | not-an-email | PSO       | https://psolicense.com | EUR      | 0     | free      | Free         | Free        | Test Regime          | Contract Template 1      | Billing Schema 1      | Sla Agreements 1      | not enabled |
      | Offering Price: License field different from URL                    | PSO  | PSO Data Offering | http://psoserviceaccesspoint.com | TEST     | en         | pso@test.com | json   |                | Test Dataset    | Test Users  | Test Data Quality | Test Algorithm | Yes           | PSO        | pso@test.com | PSO       | not-an-url             | EUR      | 0     | free      | Free         | Free        | Test Regime          | Contract Template 1      | Billing Schema 1      | Sla Agreements 1      | not enabled |
      | All fields correctly filled                                         | PSO  | PSO Data Offering | http://psoserviceaccesspoint.com | TEST     | en         | pso@test.com | json   |                | Test Dataset    | Test Users  | Test Data Quality | Test Algorithm | Yes           | PSO        | pso@test.com | PSO       | https://psolicense.com | EUR      | 0     | free      | Free         | Free        | Test Regime          | Contract Template 1      | Billing Schema 1      | Sla Agreements 1      | enabled     |

  @TCW10 @SIMPL-4232 @SIMPL-4161
  Scenario Outline: Verify if the user can choose the new <Schemas> schemas
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the Select Ecosystem dropdown menu options are displayed
    And the user selects the "SIMPL" ecosystem
    And the user clicks on the "Data Offering" service offering shape
    When the user clicks on the "<Schemas>" dropdown
    Then the user verifies the "<Schemas>" schemas are available

    Examples:
      | Schemas               |
      | ContractTemplateShape |
      | BillingSchemaShape    |
      | SlaAgreementsShape    |

  @TCW11 @SIMPL-4233 @SIMPL-4163 @SIMPL-4164
  Scenario: Verification of Contract Template, SLA Agreements and Billing Schema documents correctly stored in the Json for a Data Offering Self-Description
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the user selects the "SIMPL" ecosystem
    And the user clicks on the "Data Offering" service offering shape
    When the user fills the "Data Offering" fields with mandatory fields and the following info:
      | Contract Template Document | Contract Template 2 |
      | Billing Schema Document    | Billing Schema 2    |
      | SLA Agreements Document    | Sla Agreements 2    |
    And the user clicks the "Save" button
    Then the user checks the following schemas have been added successfully in "Data Offering" document:
      | Contract Template Document | Contract Template 2 |
      | Billing Schema Document    | Billing Schema 2    |
      | SLA Agreements Document    | Sla Agreements 2    |

  @TCW12 @SIMPL-4234
  Scenario Outline: Hash verification for Contract Template, SLA Agreements and Billing Schema documents in a Data Offering Self-Description Json: <caseName>
    Given the user logs in to the SD Wizard
    And the user clicks on get started button
    And the user selects the "SIMPL" ecosystem
    And the user clicks on the "Data Offering" service offering shape
    When the user fills the "Data Offering" fields with mandatory fields and the following info:
      | Contract Template Document | <contractTemplateDocument> |
      | Billing Schema Document    | <billingSchemaDocument>    |
      | SLA Agreements Document    | <slaAgreementsDocument>    |
    And the user clicks the "Save" button
    Then the hash value for the following documents is correctly calculated according to the algorithm stated in the "Data Offering" file:
      | Contract Template Document | <contractTemplateDocument> |
      | Billing Schema Document    | <billingSchemaDocument>    |
      | SLA Agreements Document    | <slaAgreementsDocument>    |

    Examples:
      | caseName                                                                              | contractTemplateDocument | billingSchemaDocument | slaAgreementsDocument |
      | Contract Template Document 1, Billing Schema Document 1 and SLA Agreements Document 1 | Contract Template 1      | Billing Schema 1      | Sla Agreements 1      |
      | Contract Template Document 2, Billing Schema Document 2 and SLA Agreements Document 2 | Contract Template 2      | Billing Schema 2      | Sla Agreements 2      |
      | Contract Template Document 3, Billing Schema Document 3 and SLA Agreements Document 3 | Contract Template 3      | Billing Schema 3      | Sla Agreements 3      |