@Authority @regression @ui
Feature: Authority

  @TCA01 @SIMPL-4015 @SIMPL-4534 @SIMPL-748 @SIMPL-749 @SIMPL-750
  Scenario Outline: Create credentials for <Organisation Name> with type <Participant type>
    Given the dataspace governance authority user navigates to the "Welcome to Dataspace Info" page
    And the user clicks the register for this dataspace button
    And the user is redirected to the Onboarding Request Form page
    And the user verifies the create credentials button is disabled
    And the user enters the email address
    And the user inserts the organisation
    And the user selects a "<Participant type>" participant type
    And the user inserts the surname
    And the user inserts the name
    And the user inserts the username
    And the user inserts the password
    And the user inserts the confirm password
    And the user verifies the create credentials button is enabled
    When the user clicks the create credential button
    Then the successful message is displayed
    And the user can see the submission form button
    And the user can see the access credentials

    Examples:
      | Participant type        |
      | Consumer                |
      | Application Provider    |
      | Data Provider           |
      | Infrastructure Provider |

  @TCA02 @SIMPL-740
  Scenario Outline: PSO | Verify after logging in Public Dataspace Onboarding Site, user uploads a document in .pdf format
    Given the applicant request user already created the request as "<Participant type>"
    And the user navigates to the "Additional Request" page
    And the user is redirected to the "Governance Authority" keycloak
    And the user logs in keycloak with a recently created user
    And the user selects a .pdf file
    Then the user clicks on the Submit Application Request button
    And the user clicks on the Submit button
    And the file is successfully uploaded

    Examples:
      | Participant type        |
      | Consumer                |
      | Application Provider    |
      | Data Provider           |
      | Infrastructure Provider |

#  @TCA03 @SIMPL-751 @SIMPL-1034
#  Scenario: Verify applicant dataspace participant user view in progress onboarding status
#    Given the applicant with pdf submitted is in the onboarding status page
#    When the applicant logs in with valid credentials
#    Then the application status summary page shows the status request is "Request submitted" with a "green" icon
#    And the application status summary page shows the status request is "In Progress" with a "green" icon
#    And the application status summary page shows the status request is "Request accepted" with a "grey" icon
#    And the applicant dataspace participant user can open the request status details dialog pressing See request details button
#
#  @TCA04 @SIMPL-752
#  Scenario: Verify applicant dataspace participant user does not view in progress onboarding status
#    Given the applicant with pdf not submitted is in the onboarding status page
#    When the applicant logs in with valid credentials
#    Then the application status summary page shows the status request is "Request submitted" with a "green" icon
#    And the application status summary page does not show the status request is "In Progress" with a "green" icon
#    And the application status summary page does not show the status request is "Request accepted" with a "grey" icon
#    And the applicant dataspace participant user can open the request status details dialog pressing See request details button

  @TCA05 @SIMPL-746 @SIMPL-1132
  Scenario Outline: Access Denied With Invalid Authentication: <page>
    Given the user navigates to the "<page>" page
    When the user tries to log in with invalid login credentials
    Then the user sees an error message displayed

    Examples:
      | page                    |
      | Participant Management  |
      | Dashboard Requests List |

  @TCA06 @SIMPL-777
  Scenario Outline: Participant Type Selection
    Given the dataspace governance authority user navigates to the "Welcome to Dataspace Info" page
    When the user clicks the register for this dataspace button
    And the user selects a "<participant_type>" participant type
    Then the system displays "<participant_type>" as the selected option
    Examples:
      | participant_type        |
      | Consumer                |
      | Application Provider    |
      | Data Provider           |
      | Infrastructure Provider |

  @TCA07 @SIMPL-778
  Scenario Outline: Special Characters in Organisation Name
    Given the dataspace governance authority user navigates to the "Welcome to Dataspace Info" page
    When the user clicks the register for this dataspace button
    And the user inserts the organisation "<organisation_name>"
    Then the system displays "<organisation_name>" as the input value
    Examples:
      | organisation_name   |
      | Valid Org!@#        |
      | @nother `Org*&^%$<> |

  @TCA08 @SIMPL-3049 @SIMPL-4125 @SIMPL-744
#  NOTE: Once API is done, the test should be changed, creating a new participant via API before checking the data.
  Scenario: Display of Participant Details in the Governance Authority's Data Space
    Given the user navigates to the "Participant Management" page
    And the user is redirected to the "Governance Authority" keycloak
    And the user logs in with a user with "IATTR_M" role
    And the "list of onboarded participants" for the data space is displayed
    When the user filters by "Participant Name" with value "PSO SIMPL-3049"
    And the user clicks on the first result from "Participant List"
    Then the user verifies the participants information on the Participant Details page
    And the following participant details are displayed:
      | Identifier                              | USER_IDENTIFIER                  |
      | Participant Name                        | PSO SIMPL-3049                   |
      | Participant type                        | DATA_PROVIDER                    |
      | Onboarding Date                         | USER_ONBOARDING_DATE             |
      | Participant Credentials Expiration Date | USER_CREDENTIALS_EXPIRATION_DATE |

  @TCA09 @SIMPL-4128 @SIMPL-4129 @SIMPL-4134 @SIMPL-4135
#  NOTE: Once API is done, the test should be changed, creating a new participant via API before checking the data.
  Scenario Outline: Filtering of a Participant's Identity Attributes in the Governance Authority's Data Space: <caseName>
    Given the user navigates to the "Participant Management" page
    And the user is redirected to the "Governance Authority" keycloak
    And the user logs in with a user with "IATTR_M" role
    And the user filters by "Participant Name" with value "PSO SIMPL-3049"
    And the user clicks on the first result from "Participant List"
    And the user verifies the participants information on the Participant Details page
    And the list of Identity Attributes for the participant is displayed
    When the user filters by "<filter>" with value "<value>"
    Then the identity attributes shown have "<filter>" matching "<value>"

    Examples:
      | caseName                                         | filter           | value                              |
      | Filtering by Name                                | Name             | Assignable Test Identity Attribute |
      | Filtering by Code                                | Code             | assignable_test_identity_attribute |
      | Filtering by Date Range for the Last Update Date | Last Update Date | 01/01/2024 - 31/12/2025            |
      | Filtering by Fixed Date for the Last Update Date | Last Update Date | 27/02/2025 - 27/02/2025            |

  @TCA10 @SIMPL-652 @SIMPL-651 @SIMPL-653 @SIMPL-601 @SIMPL-602 @SIMPL-604
  Scenario Outline: Filter onboarding requests: <caseName>
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    And the user sees the filter button in the onboarding requests table list
    When the user filters by "<filter>" with value "<value>"
    Then the onboarding requests list shows "<filter>" matching "<value>"

    Examples:
      | caseName                     | filter | value                                           |
      | Filtering by Status Approved | status | Approved                                        |
      | Filtering by Email           | email  | dataparticipant_request_rejected@automation.com |

  @TCA11 @SIMPL-767
  Scenario Outline: Verify Request Status of Onboarding Requesters: <caseName>
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    And the user sees the filter button in the onboarding requests table list
    When the user filters by "email" with value "<email>"
    Then the onboarding requests list shows "status" matching "<status>"

    Examples:
      | caseName                      | email                                                | status      |
      | Email with In Progress Status | dataparticipant_request_inprogress@psoautomation.com | In Progress |
      | Email with Approved Status    | dataparticipant_request_accepted@automation.com      | Approved    |
      | Email with Rejected Status    | dataparticipant_request_rejected@automation.com      | Rejected    |

#  @TCA12 @SIMPL-1128 @SIMPL-1080
#  # TODO: Check download credentials.
#  Scenario: Download credentials from approved user
#    Given the applicant with pdf submitted is in the onboarding status page
#    When the accepted applicant logs in with valid credentials
#    And the applicant dataspace participant user can open the request status details dialog pressing See request details button
#    Then the applicant dataspace participant clicks on the "Download your Credentials" button
#
#  @TCA13 @SIMPL-1130 @SIMPL-615 @SIMPL-1036
#  Scenario Outline: If incomplete or rejected onboarding request, the user does not see Download Your Credentials button
#    Given the applicant with pdf submitted is in the onboarding status page
#    And the <applicantStatus> applicant logs in with valid credentials
#    When the applicant dataspace participant user can open the request status details dialog pressing See request details button
#    Then the Download Your Credentials button is not visible
#
#    Examples:
#      | applicantStatus |
#      |                 |
#      | rejected        |

  @TCA14 @SIMPL-617 @SIMPL-612 @SIMPL-608 @SIMPL-609 @SIMPL-614 @SIMPL-618 @SIMPL-1081 @SIMPL-619 @bug:SIMPL-9566
  Scenario Outline: Confirm filtering by Status in Progress and <button> the request
    #  NOTE: Once API is done, the following two steps should be changed/deleted, at least while bugs are still affecting the flow.
    Given the applicant request user already created the request as "<Participant type>"
    And the applicant request user already submitted the PDF file
   #  NOTE: End of extra steps.
    And the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    And the user filters by "status" with value "In review"
    And the user filters by email already created user
    And the user sorts by "<sortDirection>" "<columnToSort>"
    And the user clicks on the first result from "Request List"
    When the dataspace governance user clicks on the "<button>" button
    Then the system prompts the user to confirm the "<actionValue>" action

    Examples:
      | caseName                                                          | button  | actionValue      | sortDirection | columnToSort | Participant type        |
      | Consumer user confirms and process Approval action                | Approve | Confirm Approval | descending    | Request Date | Consumer                |
      | Consumer user confirms and process Reject action                  | Reject  | Reject Request   | descending    | Request Date | Consumer                |
      | Application Provider user confirms and process Approval action    | Approve | Confirm Approval | descending    | Request Date | Application Provider    |
      | Application Provider user confirms and process Reject  action     | Reject  | Reject Request   | descending    | Request Date | Application Provider    |
      | Data Provider user confirmsand process Approval action            | Approve | Confirm Approval | descending    | Request Date | Data Provider           |
      | Data Provider user confirms and process Reject  action            | Reject  | Reject Request   | descending    | Request Date | Data Provider           |
      | Infrastructure Provider user confirms and process Approval action | Approve | Confirm Approval | descending    | Request Date | Infrastructure Provider |
      | Infrastructure Provider user confirms  and process Reject  action | Reject  | Reject Request   | descending    | Request Date | Infrastructure Provider |

  @TCA15 @SIMPL-655
  Scenario:  No matching results for filters applied
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    When the user filters by "status" with value "Rejected"
    And the user filters by "email" with value "nonexistentuser@example.com"
    Then the onboarding requests list shows a message "There are currently no onboarding requests."

  @TCA16 @SIMPL-654
  Scenario:  Applying multiple search filters to the Onboarding Requests List: status and requester email
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    When the user filters by "status" with value "Approved"
    And the user filters by "email" with value "dataparticipant_request_accepted@automation.com"
    Then the onboarding requests list shows the request with "status" "Approved" and "email" "dataparticipant_request_accepted@automation.com"

  @TCA17 @SIMPL-2868 @SIMPL-3102 @bug:SIMPL-6626
  Scenario Outline: Verify Navigation to Participant Type Detail Page and Display of Correct Title and Identity Attributes
    Given the user navigates to the "Participant Management" page
    And the user logs in with a user with "IATTR_M" role
    And the "list of onboarded participants" for the data space is displayed
    When the user filters by "Participant Type" with value "<Participant Type>"
    Then the Participant List page is displayed
    And the user clicks on the first result from "Participant List"
    And the list of Identity Attributes for the participant is displayed

    Examples:
      | Participant Type        |
      | Application Provider    |
      | Consumer                |
      | Data Provider           |
      | Infrastructure Provider |

  @TCA18 @SIMPL-3601 @SIMPL-3061 @SIMPL-3055
  Scenario Outline: Successful Creation of an Identity Attribute: <caseName>
    Given the user navigates to the "Identity Attributes" page
    And the user logs in with a user with "IATTR_M" role
    And the user clicks on the Create Identity Attribute button
    And the user is in the Identity Attributes Creation page
    And the user fills the Identity Attributes Creation form with the following data:
      | name                | <name>       |
      | code                | <code>       |
      | assignable to roles | <assignable> |
    When the user submits the form
    Then the Identity Attribute Creation success message is displayed
    And the user is redirected to the Identity Attributes Details page
    And the following identity attribute details are displayed:
      | Code                 | <code>       |
      | Name                 | <name>       |
      | Assignable to Tier 1 | <assignable> |
      | Creation date        | TODAY        |

    Examples:
      | caseName                          | name                                   | code                                   | assignable |
      | Assignable Identity Attribute     | Assignable Test Identity Attribute     | assignable_test_identity_attribute     | true       |
      | Non-Assignable Identity Attribute | Non-Assignable Test Identity Attribute | non_assignable_test_identity_attribute | false      |

  @TCA19 @SIMPL-3466 @SIMPL-3139 @SIMPL-3140 @SIMPL-4127 @SIMPL-3468 @SIMPL-3469 @SIMPL-3464 @SIMPL-3470 @SIMPL-3495
  Scenario: Successful Update of an Identity Attribute
    Given the user navigates to the "Identity Attributes" page
    And the user logs in with a user with "IATTR_M" role
    And the user filters by "Name" with value "PSO SIMPL-3466 IDENTITY ATTRIBUTE"
    And the user clicks on the first result from "Identity Attributes List"
    And the Identity Attributes Details page is displayed
    And the following identity attribute details are displayed:
      | Code                 | PSO_SIMPL-3466_IDENTITY_ATTRIBUTE |
      | Name                 | PSO SIMPL-3466 IDENTITY ATTRIBUTE |
      | Assignable to Tier 1 | true                              |
    When the user clicks on the Edit button
    And the user is redirected to the "Identity Attribute Edition" page
    And the user updates the Identity Attribute with the following data:
      | Name                 | PSO SIMPL-3466 IDENTITY ATTRIBUTE EDITED |
      | Code                 | PSO_SIMPL-3466_IDENTITY_ATTRIBUTE_EDITED |
      | Assignable to Tier 1 | false                                    |
    And the user submits the form
    Then the Identity Attribute Edition success message is displayed
    And the following identity attribute details are displayed:
      | Code                 | PSO_SIMPL-3466_IDENTITY_ATTRIBUTE_EDITED |
      | Name                 | PSO SIMPL-3466 IDENTITY ATTRIBUTE EDITED |
      | Assignable to Tier 1 | false                                    |
      | Last change date     | TODAY                                    |
    And the user reverts the changes done to the Identity Attribute

  @TCA20 @SIMPL-3467
  Scenario: Cancellation of the Identity Attribute Editing Process
    Given the user navigates to the "Identity Attributes" page
    And the user logs in with a user with "IATTR_M" role
    And the user filters by "Name" with value "PSO SIMPL-3467 IDENTITY ATTRIBUTE"
    And the user clicks on the first result from "Identity Attributes List"
    And the Identity Attributes Details page is displayed
    And the following identity attribute details are displayed:
      | Code                 | PSO_SIMPL-3467_IDENTITY_ATTRIBUTE |
      | Name                 | PSO SIMPL-3467 IDENTITY ATTRIBUTE |
      | Assignable to Tier 1 | true                              |
    When the user clicks on the Edit button
    And the user is redirected to the "Identity Attribute Edition" page
    And the user updates the Identity Attribute with the following data:
      | Name                 | PSO SIMPL-3467 IDENTITY ATTRIBUTE EDITED |
      | Code                 | PSO_SIMPL-3467_IDENTITY_ATTRIBUTE_EDITED |
      | Assignable to Tier 1 | false                                    |
    Then the user cancels the form
    And the Identity Attributes Details page is displayed
    And the following identity attribute details are displayed:
      | Code                 | PSO_SIMPL-3467_IDENTITY_ATTRIBUTE |
      | Name                 | PSO SIMPL-3467 IDENTITY ATTRIBUTE |
      | Assignable to Tier 1 | true                              |

  @TCA21 @SIMPL-848 @SIMPL-847 @SIMPL-849 @SIMPL-850 @SIMPL-851 @SIMPL-852 @SIMPL-1077 @SIMPL-1078 @SIMPL-1079
  Scenario Outline: User sorts onboarding requests by <columnToSort> in <sortDirection> order
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    And the onboarding requests list is displayed
    When the user sorts by "<sortDirection>" "<columnToSort>"
    Then the "onboarding request list" is sorted successfully by "<sortDirection>" "<columnToSort>"
    Examples:
      | sortDirection | columnToSort     |
      | ascending     | Last change date |
      | descending    | Last change date |
      | ascending     | Request Date     |
      | descending    | Request Date     |

  @TCA22 @SIMPL-3105
  Scenario: User in the Participant Management page sees the buttons to sort by Participant Type and Onboarding Date
    Given the user navigates to the "Participant Management" page
    And the user is redirected to the "Governance Authority" keycloak
    When the user logs in with a user with "IATTR_M" role
    Then the sorting button "Participant Type" is enabled
    And the sorting button "Onboarding Date" is enabled

  @TCA23 @SIMPL-766 @SIMPL-631 @SIMPL-1116 @SIMPL-3092
  Scenario Outline: Verify the pagination works successfully for Dashboard Requests List : <caseName>
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    And the onboarding requests list is displayed
    When the user selects <numberValueByPage> items per page from the dropdown list
    And the "Dashboard Requests List" is updated with a maximum of <numberValueByPage> results
    Then the user updates the request list by trying to click the "Next page" button
    And the user updates the request list by trying to click the "Previous page" button

    Examples:
      | caseName                              | numberValueByPage |
      | The user selects 5 elements by page   | 5                 |
      | The user selects 10 elements by page  | 10                |
      | The user selects 25 elements by page  | 25                |
      | The user selects 100 elements by page | 100               |

  @TCA24 @SIMPL-3057 @SIMPL-3058 @SIMPL-3059 @bug:SIMPL-951
  # @ToDo: Email user column does not appear at the moment bug reported
  Scenario Outline: Verify Table Headings, Onboarded Participants List, and Pagination Control : <caseName>
    Given the user navigates to the "Participant Management" page
    When the user logs in with a user with "IATTR_M" role
    Then the page displays a table with the following column headings:
      | Participant Name |
      | Participant type |
      | Onboarding Date  |
      #| Applicant user email |
      | Actions          |
    And the "list of onboarded participants" for the data space is displayed
    When the user selects <numberValueByPage> items per page from the dropdown list
    And the "Participant List" is updated with a maximum of <numberValueByPage> results
    Then the user updates the participant list by trying to click the "Next page" button
    And the user updates the participant list by trying to click the "Previous page" button

    Examples:
      | caseName                              | numberValueByPage |
      | The user selects 5 elements by page   | 5                 |
      | The user selects 10 elements by page  | 10                |
      | The user selects 25 elements by page  | 25                |
      | The user selects 100 elements by page | 100               |

#  @TCA25 @SIMPL-755
# # TODO: T2IAA_M role doesn't have access to Dashboard Requests List. Check if there's a change/bug.
#  Scenario: Accessing Admin Dashboard via Keycloak with role T2IAA_M
#    Given the user navigates to the "Dashboard Requests List" page
#    When the user logs in with a user with "T2IAA_M" role
#    Then the onboarding requests list is displayed

  @TCA26 @SIMPL-650 @SIMPL-521
  Scenario Outline: Denied Access To Admin Dashboard of Public Dataspace Onboarding Site : <caseName>
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "<role>" role
    Then the access is denied
    And the access denied message is displayed

    Examples:
      | caseName                                                            | role    |
      | When the user tries to log in with a user that does not have a role | NO ROLE |
      | When the user tries to login with a user that has an incorrect role | IATTR_M |

  @TCA27 @SIMPL-4018
  Scenario Outline: Create credentials for a <Participant type> and with an existing email
    Given the dataspace governance authority user navigates to the "Welcome to Dataspace Info" page
    And the user clicks the register for this dataspace button
    And the user is redirected to the Onboarding Request Form page
    And the user verifies the create credentials button is disabled
    And the user selects an "email_already_created@automation.com" address
    And the user inserts the organisation
    And the user selects a "<Participant type>" participant type
    And the user inserts the surname
    And the user inserts the name
    And the user inserts the username
    And the user inserts the password
    And the user inserts the confirm password
    And the user verifies the create credentials button is enabled
    When the user clicks the create credential button
    Then the error message is displayed

    Examples:
      | Participant type        |
      | Consumer                |
      | Application Provider    |
      | Data Provider           |
      | Infrastructure Provider |

  @TCA28 @SIMPL-3146 @SIMPL-3148 @SIMPL-3450
  Scenario Outline: Displaying the correct values in the Identity Attribute table for <columnName> column
    Given the user navigates to the "Identity Attributes" page
    And the user logs in with a user with "IATTR_M" role
    Then the list displays "<valueDisplayed>" for each Identity Attribute in the "<columnName>" column

    Examples:
      | columnName         | valueDisplayed  |
      | Assignable to Role | a boolean value |
      | In use             | a boolean value |
      | Action             | icon delete     |

  @TCA29 @SIMPL-3106 @SIMPL-3107 @SIMPL-3108 @SIMPL-3109
  Scenario Outline: User sorts Participant List requests by <columnToSort> in <sortDirection> order
    Given the user navigates to the "Participant Management" page
    And the user logs in with a user with "IATTR_M" role
    And the "list of onboarded participants" for the data space is displayed
    When the user sorts by "<sortDirection>" "<columnToSort>"
    Then the "participant list requests" is sorted successfully by "<sortDirection>" "<columnToSort>"

    Examples:
      | sortDirection | columnToSort     |
      | ascending     | Participant Type |
      | descending    | Participant Type |
      | ascending     | Onboarding Date  |
      | descending    | Onboarding Date  |

  @TCA30 @SIMPL-4291
  Scenario Outline: Display Paginated List of Identity Attributes and with Correct Column Headings : <caseName>
    Given the user navigates to the "Identity Attributes" page
    When the user logs in with a user with "IATTR_M" role
    Then the page displays a table with the following column headings:
      | Identifier         |
      | Code               |
      | Assignable to Role |
      | Enabled            |
      | In use             |
      | Creation date      |
      | Last change date   |
      | Actions            |
    And the list of Identity Attributes is displayed
    When the user selects <numberValueByPage> items per page from the dropdown list
    And the "Identity Attributes List" is updated with a maximum of <numberValueByPage> results
    Then the user updates the identity attributes list by trying to click the "Next page" button
    And the user updates the identity attributes list by trying to click the "Previous page" button

    Examples:
      | caseName                              | numberValueByPage |
      | The user selects 5 elements by page   | 5                 |
      | The user selects 10 elements by page  | 10                |
      | The user selects 25 elements by page  | 25                |
      | The user selects 100 elements by page | 100               |

  @TCA31 @SIMPL-3451
  Scenario: Confirm Deletion dialog box displays successfully.
    Given the user navigates to the "Identity Attributes" page
    And the user logs in with a user with "IATTR_M" role
    When the user presses on the delete button
    Then the dialog for the delete action is displayed successfully

  @TCA32 @SIMPL-3309 @SIMPL-3310 @SIMPL-3311 @SIMPL-3312 @SIMPL-3313 @SIMPL-3314 @bug:SIMPL-10276
     # Filter by email scenario has a bug associated.
  Scenario Outline: Participants Management page filtering and resetting the filters: <caseName>
    Given the user navigates to the "Participant Management" page
    And the user logs in with a user with "IATTR_M" role
    When the user filters by "<filter>" with value "<filterValue>"
    Then the "Participants list" for the data space is displayed
    And  the user resets the "<filter>" with the value "<filterValue>" applied successfully
    And  the "Participants list" for the data space is displayed

    Examples:
      | caseName                                  | filter           | filterValue |
      | the user is filtering by Participant type | Participant Type | Consumer    |
      | the user is filtering by Participant Name | Participant Name | PSO         |
      | the user is filtering by Onboarding date  | Onboarding Date  | TODAY       |
      #| the user is filtering by Email            | Email            | complete    |

  @TCA33 @SIMPL-3453
  Scenario: Cancellation of the Identity Attribute Deletion Process
    Given the user navigates to the "Identity Attributes" page
    And the user logs in with a user with "IATTR_M" role
    And the user filters by "Name" with value "PSO SIMPL-3453 IDENTITY ATTRIBUTE"
    And the users clicks on the remove button under the Action column
    And the system prompts the user to confirm the removal action
    Then the user cancels the removal action
    And the page displays a table with the following column headings:
      | Identifier         |
      | Code               |
      | Assignable to Role |
      | Enabled            |
      | In use             |
      | Creation date      |
      | Last change date   |
      | Actions            |
    And the list of Identity Attributes is displayed

  @TCA34 @SIMPL-4917
  Scenario: Confirm Participant Detail Information Page displays correct details.
    Given the user navigates to the "Participant Management" page
    And the user logs in with a user with "IATTR_M" role
    And the "list of onboarded participants" for the data space is displayed
    When the user reads the Participant List data from the first row
    And the user clicks on the first result from "Participant List"
    Then the user verifies the participants information on the Participant Details page
    And the user verifies the following participant details match with the values obtained from the row
      | Identifier                              | IDENTIFIER_OBTAINED_DYNAMICALLY_FROM_URL                                      |
      | Participant Name                        | PARTICIPANT_NAME_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE            |
      | Participant type                        | PARTICIPANT_TYPE_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE            |
      | Onboarding Date                         | ONBOARDING_DATE_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE             |
      | Participant Credentials Expiration Date | CREDENTIALS_EXPIRATION_DATE_OBTAINED_DYNAMICALLY_FROM_PARTICIPANT_DETAIL_PAGE |

  @TCA35 @SIMPL-656
  Scenario Outline: The Dashboard Requests List filtering and resetting multiple filters applied : <caseName>
    Given the user navigates to the "Dashboard Requests List" page
    And the user logs in with a user with "NOTARY" role
    When the user filters by "<filter1>" with value "<filterValue1>"
    And the user filters by "<filter2>" with value "<filterValue2>"
    Then the onboarding requests list is displayed
    And the user verifies the "<filter1>" with value "<filterValue1>" and "<filter2>" with value "<filterValue2>" are applied
    And the user resets the multiples filters applied successfully
    And the onboarding requests list is displayed

    Examples:
      | caseName                                                         | filter1 | filterValue1 | filter2 | filterValue2                                    |
      | the user filtering by status and by email, then reset the filter | status  | Approved     | email   | dataparticipant_request_accepted@automation.com |