@createDataSpace @regression @ui
Feature: Data Spaces Creation

  @SIMPL-3667 @smoke
  Scenario: Filter Data Spaces by Status
    Given the user is on the 'Data spaces' page
    When the user clicks on 'Create Data Space' button
    Then the page 'Select a Template' should be opened

  @SIMPL-3793 @smoke
  Scenario: Select Data Space Template
    Given the user is on the 'Data spaces' page
    When the user clicks on 'Create Data Space' button
    And the user selects a template
    Then the system should open the page 'Set up Data Space'

  @SIMPL-3958
  Scenario: Navigate Back to Previous Step
    Given the user is on the 'Set up Data Space' page
    When the user clicks the 'Back' button
    Then the page 'Select a Template' should be opened

  @SIMPL-3943
  Scenario: Add Data Space Conditions
    Given the user is on the 'Data Space details' page
    When the user clicks the button 'Next'
    Then the page 'Conditions' should be opened
    And the user is allowed to accept all conditions to the data space
    And the button Next should be enabled

  @SIMPL-3694 @smoke
  Scenario: Mandatory conditions of components for the Data Space Creation
    Given the user is on the 'Conditions' page
    When the user accepts not all conditions for selected components
    Then the user should not be able to click the 'Create data space' button

  @SIMPL-3641 @smoke
  Scenario: Add Nodes to Data Space
    Given the user is on the 'Set up Data Space' page
    When the user clicks the 'Add Nodes' button
    And the user selects the following nodes from the available list:
      | APPLICATION_PROVIDER   |
      | INFRASTRUCTURE_PROVIDER|
    Then the following nodes should be displayed in the Selected nodes list:
      | APPLICATION_PROVIDER   |
      | INFRASTRUCTURE_PROVIDER|

  @SIMPL-3641 @smoke
  Scenario: Add Nodes to Data Space
    Given the user is on the 'Set up Data Space' page
    And the user selected "APPLICATION_PROVIDER" and "INFRASTRUCTURE_PROVIDER" in 'Add nodes' modal
    When the user clicks the 'Add Nodes' button in 'Add nodes' modal
    Then the selected nodes should appear in the list of nodes
      | Application Provider   |
      | Infrastructure Provider|

  @SIMPL-3693 @smoke
  Scenario: Data Space creation status
    Given the user is on the 'Conditions' page
    When the user accepts all conditions for selected components
    And the user clicks the 'Create data space' button
    Then the system should display a confirmation page
    When the user clicks 'Go back' button
    Then the system should display the newly created data space with the status 'Requested'
