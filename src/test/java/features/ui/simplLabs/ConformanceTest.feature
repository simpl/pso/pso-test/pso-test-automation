@conformanceTest @regression @ui
Feature: Conformance test

  @SIMPL-5471 @smoke
  Scenario: Start conformance test
    Given the user is on the Components page
    When the user opens a custom component
    And the user clicks on a checkbox for a test case
    And the user clicks on the button 'Start conformance test'
    Then the system should open the page 'Conformance test'
