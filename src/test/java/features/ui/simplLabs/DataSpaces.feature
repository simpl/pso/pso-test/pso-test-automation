@dataspaces @regression @ui
Feature: Data Spaces Filters

  @SIMPL-3666 @smoke
  Scenario Outline: Filter Data Spaces by Status
    Given the user is on the 'Data spaces' page
    When the user selects the status filter "<status>"
    Then the system should display data spaces with the status "<status>"
    Examples:
      | status  |
      | RUNNING |

  @SIMPL-3883 @smoke
  Scenario: Interact with a Data Space
    Given the user is on the 'Data spaces' page
    When the user clicks on 'Interact' for a data space with status RUNNING
    Then the system should open the page 'Data Space architecture'
