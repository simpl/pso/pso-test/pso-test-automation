@logout @regression @ui
Feature: Logout

  @SIMPL-3603 @smoke
  Scenario: User logs out successfully
    Given the user is on the 'Data spaces' page
    When the user clicks on the 'Log out' button next to the users email
    Then the login page is displayed
