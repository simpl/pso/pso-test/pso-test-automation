@AuthorityAPI @regression @api
Feature: Authority API scenarios

  @TCA01_API @SIMPL-3336 @SIMPL-3337 @SIMPL-3338 @SIMPL-3344
  Scenario: User with IATTR_M Role Creates a New Identity Attribute
    Given a user with role "IATTR_M" is logged in to governance authority
    When the user creates and assigns Identity Attribute with the following data:
      | Code                | RANDOM_CODE             |
      | Enabled             | true                    |
      | Assignable to roles | true                    |
      | Name                | RANDOM_NAME             |
      | Description         | RANDOM_DESCRIPTION      |
      | Participant Types   | DATA_PROVIDER; CONSUMER |
    Then the system indicates successful creation
    And the response body contains the expected Identity Attribute's details
    And the response body contains assigned Participant Types
      | DATA_PROVIDER |
      | CONSUMER      |

  @TCA02_API @SIMPL-3334
  Scenario: Successful retrieval of an Identity Attribute by ID
    Given a user with role "IATTR_M" is logged in to governance authority
    And the user creates and assigns Identity Attribute with the following data:
      | Code                | RANDOM_CODE        |
      | Enabled             | true               |
      | Assignable to roles | true               |
      | Name                | RANDOM_NAME        |
      | Description         | RANDOM_DESCRIPTION |
      | Participant Types   | CONSUMER           |
    Then the system indicates successful creation
    When the user searches for the identity attribute by ID
    Then the response body contains the expected Identity Attribute's details

  @TCA03_API @SIMPL-3335 @SIMPL-3343
  Scenario: User with IATTR_M role updates an Identity Attribute
    Given a user with role "IATTR_M" is logged in to governance authority
    And an Identity Attribute is already created and assigned
    When the user updates the Identity Attribute with the new data:
      | Code                | UPDATED_CODE                                  |
      | Enabled             | false                                         |
      | Assignable to roles | false                                         |
      | Name                | UPDATED_NAME                                  |
      | Description         | UPDATED_DESCRIPTION                           |
      | Participant Types   | APPLICATION_PROVIDER; INFRASTRUCTURE_PROVIDER |
    And the update is performed successfully
    And the user searches for the identity attribute by ID
    Then the response body contains the expected Identity Attribute's details

  @TCA04_API @SIMPL-4072 @skip
  Scenario: Attempt to Delete Assigned Identity Attribute via API - Deletion forbidden
    Given a user with role "IATTR_M" is logged in to governance authority
    When the user searches for the identity attribute
    And the user saves an ID of the first found identity attribute
    And the user deletes "the found" Identity Attribute
    Then the system doesn't allow to do that
    And the response body contains appropriate response message:
      | error | The deletion of an assigned identity attribute is not allowed. |

  @TCA05_API @SIMPL-4096
  Scenario: Attempt to Delete random Identity Attribute
    Given a user with role "IATTR_M" is logged in to governance authority
    When the user deletes "a random" Identity Attribute
    Then the system indicates absence of such resource
    And the response body contains appropriate response message:
      | error | Identity attribute with id [ random_id ] not found |

  @TCA06_API @SIMPL-4532
  Scenario: Disable an Identity Attribute
    Given a user with role "IATTR_M" is logged in to governance authority
    And an Identity Attribute is already created and assigned
    When the user sets the Identity Attribute as assignable to "false"
    And the operation is completed successfully
    And the user searches for the identity attribute by ID
    Then the response body contains updated Identity Attribute data:
      | assignable to roles | false     |

  @TCA07_API @SIMPL-4502 @SIMPL-4533 @bug:SIMPL-10207 @bug:SIMPL-10297
  Scenario: Unsuccessful Identity Attribute enablement and disablement
    Given a user with role "IATTR_M" is logged in to governance authority
    And an Identity Attribute is already created and assigned
    When the user attempts to set the Identity Attribute as assignable to "true" with a modified ID
    And the system indicates bad request
    And the user attempts to set the Identity Attribute as assignable to "false" with a modified ID
    Then the system indicates bad request


  @TCA08_API @SIMPL-5233
  Scenario: Creation of New Tier1 Role with T1UAR_M Role - Role Successfully Created
    Given a user with role "AUTHORITY_T1UAR_M" is logged in to governance authority
    When the user creates User Role with the following data:
      | name                | RANDOM_NAME                   |
      | description         | RANDOM_DESCRIPTION            |
    Then the system indicates successful creation
    And the response body contains the expected User Role's details

  @TCA09_API @SIMPL-5265 @SIMPL-5235
  Scenario: Unauthorized Access Attempt for CRUD Operations
    When the user tries to perform any CRUD operation
    Then the system declines all unauthorized requests
