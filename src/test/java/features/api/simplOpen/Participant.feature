@ParticipantAPI @regression @api
Feature: Participant API scenarios

  @TCPP01_API @SIMPL-769 @SIMPL-768 @SIMPL-2587
  Scenario Outline: Successful retrieval of the identity attributes stored in a Participant's Agent: <caseName>
    When a user with "<role>" role searches for identity attributes stored locally in the agent
    Then the search of identity attributes is successful

    Examples:
      | caseName                        | role                  |
      | Participant Type: Data Provider | DATA_PROVIDER_T1UAR_M |
      | Participant Type: Consumer      | CONSUMER_T1UAR_M      |
