package stepDefinitions.api.simplOpen;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import framework.api.enums.*;
import framework.api.helpers.ApiSetup;
import framework.api.helpers.RequestHandler;
import framework.ui.helpers.Utils;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParticipantSteps {

    private static ApiSetup setup;
    private RequestHandler requestHandler;
    private Scenario scenario;

    @Before("@ParticipantAPI")
    public void setUp(Scenario scenario) {
        this.scenario = scenario;
        setup = new ApiSetup();
    }

    @When("a user with {string} role searches for identity attributes stored locally in the agent")
    public void aUserWithRoleSearchesForIdentityAttributesStoredLocallyInTheAgent(String role) {
        String baseURL;
        if (role.contains("DATA_PROVIDER")) {
            baseURL = BaseUrl.DATA_PROVIDER;
            setup.setUp(baseURL, scenario);
            requestHandler = new RequestHandler(baseURL, setup.getApiContext(), setup.getScenario());
        } else if (role.contains("CONSUMER")) {
            baseURL = BaseUrl.CONSUMER;
            setup.setUp(baseURL, scenario);
            requestHandler = new RequestHandler(baseURL, setup.getApiContext(), setup.getScenario());
        } else {
            throw new IllegalArgumentException("Role " + role + " did not specify the type of participant.");
        }

        Utils.LogInDetails userAndPassword = Utils.getUserAndPasswordByRole(role);
        String token = requestHandler.getAuthorizationToken(Realm.PARTICIPANT, userAndPassword.getUsername(),
                userAndPassword.getPassword());
        requestHandler.addHeader("Authorization", "Bearer " + token);
        requestHandler.sendRequest(HttpMethod.GET, ApiEndpoint.IDENTITY_ATTRIBUTE_LOCAL_AGENT_SEARCH.getPath());
    }

    @Then("the search of identity attributes is successful")
    public void theSearchOfIdentityAttributesIsSuccessful() {
        JsonObject lastResponse = requestHandler.getLastResponseBody();
        int lastStatusCode = requestHandler.getLastStatusCode();

        assertEquals(lastStatusCode, HttpStatus.OK.getCode());
        JsonObject responsePage = lastResponse.getAsJsonObject("page");
        int totalElementsInResponse = responsePage.get("totalElements").getAsInt();
        if (totalElementsInResponse != 0) {
            JsonArray content = lastResponse.get("content").getAsJsonArray();
            JsonObject firstIdentityAttribute = content.get(0).getAsJsonObject();
            assertTrue(firstIdentityAttribute.has("id"));
            assertTrue(firstIdentityAttribute.has("code"));
            assertTrue(firstIdentityAttribute.has("name"));
            assertTrue(firstIdentityAttribute.has("description"));
            assertTrue(firstIdentityAttribute.has("assignableToRoles"));
            assertTrue(firstIdentityAttribute.has("enabled"));
            assertTrue(firstIdentityAttribute.has("assignedToParticipant"));
        }
    }

}
