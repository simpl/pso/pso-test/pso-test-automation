package stepDefinitions.api.simplOpen;

import com.google.gson.*;
import framework.api.services.usersandroles.UserRolesRequestBuilder;
import framework.api.services.securityattributesprovider.objects.IdentityAttribute;
import framework.api.services.securityattributesprovider.IdentityAttributeRequestBuilder;

import framework.api.helpers.RequestHandler;
import framework.api.enums.*;
import framework.api.helpers.ApiSetup;
import framework.api.services.usersandroles.objects.UserRole;
import framework.ui.helpers.Utils;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assume;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static framework.common.Utils.modifyId;
import static org.junit.Assert.*;

public class AuthoritySteps {

    private RequestHandler requestHandler;
    private IdentityAttribute identityAttribute = new IdentityAttribute();
    private UserRole userRole = new UserRole();
    private String savedIdentityAttributeId;
    private final List<String> createdIdentityAttributesIDs = new ArrayList<>();
    private final List<String> createdUserRoleIDs = new ArrayList<>();
    private final List<Integer> unauthorizedStatusCodes = new ArrayList<>();
    private String randomIdentityAttributeId;

    @Before("@AuthorityAPI")
    public void setUp(Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@skip")) {
            System.out.println("Skipping scenario: " + scenario.getName());
            Assume.assumeTrue(false);
        }
        ApiSetup setup = new ApiSetup();
        setup.setUp(BaseUrl.AUTHORITY, scenario);
        requestHandler = new RequestHandler(BaseUrl.AUTHORITY, setup.getApiContext(), scenario);
    }

    @Given("a user with role {string} is logged in to governance authority")
    public void aUserWithRoleIsLoggedInToGovernanceAuthority(String role) {
        Utils.LogInDetails userAndPassword = Utils.getUserAndPasswordByRole(role);
        String token = requestHandler.getAuthorizationToken(Realm.AUTHORITY, userAndPassword.getUsername(),
                userAndPassword.getPassword());
        requestHandler.addHeader("Authorization", "Bearer " + token);
    }

    @Given("an Identity Attribute is already created and assigned")
    public void anIdentityAttributeIsAlreadyCreated() {
        JsonObject requestBody = new IdentityAttributeRequestBuilder()
                .withRandomData()
                .build();

        requestHandler.sendRequest(HttpMethod.POST, ApiEndpoint.IDENTITY_ATTRIBUTE.getPath(), requestBody);
        JsonObject createdIdentityAttribute = requestHandler.getLastResponseBody();
        Gson gson = new Gson();
        identityAttribute = gson.fromJson(createdIdentityAttribute, IdentityAttribute.class);
        createdIdentityAttributesIDs.add(requestHandler.getLastResponseBody().get("id").getAsString());
    }

    @When("the user creates and assigns Identity Attribute with the following data:")
    public void theUserSendsAPostRequestWithAListOfParticipantTypes(DataTable dataTable) {
        Gson gson = new Gson();
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        JsonObject requestBody = new IdentityAttributeRequestBuilder()
                .fromMap(data)
                .build();

        requestHandler.sendRequest(HttpMethod.POST, ApiEndpoint.IDENTITY_ATTRIBUTE.getPath(), requestBody);

        JsonObject createdIdentityAttribute = requestHandler.getLastResponseBody();

        identityAttribute = gson.fromJson(requestBody, IdentityAttribute.class);
        identityAttribute.setId(createdIdentityAttribute.get("id").getAsString());

        createdIdentityAttributesIDs.add(identityAttribute.getId());
    }

    @When("the user creates User Role with the following data:")
    public void theUserCreatesRoleWithTheFollowingData(DataTable dataTable) {
        Gson gson = new Gson();
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        JsonObject requestBody = new UserRolesRequestBuilder()
                .fromMap(data)
                .build();

        requestHandler.sendRequest(HttpMethod.POST, ApiEndpoint.USER_ROLE.getPath(), requestBody);

        JsonObject createdUserRole = requestHandler.getLastResponseBody();

        userRole = gson.fromJson(requestBody, UserRole.class);
        userRole.setId(createdUserRole.get("id").getAsString());

        createdUserRoleIDs.add(userRole.getId());
    }

    @When("the user updates the Identity Attribute with the new data:")
    public void theUserSendsAnUpdateRequestTheNewData(DataTable dataTable) {
        Gson gson = new Gson();
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        String identityAttributeId = identityAttribute.getId();

        JsonObject requestBody = new IdentityAttributeRequestBuilder()
                .fromMap(data)
                .build();

        requestHandler.sendRequest(HttpMethod.PUT, ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + "/" + identityAttributeId, requestBody);
        JsonObject updatedIdentityAttribute = requestHandler.getLastRequestBody();
        identityAttribute = gson.fromJson(updatedIdentityAttribute, IdentityAttribute.class);
    }

    @When("the user searches for the identity attribute by ID")
    public void theUserSearchesForTheIdentityAttributeByID() {
        requestHandler.sendRequest(HttpMethod.GET,
                ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + "/" + createdIdentityAttributesIDs.get(0));
    }

    @When("the user searches for the identity attribute")
    public void theUserSearchesForTheIdentityAttribute() {
        requestHandler.sendRequest(HttpMethod.GET, ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + "/search");
    }

    @When("the user saves an ID of the first found identity attribute")
    public void theUserSavesIDOfTheIdentityAttribute() {
        Gson gson = new Gson();

        JsonObject responseBody = requestHandler.getLastResponseBody();
        assertTrue("Response does not contain 'Identity Attributes' list", responseBody.has("content"));

        JsonArray participantsArray = responseBody.getAsJsonArray("content");
        assertNotEquals("Identity Attributes list is empty", 0, participantsArray.size());

        List<IdentityAttribute> identityAttributes = new ArrayList<>();
        for (JsonElement element : participantsArray) {
            identityAttributes.add(gson.fromJson(element, IdentityAttribute.class));
        }

        IdentityAttribute firstIdentityAttribute = identityAttributes.get(0);
        savedIdentityAttributeId = firstIdentityAttribute.getId();
    }

    @When("the user deletes {string} Identity Attribute")
    public void theUserDeletesIdentityAttribute(String attributeType) {
        String identityAttributeId;

        if (attributeType.equalsIgnoreCase("the found")) {
            identityAttributeId = savedIdentityAttributeId;
        } else if (attributeType.equalsIgnoreCase("a random")) {
            randomIdentityAttributeId = UUID.randomUUID().toString();
            identityAttributeId = randomIdentityAttributeId;
        } else {
            throw new IllegalArgumentException("Unknown attribute type: " + attributeType);
        }

        requestHandler.sendRequest(HttpMethod.DELETE, ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + "/" + identityAttributeId);
    }

    @When("the user sets the Identity Attribute as assignable to {string}")
    public void theUserChangesTheIdentityAttributeAssignableToFalse(String isAssignable) {
        if (!isAssignable.equalsIgnoreCase("true") && !isAssignable.equalsIgnoreCase("false")) {
            throw new IllegalArgumentException("Invalid value for assignable: " + isAssignable + ". Expected 'true' or 'false'.");
        }
        JsonArray idArray = new JsonArray();
        idArray.add(identityAttribute.getId());

        String endpoint = ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + "/assignable/" + isAssignable.toLowerCase();

        requestHandler.sendRequest(HttpMethod.PUT, endpoint, idArray.toString());
    }

    @When("the user attempts to set the Identity Attribute as assignable to {string} with a modified ID")
    public void theUserAttemptsToSetIdentityAttributeWithModifiedId(String isAssignable) {
        if (!isAssignable.equalsIgnoreCase("true") && !isAssignable.equalsIgnoreCase("false")) {
            throw new IllegalArgumentException("Invalid value for assignable: " + isAssignable + ". Expected 'true' or 'false'.");
        }

        String modifiedId = modifyId(identityAttribute.getId());

        JsonArray idArray = new JsonArray();
        idArray.add(modifiedId);

        String endpoint = ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + "/assignable/" + isAssignable.toLowerCase();
        requestHandler.sendRequest(HttpMethod.PUT, endpoint, idArray.toString());
    }

    @When("the operation is completed successfully")
    public void theOperationIsCompletedSuccessfully() {
        int actualStatusCode = requestHandler.getLastStatusCode();
        int expectedStatusCode = HttpStatus.OK.getCode();

        assertEquals("Mismatch in status code", expectedStatusCode, actualStatusCode);
    }

    @When("the user tries to perform any CRUD operation")
    public void theUserTriesToPerformAnyCRUDOperation() {
        List<ApiEndpoint> endpoints = ApiEndpoint.getCrudTestableEndpoints();
        List<HttpMethod> methods = List.of(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE);

        for (ApiEndpoint endpoint : endpoints) {
            for (HttpMethod method : methods) {
                requestHandler.sendRequest(method, endpoint.getPath());
                unauthorizedStatusCodes.add(requestHandler.getStatusCode());
            }
        }
    }

    @Then("the system indicates absence of such resource")
    public void theSystemIndicatesAbsenceOfSuchResource() {
        int actualStatusCode = requestHandler.getLastStatusCode();
        int expectedStatusCode = HttpStatus.NOT_FOUND.getCode();

        assertEquals("Mismatch in status code", expectedStatusCode, actualStatusCode);
    }

    @Then("the system indicates bad request")
    public void theSystemIndicatesBadRequest() {
        int actualStatusCode = requestHandler.getLastStatusCode();
        int expectedStatusCode = HttpStatus.BAD_REQUEST.getCode();

        assertEquals("Mismatch in status code", expectedStatusCode, actualStatusCode);
    }

    @Then("the system declines all unauthorized requests")
    public void theSystemDeclinesUnauthorizedRequest() {
        int expectedStatusCode = HttpStatus.UNAUTHORIZED.getCode();

        assertFalse("No unauthorized requests were made", unauthorizedStatusCodes.isEmpty());
        for (int statusCode : unauthorizedStatusCodes) {
            assertEquals("Mismatch in status code", expectedStatusCode, statusCode);
        }
    }

    @Then("the response body contains updated Identity Attribute data:")
    public void theResponseBodyContainsUpdatedIdentityAttributeData(DataTable dataTable) {
        Gson gson = new Gson();
        IdentityAttribute identityAttributeRetrieved = gson.fromJson(requestHandler.getLastResponseBody(), IdentityAttribute.class);

        assertNotNull("Retrieved identity attribute is null", identityAttributeRetrieved);

        Map<String, String> expectedValues = dataTable.asMap(String.class, String.class);

        for (Map.Entry<String, String> entry : expectedValues.entrySet()) {
            String fieldName = entry.getKey();
            String expectedValue = entry.getValue();

            switch (fieldName.toLowerCase()) {
                case "assignable to roles":
                    boolean expectedAssignableToRoles = Boolean.parseBoolean(expectedValue);
                    assertEquals("Mismatch in 'assignableToRoles'", expectedAssignableToRoles, identityAttributeRetrieved.isAssignableToRoles());
                    break;

                case "enabled":
                    boolean expectedEnabled = Boolean.parseBoolean(expectedValue);
                    assertEquals("Mismatch in 'enabled'", expectedEnabled, identityAttributeRetrieved.isEnabled());
                    break;

                default:
                    throw new IllegalArgumentException("Unknown field in DataTable: " + fieldName);
            }
        }
    }

    @Then("the update is performed successfully")
    public void theUpdateIsPerformedSuccessfully() {
        int actualStatusCode = requestHandler.getLastStatusCode();
        int expectedStatusCode = HttpStatus.UPDATED_NO_CONTENT.getCode();

        assertEquals("Mismatch in status code", expectedStatusCode, actualStatusCode);
    }

    @Then("the response body contains the expected Identity Attribute's details")
    public void theResponseBodyContainsTheUpdatedIdentityAttributeSDetails() {
        Gson gson = new Gson();
        IdentityAttribute identityAttributeRetrieved = gson.fromJson(requestHandler.getLastResponseBody(), IdentityAttribute.class);

        assertNotNull("Retrieved identity attribute is null", identityAttributeRetrieved);
        assertEquals("Mismatch in 'code'", identityAttribute.getCode(), identityAttributeRetrieved.getCode());
        assertEquals("Mismatch in 'name'", identityAttribute.getName(), identityAttributeRetrieved.getName());
        assertEquals("Mismatch in 'description'", identityAttribute.getDescription(), identityAttributeRetrieved.getDescription());
        assertEquals("Mismatch in 'assignableToRoles'", identityAttribute.isAssignableToRoles(), identityAttributeRetrieved.isAssignableToRoles());
        assertEquals("Mismatch in 'enabled'", identityAttribute.isEnabled(), identityAttributeRetrieved.isEnabled());
        assertEquals("Mismatch in 'used'", identityAttribute.isUsed(), identityAttributeRetrieved.isUsed());

        assertNotNull("participantTypes should not be null", identityAttributeRetrieved.getParticipantTypes());
        assertEquals("Mismatch in participantTypes count", identityAttribute.getParticipantTypes().size(), identityAttributeRetrieved.getParticipantTypes().size());

        for (String expectedParticipantType : identityAttribute.getParticipantTypes()) {
            assertTrue("Missing participant type: " + expectedParticipantType,
                    identityAttributeRetrieved.getParticipantTypes().contains(expectedParticipantType));
        }
    }

    @Then("the response body contains the expected User Role's details")
    public void theResponseBodyContainsTheExpectedUserRolesDetails() {
        Gson gson = new Gson();
        UserRole userRoleRetrieved = gson.fromJson(requestHandler.getLastResponseBody(), UserRole.class);

        assertNotNull("Retrieved identity attribute is null", userRoleRetrieved);
        assertEquals("Mismatch in 'name'", userRole.getName(), userRoleRetrieved.getName());
        assertEquals("Mismatch in 'description'", userRole.getDescription(), userRoleRetrieved.getDescription());
    }

    @Then("the response body contains assigned Participant Types")
    public void theResponseBodyContainsAssignedParticipantTypes(DataTable dataTable) {
        List<String> expectedParticipantTypes = dataTable.asList(String.class);
        JsonObject responseBody = requestHandler.getLastResponseBody();

        JsonArray actualParticipantTypes = responseBody.getAsJsonArray("participantTypes");

        for (String expectedType : expectedParticipantTypes) {
            assertTrue("Participant type not found in response: " + expectedType,
                    actualParticipantTypes.toString().contains(expectedType));
        }
    }

    @Then("the system indicates successful creation")
    public void validateSuccessfulCreation() {
        int actualStatusCode = requestHandler.getLastStatusCode();
        int expectedStatusCode = HttpStatus.CREATED.getCode();

        assertEquals("Mismatch in status code", expectedStatusCode, actualStatusCode);
    }

    @Then("the response status code is {int}")
    public void validateResponseStatusCode(int expectedStatusCode) {
        int actualStatusCode = requestHandler.getStatusCode();
        assertEquals(expectedStatusCode, actualStatusCode);
    }

    @Then("the identity attribute is correctly retrieved")
    public void theIdentityAttributeIsCorrectlyRetrieved() {
        Gson gson = new Gson();
        IdentityAttribute identityAttributeRetrieved = gson.fromJson(requestHandler.getLastResponseBody(),
                IdentityAttribute.class);
        assertEquals(identityAttribute, identityAttributeRetrieved);
    }

    @Then("the system doesn't allow to do that")
    public void theSystemDoesntAllowToDoThat() {
        int actualStatusCode = requestHandler.getLastStatusCode();
        int expectedStatusCode = HttpStatus.FORBIDDEN.getCode();

        assertEquals("Mismatch in status code", expectedStatusCode, actualStatusCode);
    }

    @Then("the response body contains appropriate response message:")
    public void theResponseBodyContainsAppropriateResponseMessage(DataTable expectedData) {
        String responseBody = requestHandler.getLastResponseBody().toString();
        JsonObject jsonResponse = JsonParser.parseString(responseBody).getAsJsonObject();

        Map<String, String> expectedValues = expectedData.asMap(String.class, String.class);

        for (Map.Entry<String, String> entry : expectedValues.entrySet()) {
            String expectedKey = entry.getKey();
            String expectedValue = entry.getValue();

            if (expectedValue.contains("random_id")) {
                expectedValue = expectedValue.replace("random_id", randomIdentityAttributeId);
            }

            assertTrue("Response does not contain key: " + expectedKey, jsonResponse.has(expectedKey));
            assertEquals("Mismatch in response value for key: " + expectedKey,
                    expectedValue,
                    jsonResponse.get(expectedKey).getAsString());
        }
    }

    @After(value = "@AuthorityAPI", order = 2)
    public void deleteIdentityAttribute() {
        for (String id : createdIdentityAttributesIDs) {
            String deleteEndpoint = ApiEndpoint.IDENTITY_ATTRIBUTE.getPath() + '/' + id;
            requestHandler.sendRequest(HttpMethod.DELETE, deleteEndpoint);
            int actualStatusCode = requestHandler.getStatusCode();
            int expectedStatusCode = HttpStatus.DELETED.getCode();
            assertEquals(expectedStatusCode, actualStatusCode);
        }
    }
}
