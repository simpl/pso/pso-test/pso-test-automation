package stepDefinitions.ui.simplOpen;

import com.google.gson.JsonObject;
import com.microsoft.playwright.Download;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import framework.common.Config;
import framework.ui.helpers.DocumentValidator;
import framework.ui.helpers.UiSetup;
import framework.ui.helpers.Utils;
import framework.ui.pages.simplOpen.WizardPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static configuration.ui.data.simplOpen.Wizard.*;
import static framework.common.Assertions.assertElementIsVisibleByLocator;
import static framework.ui.locators.simplOpen.Wizard.*;

public class WizardSteps {

    private Page page = UiSetup.getPage();
    private final WizardPage wizardPage = new WizardPage(page);

    @Given("the user logs in to the SD Wizard")
    public void theUserLogsInToTheSDWizard() {
        page = UiSetup.httpLogIn(Config.get("USER_WIZARD"), Config.get("PASSWORD_WIZARD"));
        page.navigate(URL_WIZARD);
    }

    @Given("the user is on the SD Wizard")
    public void theUserIsOnTheSDWizard() {
        page.navigate(URL_WIZARD);
    }

    @When("the user clicks on get started button")
    public void theUserClicksOnGetStartedButton() {
        page.getByText(GET_STARTED_BUTTON).click();
    }

    @Then("upload widget should be visible")
    public void uploadWidgetShouldBeVisible() {
        assertThat(page.locator(WIDGET_CLASS)).isVisible();
        UiSetup.attachScreenshot();
    }

    @Then("the information select a SHACL shape or Upload should be displayed")
    public void theInformationSelectASHACLShapeOrUploadShouldBeDisplayed() {
        assertThat(page.getByText(SELECT_SHACL_TEXT)).isVisible();
        assertThat(page.locator(UPLOAD_TEXT)).isVisible();
    }

    @Then("the Select Ecosystem dropdown menu options are displayed")
    public void the_select_ecosystem_dropdown_menu_options_are_displayed() {
        page.locator(SELECT_ECOSYSTEM_DROPDOWN_MENU).hover();
    }

    @When("the user clicks the SIMPL option")
    public void theUserClicksTheSIMPLOption() {
        page.locator(DROPDOWN_SIMPL_OPTION).click();
    }

    @Given("the file selection dialog box is visible")
    public void the_file_selection_dialog_box_is_visible() {
        assertThat(page.locator(WIDGET_CLASS)).isVisible();
    }

    @When("the user clicks the Upload button from the widget")
    public void theUserClicksTheUploadButtonFromTheWidget() {
        page.locator(UPLOAD_TEXT).click();
    }

    @When("the user clicks the Upload button link on Select a SHACL or Upload format")
    public void theUserClicksTheUploadButtonLinkOnSelectASHACLOrUploadFormat() {
        page.locator(UPLOAD_BUTTON_WIDGET).click();
    }

    @When("the user clicks the Choose file button")
    public void theUserClicksTheChooseFileButton() {
        page.locator(CHOOSE_FILE_BUTTON).click();
    }

    @When("the user selects a .ttl file")
    public void theUserSelectsATtlFile() {
        String filePath = FILE_PATH;
        URL resource = getClass().getClassLoader().getResource(filePath);
        if (resource == null) {
            throw new RuntimeException("File not found: " + filePath);
        }
        try {
            Path fullPath = Paths.get(resource.toURI());
            page.setInputFiles(CHOOSE_FILE_BUTTON, fullPath);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Invalid URI syntax for file: " + filePath, e);
        }
    }

    @Then("the file name is visible in the upload widget")
    public void theFileNameIsVisibleInTheUploadWidget() {
        assertThat(page.locator(FILE_NAME_DISPLAYED)).isVisible();
    }

    @When("the user clicks the upload button from the Upload Shape")
    public void theUserClicksTheUploadButtonFromTheUploadShape() {
        page.locator(UPLOAD_BUTTON).click();
    }

    @Then("the Complete Person Form is displayed")
    public void the_complete_person_form_is_displayed() {
        assertThat(page.locator(COMPLETE_PERSON_FORM)).isVisible();
        UiSetup.attachScreenshot();
    }

    @When("the user fills in the Compliance Review Body Form")
    public void theUserFillsInTheComplianceReviewBodyForm() {
        page.locator(AGE_FIELD).fill(AGE);

        page.locator(NAME_FIELD).fill(NAME);

        page.locator(GENDER_FIELD).click();
        page.locator(MALE_OPTION).click();

        page.locator(BIRTH_DATE_FIELD).fill(BIRTH_DATE);
    }

    @Then("the Save button is enabled")
    public void theSaveButtonIsEnabled() {
        boolean isEnabled = page.locator(SAVE_BUTTON).isEnabled();
        if (!isEnabled) {
            throw new RuntimeException("Save button is not enabled");
        }
    }

    @Then("the user clicks the Save button")
    public void theUserClicksTheSaveButton() {
        Download download = page.waitForDownload(() -> {
            page.locator(SAVE_BUTTON).click();
        });

        UiSetup.attachScreenshot();
        wizardPage.setSuggestedFilename(download.suggestedFilename());
    }

    @Then("the JSON file is downloaded")
    public void theJSONFileIsDownloaded() {
        String suggestedFilename = wizardPage.getSuggestedFilename();

        if (!suggestedFilename.endsWith(".json")) {
            throw new AssertionError("Downloaded file is not a JSON file: " + suggestedFilename);
        }

        if (!suggestedFilename.contains("Person-instance")) {
            throw new AssertionError("Downloaded file name does not contain 'Person-instance': " + suggestedFilename);
        }
    }

    @Then("the wizard displays on Service Offering shapes these shapes")
    public void theWizardDisplaysOnServiceOfferingShapesTheseShapes() {
        if (!page.locator(SERVICE_OFFERING_SHAPES_SECTION).isVisible()) {
            throw new AssertionError("Section not visible");
        }
        UiSetup.attachScreenshot();
    }

    @Then("the user sees the Data Offering shape")
    public void theUserSeesTheDataOfferingShape() {
        assertElementIsVisibleByLocator(page.locator(DATA_OFFERING_SHAPE));
        if (!page.locator(DATA_OFFERING_SHAPE).isVisible()) {
            throw new AssertionError("Data Offering Shape is not visible");
        }
        UiSetup.attachScreenshot();
    }

    @Then("the user sees the Application Offering shapes")
    public void theUserSeesTheApplicationOfferingShapes() {
        if (!page.locator(APPLICATION_OFFERING_SHAPE).isVisible()) {
            throw new AssertionError("Application Offering Shape is not visible");
        }
        UiSetup.attachScreenshot();
    }

    @Then("the wizard displays Upload button on the right side")
    public void theWizardDisplaysUploadButtonOnTheRightSide() {
        if (!page.locator(UPLOAD_BUTTON_WIDGET).isVisible()) {
            throw new AssertionError(" The Upload Button is not visible");
        }
        UiSetup.attachScreenshot();
    }


    @Then("the wizard displays the Participant shapes section")
    public void theWizardDisplaysTheParticipantShapesSection() {
        if (!page.locator(PARTICIPANT_SHAPES_SECTION).isVisible()) {
            throw new AssertionError(" The Participant Shapes Section is not visible");
        }
        UiSetup.attachScreenshot();
    }

    @Then("the wizard displays the Service Offering shapes section")
    public void theWizardDisplaysTheServiceOfferingShapesSection() {
        if (!page.locator(SERVICE_OFFERING_SHAPES_SECTION).isVisible()) {
            throw new AssertionError(" The Service Offering Shapes Section is not visible");
        }
        UiSetup.attachScreenshot();
    }

    @Then("the wizard displays the Resource shapes section")
    public void theWizardDisplaysTheResourceShapesSection() {
        if (!page.locator(RESOURCES_SHAPES_SECTION).isVisible()) {
            throw new AssertionError(" The Resources Shapes Section is not visible");
        }
        UiSetup.attachScreenshot();
    }

    @When("the user selects the {string} ecosystem")
    public void theUserSelectsTheEcosystem(String ecosystem) {
        Locator selectEcosystem = page.locator(SELECT_ECOSYSTEM_DROPDOWN_MENU);
        selectEcosystem.hover();
        page.locator(ECOSYSTEM_DROPDOWN_CONTENTS).getByText(ecosystem).click();
        assertThat(selectEcosystem).hasText(ecosystem);
    }

    @When("the user clicks on the {string} service offering shape")
    public void theUserClicksOnTheServiceOfferingShape(String serviceOffering) {
        Utils.clickButtonByAriaText(serviceOffering);
    }

    @When("the user fills the Data Offering fields with the following information:")
    public void theUserFillsTheDataOfferingFieldsWithTheFollowingInformation(DataTable dataTable) {
        Map<String, String> details = dataTable.asMap(String.class, String.class);
        fillDataOfferingForm(details);
    }

    @When("the user fills the {string} fields with mandatory fields")
    @When("the user fills the {string} fields with mandatory fields and the following info:")
    public void theUserFillsTheFormFieldsWithMandatoryFields(String serviceOffering, @Nullable DataTable dataTable) {
        serviceOffering = serviceOffering.replaceAll("\\s+", "");
        String filePath = "src/main/resources/simplOpen/" + serviceOffering + "DefaultValues.json";
        Map<String, String> detailsMap = DocumentValidator.getMapFromJsonPath(filePath);

        if (dataTable != null) {
            Map<String, String> additionalDetails = dataTable.asMap(String.class, String.class);
            detailsMap.putAll(additionalDetails);
        }
        fillDataOfferingForm(detailsMap);
    }

    @Then("^the \"Save\" button is (enabled|not enabled)$")
    public void theSaveButtonIsEnabledOrNotEnabled(String isEnabled) {
        Locator saveButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save"));
        if ("enabled".equals(isEnabled)) {
            assertThat(saveButton).isEnabled();
        } else if ("not enabled".equals(isEnabled)) {
            assertThat(saveButton).isDisabled();
        } else {
            throw new IllegalArgumentException("Unknown button status: " + isEnabled);
        }
    }

    @Then("the user clicks the \"Save\" button")
    public void theUserClicksOnTheSaveButton() {
        Locator saveButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save"));
        assertElementIsVisibleByLocator(saveButton);
        theSaveButtonIsEnabledOrNotEnabled("enabled");
        Download download = page.waitForDownload(saveButton::click);
        download.saveAs(Paths.get("src/main/resources/" + download.suggestedFilename()));
    }

    @When("the user checks the following schemas have been added successfully in {string} document:")
    public void theUserChecksDocumentsHaveBeenAddedToTheDocumentSuccessfully(String serviceOffering, DataTable dataTable) {
        serviceOffering = serviceOffering.replaceAll("\\s+", "");
        String filePath = "src/main/resources/" + serviceOffering + "-instance.json";

        Map<String, String> schemasMap = dataTable.asMap(String.class, String.class);
        JsonObject jsonDocument = DocumentValidator.getJsonObjectFromFile(filePath);

        schemasMap.forEach((key, expectedValue) ->
                DocumentValidator.validateValueInJson(key, expectedValue, mapSchemaToJsonPath().get(key), jsonDocument));

        DocumentValidator.deleteDownloadedFile(filePath);
    }

    @When("the user clicks on the {string} dropdown")
    public void theUserClicksOnTheDropdown(String dropdownOpt) {
        switch (dropdownOpt) {
            case "ContractTemplateShape":
                page.getByText(CONTRACT_TEMPLATE_SHAPE_DROPDOWN).click();
                break;
            case "BillingSchemaShape":
                page.getByText(BILLING_SCHEMA_SHAPE_DROPDOWN).click();
                break;
            case "SlaAgreementsShape":
                page.getByText(SLA_AGREEMENTS_SHAPE_DROPDOWN).click();
                break;
            default:
                throw new IllegalArgumentException("Unknown schema: " + dropdownOpt);
        }
    }

    @Then("the user verifies the {string} schemas are available")
    public void theUserVerifiesTheSchemasAreAvailable(String dropdownOpt) {
        switch (dropdownOpt) {
            case "ContractTemplateShape":
                page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save")).scrollIntoViewIfNeeded();
                page.getByText(CONTRACT_TEMPLATE_DOCUMENT_PLACEHOLDER).click(new Locator.ClickOptions().setForce(true));
                assertElementIsVisibleByLocator(page.locator(SELECT_CONTRACT_DROPDOWN_OPTIONS));
                break;
            case "BillingSchemaShape":
                page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save")).scrollIntoViewIfNeeded();
                page.getByText(BILLING_SCHEMA_DOCUMENT_PLACEHOLDER).click(new Locator.ClickOptions().setForce(true));
                assertElementIsVisibleByLocator(page.locator(BILLING_SCHEMA_DROPDOWN_OPTIONS));
                break;
            case "SlaAgreementsShape":
                page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Save")).scrollIntoViewIfNeeded();
                page.getByText(SLA_AGREEMENTS_DOCUMENT_PLACEHOLDER).click(new Locator.ClickOptions().setForce(true));
                assertElementIsVisibleByLocator(page.locator(SLA_AGREEMENTS_DROPDOWN_OPTIONS));
                break;
            default:
                throw new IllegalArgumentException("Unknown schema: " + dropdownOpt);
        }
    }

    @Then("the hash value for the following documents is correctly calculated according to the algorithm stated in the {string} file:")
    public void theHashValueForTheFollowingDocumentsIsCorrectlyCalculatedAccordingToTheAlgorithmStatedInTheFile(String serviceOfferingFile, DataTable dataTable) throws IOException, NoSuchAlgorithmException {
        serviceOfferingFile = serviceOfferingFile.replaceAll("\\s+", "");
        String filePath = "src/main/resources/" + serviceOfferingFile + "-instance.json";
        Map<String, String> schemasMap = dataTable.asMap(String.class, String.class);
        JsonObject jsonFile = DocumentValidator.getJsonObjectFromFile(filePath);

        for (String document : schemasMap.keySet()) {
            String algorithmPath = getAlgorithmPathFromDocument(document);
            String algorithm = DocumentValidator.getJsonValueByPath(jsonFile, algorithmPath);
            String hashPath = getHashPathFromDocument(document);
            String expectedHash = DocumentValidator.getJsonValueByPath(jsonFile, hashPath);
            String documentUrl = DocumentValidator.getJsonValueByPath(jsonFile, getUrlPathFromDocument(document));
            DocumentValidator.validateFieldIsPresentInJson(algorithmPath, jsonFile);
            DocumentValidator.validateDocumentHashFromDocumentURL(documentUrl, algorithm, expectedHash);
        }

        DocumentValidator.deleteDownloadedFile(filePath);
    }

    private void fillDataOfferingForm(Map<String, String> details) {
        page = UiSetup.getPage();
        WizardPage wizardPage = new WizardPage(page);
        WizardPage.GeneralServicePropertiesShape generalServiceProperties =
                new WizardPage.GeneralServicePropertiesShape();
        WizardPage.DataPropertiesShape dataProperties = new WizardPage.DataPropertiesShape();
        WizardPage.ProviderInformationShape providerInformation = new WizardPage.ProviderInformationShape();
        WizardPage.OfferingPriceShape offeringPrice = new WizardPage.OfferingPriceShape();
        WizardPage.ServicePolicyShape servicePolicy = new WizardPage.ServicePolicyShape();
        buildDataOfferingFromDataTable(details, generalServiceProperties, dataProperties, providerInformation,
                offeringPrice, servicePolicy);
        wizardPage.fillGeneralServicePropertiesFields(generalServiceProperties);
        wizardPage.fillDataPropertiesFields(dataProperties);
        wizardPage.fillProviderInformationFields(providerInformation);
        wizardPage.fillOfferingPriceFields(offeringPrice);
        wizardPage.fillServicePolicyFields(servicePolicy);
        wizardPage.fillContractTemplate(details.get("Contract Template Document"));
        wizardPage.fillBillingSchema(details.get("Billing Schema Document"));
        wizardPage.fillSlaAgreements(details.get("SLA Agreements Document"));
    }

    private static void buildDataOfferingFromDataTable(Map<String, String> details,
                                                       WizardPage.GeneralServicePropertiesShape generalServiceProperties,
                                                       WizardPage.DataPropertiesShape dataProperties,
                                                       WizardPage.ProviderInformationShape providerInformation,
                                                       WizardPage.OfferingPriceShape offeringPrice,
                                                       WizardPage.ServicePolicyShape servicePolicy) {
        generalServiceProperties.setName(details.get("General Service Properties - Name"));
        generalServiceProperties.setDescription(details.get("General Service Properties - Description"));
        generalServiceProperties.setServiceAccessPoint(details.get("General Service Properties - Service Access " +
                "Point"));
        Optional.ofNullable(details.get("General Service Properties - Keywords")).ifPresent(generalServiceProperties::setKeywords);
        generalServiceProperties.setInLanguage(details.get("General Service Properties - In Language"));

        Optional.ofNullable(details.get("Data Properties - Produced By")).ifPresent(dataProperties::setProducedBy);
        dataProperties.setFormat(details.get("Data Properties - Format"));
        Optional.ofNullable(details.get("Data Properties - Related Datasets")).ifPresent(dataProperties::setRelatedDatasets);
        Optional.ofNullable(details.get("Data Properties - Target Users")).ifPresent(dataProperties::setTargetUsers);
        Optional.ofNullable(details.get("Data Properties - Data Quality")).ifPresent(dataProperties::setDataQuality);
        Optional.ofNullable(details.get("Data Properties - Encryption")).ifPresent(dataProperties::setEncryption);
        Optional.ofNullable(details.get("Data Properties - Anonymization")).ifPresent(dataProperties::setAnonymization);
        providerInformation.setProvidedBy(details.get("Provider Information - Provided By"));
        providerInformation.setContact(details.get("Provider Information - Contact"));
        providerInformation.setSignature(details.get("Provider Information - Signature"));
        offeringPrice.setLicense(details.get("Offering Price - License"));
        offeringPrice.setCurrency(details.get("Offering Price - Currency"));
        offeringPrice.setPrice(details.get("Offering Price - Price"));
        offeringPrice.setPriceType(details.get("Offering Price - Price Type"));
        servicePolicy.setAccessPolicy(details.get("Service Policy - Access Policy"));
        servicePolicy.setUsagePolicy(details.get("Service Policy - Usage Policy"));
        servicePolicy.setDataProtectionRegime(details.get("Service Policy - Data Protection Regime"));
    }

    private Map<String, String> mapSchemaToJsonPath() {
        Map<String, String> schemaToPathMap = new HashMap<>();
        schemaToPathMap.put("Contract Template Document", "simpl:contractTemplate.simpl:contractTemplateDocument");
        schemaToPathMap.put("Billing Schema Document", "simpl:billingSchema.simpl:billingSchemaDocument");
        schemaToPathMap.put("SLA Agreements Document", "simpl:slaAgreements.simpl:slaAgreementsDocument");
        return schemaToPathMap;
    }

    private static String getAlgorithmPathFromDocument(String document) {
        String algorithmPath;
        switch (document) {
            case "Contract Template Document":
                algorithmPath = "simpl:contractTemplate.simpl:contractTemplateHashAlg";
                break;
            case "Billing Schema Document":
                algorithmPath = "simpl:billingSchema.simpl:billingSchemaHashAlg";
                break;
            case "SLA Agreements Document":
                algorithmPath = "simpl:slaAgreements.simpl:slaAgreementsHashAlg";
                break;
            default:
                throw new IllegalArgumentException("Unknown document: " + document);
        }
        return algorithmPath;
    }

    private static String getHashPathFromDocument(String document) {
        String hashPath;
        switch (document) {
            case "Contract Template Document":
                hashPath = "simpl:contractTemplate.simpl:contractTemplateHashValue";
                break;
            case "Billing Schema Document":
                hashPath = "simpl:billingSchema.simpl:billingSchemaHashValue";
                break;
            case "SLA Agreements Document":
                hashPath = "simpl:slaAgreements.simpl:slaAgreementsHashValue";
                break;
            default:
                throw new IllegalArgumentException("Unknown document: " + document);
        }
        return hashPath;
    }

    private static String getUrlPathFromDocument(String document) {
        String urlPath;
        switch (document) {
            case "Contract Template Document":
                urlPath = "simpl:contractTemplate.simpl:contractTemplateURL";
                break;
            case "Billing Schema Document":
                urlPath = "simpl:billingSchema.simpl:billingSchemaURL";
                break;
            case "SLA Agreements Document":
                urlPath = "simpl:slaAgreements.simpl:slaAgreementsURL";
                break;
            default:
                throw new IllegalArgumentException("Unknown document: " + document);
        }
        return urlPath;
    }

}
