package stepDefinitions.ui.simplOpen;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.LoadState;
import framework.ui.helpers.UiSetup;
import framework.ui.pages.simplOpen.ParticipantPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Map;

import static configuration.ui.data.simplOpen.Participant.*;
import static framework.common.Assertions.*;
import static framework.ui.locators.simplOpen.Participant.*;

public class ParticipantSteps {

    private final Page page = UiSetup.getPage();
    private final ParticipantPage participantPage = new ParticipantPage();

    @Then("^the user (sees|does not see) the \"(.*)\" button$")
    public void theUserSeesOrDoesNotSeeTheButton(String buttonVisibility, String button) {
        if ("sees".equals(buttonVisibility)) {
            participantPage.verifyVisibilityOfButtonsInParticipantUtilityPage(true, button);
        } else if ("does not see".equals(buttonVisibility)) {
            participantPage.verifyVisibilityOfButtonsInParticipantUtilityPage(false, button);
        } else {
            throw new IllegalArgumentException("Unknown visibility option: " + buttonVisibility);
        }
    }

    @When("the user clicks on the {string} button")
    public void theUserClicksOnTheButton(String button) {
        if ("Echo".equals(button)) {
            page.getByText(ECHO_BUTTON_TEXT).click();
        } else {
            throw new IllegalArgumentException("Unknown button: " + button);
        }
        page.waitForLoadState(LoadState.NETWORKIDLE);
    }

    @Then("the Echo page is displayed")
    public void theEchoPageIsDisplayed() {
        page.waitForURL("**/" + HOST_ECHO);
        assertPageHeaderHasText(page, ECHO_PAGE_HEADER);
    }

    @Then("the following information about the user's identity token is displayed:")
    public void theFollowingInformationAboutTheUsersIdentityTokenIsDisplayed(DataTable dataTable) {
        ParticipantPage.UserInformation userInformation = new ParticipantPage.UserInformation();
        buildUserInformationFromDataTable(dataTable, userInformation);
        participantPage.verifyUserInformationInEchoPage(userInformation);
    }

    @Then("the connection status is {string}")
    public void theConnectionStatusIs(String status) {
        if ("CONNECTED".equals(status)) {
            assertElementIsPresentInCollection("Connection status:" + status,
                    page.locator(ECHO_PAGE_USER_INFORMATION_DIVS).allTextContents());
        } else {
            throw new IllegalArgumentException("Unknown connection status: " + status);
        }
    }

    @Then("the mTLS status is {string}")
    public void theMtlsStatusIs(String status) {
        if ("SECURED".equals(status)) {
            assertElementIsPresentInCollection("mTLS Status:" + status,
                    page.locator(ECHO_PAGE_USER_INFORMATION_DIVS).allTextContents());
        } else {
            throw new IllegalArgumentException("Unknown mTLS status: " + status);
        }
    }

    private static void buildUserInformationFromDataTable(DataTable dataTable, ParticipantPage.UserInformation userInformation) {
        Map<String, String> dataTableAsMap = dataTable.asMap(String.class, String.class);

        if (dataTableAsMap.containsKey("Username")) {
            userInformation.setUsername(dataTableAsMap.get("Username"));
        }

        if (dataTableAsMap.containsKey("Email Address")) {
            userInformation.setEmailAddress(dataTableAsMap.get("Email Address"));
        }

        if (dataTableAsMap.containsKey("Common name") && dataTableAsMap.get("Common name") != null) {
            userInformation.setCommonName(dataTableAsMap.get("Common name"));
        }

        if (dataTableAsMap.containsKey("Organisation") && dataTableAsMap.get("Organisation") != null) {
            userInformation.setOrganisation(dataTableAsMap.get("Organisation"));
        }

        if (dataTableAsMap.containsKey("Participant Type") && dataTableAsMap.get("Participant Type") != null) {
            userInformation.setParticipantType(dataTableAsMap.get("Participant Type"));
        }

        if (dataTableAsMap.containsKey("Assigned identity attributes") &&
                dataTableAsMap.get("Assigned identity attributes") != null) {
            userInformation.setAssignedIdentityAttributes(dataTableAsMap.get("Assigned identity attributes"));
        }
    }

}
