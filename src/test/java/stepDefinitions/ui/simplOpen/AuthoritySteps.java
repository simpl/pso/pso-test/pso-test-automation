package stepDefinitions.ui.simplOpen;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import com.microsoft.playwright.options.LoadState;
import framework.ui.helpers.CalendarWidget;
import framework.ui.helpers.UiSetup;
import framework.ui.helpers.Utils;
import framework.ui.pages.simplOpen.AuthorityPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static configuration.ui.data.simplOpen.Authority.*;
import static framework.common.Assertions.*;
import static framework.ui.helpers.Utils.clickButtonByLocator;
import static framework.ui.locators.simplOpen.Authority.*;
import static framework.ui.pages.simplOpen.AuthorityPage.*;
import static org.junit.Assert.*;

public class AuthoritySteps {
    public static final DateTimeFormatter DATE_CUCUMBER_INPUT = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final Page page = UiSetup.getPage();
    public static String NEW_USER = "";
    public static String NEW_USER_EMAIL = "";
    public static Boolean IS_PDF_SUBMITTED = false;
    private final IdentityAttribute identityAttribute = new IdentityAttribute();
    private final AuthorityPage authorityPage = new AuthorityPage();
    private final CommonSteps commonSteps = new CommonSteps();
    private static String participantDetailNameValue = "null";
    private static String participantDetailTypeValue = null;
    private static String participantDetailOnboardingValue = null;
    private static String datePasedToString = null;
    private static String getCodeValueToDelete = null;


    @Given("the user clicks the register for this dataspace button")
    public void the_user_clicks_the_register_for_this_dataspace_button() {
        page.locator(ONBOARDING_INFO_REGISTER_BUTTON_LOCATOR).click();
    }

    @Given("the applicant request user already created the request as {string}")
    public void the_user_fills_the_request_form(String participantType) {
//        NOTE: Once API is done, this should be changed to that instead of working with the UI
        commonSteps.the_user_navigates_to_the_page("Welcome to Dataspace Info");
        the_user_clicks_the_register_for_this_dataspace_button();
        the_user_enters_the_email_address();
        theUserInsertsTheOrganisation();
        the_user_selects_a_participant_type(participantType);
        theUserInsertsTheSurname();
        theUserInsertsTheName();
        theUserInsertsTheUsername();
        theUserInsertsThePassword();
        theUserInsertsHeConfirmPassword();
        the_user_clicks_the_create_credential_button();
        the_successful_message_is_displayed();
    }

    @Given("the applicant request user already submitted the PDF file")
    public void the_user_submits_the_PDF_file() {
//        NOTE: Once API is done, this should be changed to that instead of working with the UI
        commonSteps.the_user_navigates_to_the_page("Additional Request");
        commonSteps.theUserIsRedirectedToTheKeycloak("Governance Authority");
        theUserLogsInWithCreatedUser();
        theUserSelectsAPDFFile();
        the_user_clicks_SubmitApplicationRequestButton();
        the_user_clicks_SubmitPDFButton();
        theFileIsSuccessfullyUploaded();
        theUserClicksTheLogoutButton();
    }

    @Given("the user enters the email address")
    public void the_user_enters_the_email_address() {
        String randomNumber = UUID.randomUUID().toString().replace("-", "").substring(0, 5);
        String email = randomNumber + "@gmail.com";
        NEW_USER_EMAIL = email;
        page.locator(EMAIL_ADDRESS_LOCATOR).fill(email);
    }

    @Given("the user selects an {string} address")
    public void the_user_selects_the_email_address(String email) {
        page.locator(EMAIL_ADDRESS_LOCATOR).fill(email);
    }

    @Given("^the user verifies the create credentials button is (enabled|disabled)$")
    public void the_user_verifies_the_create_credential_button_is(String state) {
        boolean shouldBeEnabled = state.equalsIgnoreCase("enabled");
        boolean isEnabled = page.locator(CREATE_CREDENTIALS_BUTTON_LOCATOR).isEnabled();
        assertEquals(String.format("Expected 'Create Credentials' button to be %s, but it was %s.", state, isEnabled ? "enabled" : "disabled"), shouldBeEnabled, isEnabled);
    }

    @Given("^the applicant with (pdf submitted|pdf not submitted) is in the onboarding status page$")
    public void the_applicant_is_in_the_onboarding_status_page(String pdfIsSubmitted) {
        commonSteps.the_user_navigates_to_the_page("Onboarding Status");
        IS_PDF_SUBMITTED = pdfIsSubmitted.equals("pdf submitted");
    }

    @Given("the user sees the filter button in the onboarding requests table list")
    public void the_user_sees_the_filter_button() {
        assertElementIsVisibleByLocator(page.locator(FILTER_FORM_BUTTON_LOCATOR));
    }

    @When("the user selects a {string} participant type")
    public void the_user_selects_a_participant_type(String participantType) {
        int index = Arrays.asList(PARTICIPANT_TYPES).indexOf(participantType);
        page.locator(PARTICIPANT_TYPE_DROPDOMN_MENU_LOCATOR).click();
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR).nth(index));
        page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR).nth(index).click();
    }

    @When("the user clicks the create credential button")
    public void the_user_clicks_the_create_credential_button() {
        page.locator(CREATE_CREDENTIALS_BUTTON_LOCATOR).click();
    }

    @When("the user inserts the organisation")
    public void theUserInsertsTheOrganisation() {
        String organisationRandom = ORGANISATION_INPUT_VALUE + System.currentTimeMillis();
        page.locator(ORGANISATION_INPUT_LOCATOR).fill(organisationRandom);
    }

    @When("the user inserts the organisation {string}")
    public void theUserInsertsTheOrganisation(String organisationName) {
        page.locator(ORGANISATION_INPUT_LOCATOR).fill(organisationName);
    }

    @When("the user inserts the surname")
    public void theUserInsertsTheSurname() {
        String surnameRandom = SURNAME_INPUT_VALUE + System.currentTimeMillis();
        page.locator(SURNAME_INPUT_LOCATOR).fill(surnameRandom);
    }

    @When("the user inserts the name")
    public void theUserInsertsTheName() {
        String nameRandom = NAME_INPUT_VALUE + System.currentTimeMillis();
        page.locator(NAME_INPUT_LOCATOR).fill(nameRandom);
    }

    @When("the user inserts the username")
    public void theUserInsertsTheUsername() {
        NEW_USER = USERNAME_INPUT_VALUE + System.currentTimeMillis();
        page.locator(USERNAME_INPUT_LOCATOR).fill(NEW_USER);
    }

    @When("the user logs in keycloak with a recently created user")
    public void theUserLogsInWithCreatedUser() {
        Utils.LogInDetails logInDetails = new Utils.LogInDetails();
        logInDetails.setUsername(NEW_USER);
        logInDetails.setPassword(ONBOARDING_DATAPARTICIPANT_PASSWORD);
        commonSteps.keycloakLogin(logInDetails);
        Locator submitAppButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Submit application request"));
        assertElementIsVisibleByLocator(submitAppButton);
    }

    @When("the user selects a .pdf file")
    public void theUserSelectsAPDFFile() {
        String filePath = PDF_FILE_PATH;
        URL resource = getClass().getClassLoader().getResource(filePath);
        if (resource == null) {
            throw new RuntimeException("File not found: " + filePath);
        }
        try {
            Path fullPath = Paths.get(resource.toURI());
            page.setInputFiles(CHOOSE_FILE_BUTTON_LOCATOR, fullPath);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Invalid URI syntax for file: " + filePath, e);
        }
    }

    @When("the user inserts the password")
    public void theUserInsertsThePassword() {
        page.locator(PASSWORD_INPUT_LOCATOR).fill(PASSWORD_INPUT_VALUE);
    }

    @When("the user inserts the confirm password")
    public void theUserInsertsHeConfirmPassword() {
        page.locator(CONFIRM_PASSWORD_INPUT_LOCATOR).fill(PASSWORD_INPUT_VALUE);
    }

    @When("^the ?(accepted|rejected|) applicant logs in with valid credentials$")
    public void theApplicantLogsInWithValidCredentials(String applicantStatus) {
        applicantStatus = applicantStatus.trim().toLowerCase();
        boolean isAccepted = applicantStatus.equals("accepted");
        boolean isRejected = applicantStatus.equals("rejected");

        String userKey = authorityPage.determineApplicantUser(IS_PDF_SUBMITTED, isAccepted, isRejected);
        Utils.LogInDetails userAndPassword = Utils.getUserNameAndPassword(userKey);

        commonSteps.keycloakLogin(userAndPassword);
        assertElementIsVisibleByLocator(page.locator(ONBOARDING_STATUS_PANEL_LOCATOR));
    }

    @When("the user filters by {string} with value {string}")
    public void theUserFiltersByWithValue(String filter, String value) {
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON));
        page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON).click();
        List<Locator> dropdownOptions;
        switch (filter) {
            case "Participant Name", "Name" -> {
                page.getByText(PARTICIPANT_PAGE_FILTER_SELECT_COLUMN).click();
                dropdownOptions = page.locator(PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR).all();
                dropdownOptions.get(0).click();
                page.locator(PARTICIPANT_LIST_FILTER_INPUT_LOCATOR).fill(value);
            }
            case "Participant Type" -> {
                page.getByText(PARTICIPANT_PAGE_FILTER_SELECT_COLUMN).click();
                dropdownOptions = page.locator(PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR).all();
                dropdownOptions.get(1).click();
                authorityPage.verifyUserSelectOptionFromSecondFilter(value, page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR), "Participant list");
            }
            case "Code" -> {
                page.getByText(PARTICIPANT_PAGE_FILTER_SELECT_COLUMN).click();
                dropdownOptions = page.locator(PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR).all();
                dropdownOptions.get(1).click();
                page.locator(PARTICIPANT_LIST_FILTER_INPUT_LOCATOR).fill(value);
            }
            case "Email" -> {
                page.getByText(PARTICIPANT_PAGE_FILTER_SELECT_COLUMN).click();
                dropdownOptions = page.locator(PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR).all();
                dropdownOptions.get(2).click();
                page.locator(PARTICIPANT_LIST_FILTER_INPUT_LOCATOR).fill(value);
            }
            case "Last Update Date" -> {
                page.getByText(PARTICIPANT_PAGE_FILTER_SELECT_COLUMN).click();
                dropdownOptions = page.locator(PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR).all();
                dropdownOptions.get(2).click();
                String[] range = value.split("-");
                LocalDate startDate = LocalDate.parse(range[0].strip(), DATE_CUCUMBER_INPUT);
                LocalDate endDate = LocalDate.parse(range[1].strip(), DATE_CUCUMBER_INPUT);
                CalendarWidget.selectDateRange(page, startDate, endDate);
            }
            case "status" -> {
                authorityPage.verifyUserSelectOptionFromFirstFilter(filter, page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR));
                authorityPage.verifyUserSelectOptionFromSecondFilter(value, page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR), "Request list");
            }
            case "email" -> {
                authorityPage.verifyUserSelectOptionFromFirstFilter(filter, page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR));
                page.locator(PARTICIPANT_LIST_FILTER_INPUT_LOCATOR).fill(value);
            }
            case "Onboarding Date" -> {
                page.getByText(PARTICIPANT_PAGE_FILTER_SELECT_COLUMN).click();
                dropdownOptions = page.locator(PARTICIPANT_LIST_FILTER_DROPDOWN_OPTIONS_LOCATOR).all();
                dropdownOptions.get(2).click();
                LocalDate today = LocalDate.now();
                DateTimeFormatter formatter = CalendarWidget.getDateFormat();
                page.locator(PARTICIPANT_LIST_SECOND_FILTER_LOCATOR).fill(today.format(formatter));
                datePasedToString = authorityPage.parseDate(today.format(formatter));
            }
            default -> throw new IllegalArgumentException("Unknown filter: " + filter);
        }
        page.getByText(PARTICIPANT_PAGE_APPLY_FILTER_BUTTON).click();
    }

    @When("the user filters by email already created user")
    public void theUserFiltersByEmailWithCreatedUser() {
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON));
        page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON).click();
        authorityPage.verifyUserSelectOptionFromFirstFilter("email", page.locator(PARTICIPANT_TYPE_OPTIONS_LOCATOR));
        page.locator(PARTICIPANT_LIST_FILTER_INPUT_LOCATOR).fill(NEW_USER_EMAIL);
        page.getByText(PARTICIPANT_PAGE_APPLY_FILTER_BUTTON).click();
    }

    @When("the user sorts by {string} {string}")
    public void the_user_sorts_the_values_of_a_column_table_ascending_or_descending(String sortDirection, String columnToSort) throws InterruptedException {
        authorityPage.sortsTheValuesOfAColumnTable(sortDirection, columnToSort);
        Thread.sleep(750);
    }

    @When("the dataspace governance user clicks on the {string} button")
    public void the_user_clicks_on_reject_or_approve_btn(String buttonValue) {
        Locator button;
        switch (buttonValue.trim()) {
            case "Approve":
            case "Reject":
                button = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName(buttonValue.trim()));
                break;
            default:
                throw new IllegalArgumentException("Invalid button value: " + buttonValue);
        }
        assertElementIsVisibleByLocator(button);
        button.click();
        page.waitForLoadState(LoadState.NETWORKIDLE);
    }

    @When("^the user (submits|cancels) the form$")
    public void theUserHandlesTheForm(String action) {
        String locator = action.equals("submits") ? IDENTITY_ATTRIBUTE_CREATION_SAVE_BUTTON_LOCATOR : IDENTITY_ATTRIBUTE_CREATION_CANCEL_BUTTON_LOCATOR;
        clickButtonByLocator(locator);
    }

    @When("the user is in the Identity Attributes Creation page")
    public void theUserIsInTheIdentityAttributesCreationPage() {
        assertElementIsVisibleByLocator(page.getByRole(AriaRole.HEADING));
        assertThat(page.getByRole(AriaRole.HEADING)).hasText(NEW_ATTRIBUTE_HEADER);
    }

    @When("the user fills the Identity Attributes Creation form with the following data:")
    public void theUserFillsTheIdentityAttributesCreationFormWithTheFollowingData(DataTable dataTable) {
        Map<String, String> data = dataTable.asMap(String.class, String.class);

        String name = data.get("name") + "_" + System.currentTimeMillis();
        String code = data.get("code") + "_" + System.currentTimeMillis();
        boolean assignable = Boolean.parseBoolean(data.get("assignable to roles"));

        identityAttribute.setCode(code);
        identityAttribute.setName(name);
        identityAttribute.setAssignableToTier1(assignable);

        authorityPage.fillIdentityAttributeForm(name, code, assignable);
    }

    @When("the user clicks on the Edit button")
    public void theUserClicksOnTheEditButton() {
        clickButtonByLocator(IDENTITY_ATTRIBUTE_EDIT_BUTTON_LOCATOR);
    }

    @When("the user updates the Identity Attribute with the following data:")
    public void theUserUpdatesTheIdentityAttributeWithTheFollowingData(DataTable dataTable) {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        authorityPage.fillIdentityAttributeForm(data.get("Name"), data.get("Code"), Boolean.parseBoolean((data.get("Assignable to Tier 1"))));
    }

    @When("the user reverts the changes done to the Identity Attribute")
    public void theUserRevertsTheChangesDoneToTheIdentityAttribute() {
        clickButtonByLocator(IDENTITY_ATTRIBUTE_EDIT_BUTTON_LOCATOR);
        authorityPage.fillIdentityAttributeForm(identityAttribute.getName(), identityAttribute.getCode(), identityAttribute.getAssignableToTier1());
        clickButtonByLocator(IDENTITY_ATTRIBUTE_CREATION_SAVE_BUTTON_LOCATOR);
        Locator updateAlert = page.getByRole(AriaRole.ALERT);
        assertElementIsVisibleByLocator(updateAlert);
        assertThat(updateAlert).hasText(SUCCESSFUL_IDENTITY_ATTRIBUTE_EDITION_ALERT_MESSAGE);
    }

    @When("the user selects {int} items per page from the dropdown list")
    public void the_user_selects_number_value_by_page_items_per_page_from_the_dropdown_list(Integer numberValueByPage) throws InterruptedException {
        authorityPage.verifyThePaginationElementsAreVisible();
        authorityPage.itemsPerPageSelectionInTableDropdown(numberValueByPage);
        Thread.sleep(750);
    }

    @When("the access denied message is displayed")
    public void theAccessDeniedMessageIsDisplayed() {
        assertElementIsVisibleByLocator(page.getByText(ACCESS_DENIED));
    }


    @When("the user clicks on the Create Identity Attribute button")
    public void theUserClicksOnTheCreateIdentityAttributeButton() {
        clickButtonByLocator(NEW_ATTRIBUTE_BUTTON_LOCATOR);
    }

    @When("the user presses on the delete button")
    public void theUserClicksOnTheDeleteIdentityAttributeButton() {
        page.locator(IDENTITY_ATTRIBUTE_LIST_DELETE_COLUMN_LOCATOR).first().click();
    }

    @When("the user reads the Participant List data from the first row")
    public void TheUserReadsTheParticipantListDataFromTheFirstRow() {
        participantDetailNameValue = page.locator(PARTICIPANT_DETAILS_VALUE_ROW_lOCATOR).first().textContent();
        participantDetailTypeValue = page.locator(PARTICIPANT_DETAILS_VALUE_ROW_lOCATOR).nth(1).textContent();
        participantDetailOnboardingValue = page.locator(PARTICIPANT_DETAILS_VALUE_ROW_lOCATOR).nth(2).textContent();
        participantDetailOnboardingValue = authorityPage.parseDate(participantDetailOnboardingValue);
    }

    @When("the users clicks on the remove button under the Action column")
    public void theUsersClicksOnTheRemoveButtonUnderTheActionColumn() {
        getCodeValueToDelete = "Confirm deletion of " + page.locator((PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_CODE_COLUMN_LOCATOR)).innerText();
        page.locator(IDENTITY_ATTRIBUTE_REMOVE_BUTTON_LOCATOR).click();
    }

    @When("the system prompts the user to confirm the removal action")
    public void theSystemPromptsTheUserToConfirmTheRemovalAction() {
        String getCodeValueToDeleteFromDialog = page.locator(IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_MESSAGE_LOCATOR).innerText();
        assertEquals(getCodeValueToDelete, getCodeValueToDeleteFromDialog);
    }

    @When("^the user (confirms|cancels) the removal action$")
    public void theUserConfirmsTheRemovalAction(String action) {
        String locator = action.equals("confirms")
                ? IDENTITY_ATTRIBUTE_DELETE_BUTTON_LOCATOR
                : IDENTITY_ATTRIBUTE_DELETE_CANCEL_BUTTON_LOCATOR;
        clickButtonByLocator(locator);
    }

    @When("a success message is displayed")
    public void aSuccessMessageIsDisplayed() {
        Locator successMessage = page.getByRole(AriaRole.ALERT);
        assertElementIsVisibleByLocator(successMessage);
        assertThat(successMessage).hasText(SUCCESSFUL_IDENTITY_ATTRIBUTE_DELETION_ALERT_MESSAGE);
    }


    @Then("the {string} is updated with a maximum of {int} results")
    public void theRequestListIsUpdatedShowingAMaximumNumberOfResultsPerPage(String listType, Integer numberValueByPage) {
        assertElementIsVisibleByLocator(page.locator(FILTER_TABLE));
        authorityPage.verifyNumberOfResultsDisplayedInTheList(listType, numberValueByPage);
    }

    @Then("the {string} for the data space is displayed")
    public void the_list_of_onboarded_participants_for_the_data_space_is_displayed(String name) {
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_PARTICIPANT_LIST_HEADER));
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON));
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_LIST_TABLE_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_lIST_HEADER_PARTICIPANT_NAME_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_lIST_HEADER_PARTICIPANT_TYPE_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_lIST_HEADER_ONBOARDING_DATE_LOCATOR));
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_ACTIONS_COLUMN));
    }

    @Then("^the application status summary page (shows|does not show) the status request is \"(.*)\" with a \"(.*)\" icon$")
    public void status_summary_page_shows_the_status(String isDiplayed, String status, String icon) {
        assertElementIsVisibleByLocator(page.locator(ONBOARDING_REQUESTS_PANEL_LOCATOR));
        switch (status) {
            case "Request submitted":
                if (!page.locator(GREEN_ICON_LOCATOR).first().isVisible()) {
                    throw new RuntimeException("the status did not match");
                }
                break;
            case "In Progress":
                if (isDiplayed.equals("does not show") && icon.equals("green")) {
                    assertElementIsVisibleByLocator(page.locator(GREY_ICON_LOCATOR_2));
                } else if (isDiplayed.equals("show") && icon.equals("green") && (IS_PDF_SUBMITTED)) {
                    if (!page.locator(GREEN_ICON_LOCATOR).nth(2).isVisible()) {
                        throw new RuntimeException("the status did not match");
                    }
                }
                break;
            case "Request accepted":
                if (isDiplayed.equals("does not show") && icon.equals("grey") && (!IS_PDF_SUBMITTED)) {
                    assertElementIsVisibleByLocator(page.locator(GREY_ICON_LOCATOR_2));
                } else if (isDiplayed.equals("show") && icon.equals("grey") && (IS_PDF_SUBMITTED)) {
                    assertElementIsVisibleByLocator(page.locator(GREY_ICON_LOCATOR_3));
                }
        }
    }

    @Then("the user clicks on the Submit Application Request button")
    public void the_user_clicks_SubmitApplicationRequestButton() {
        Utils.clickButtonByAriaText("Submit application request");
        assertElementIsVisibleByLocator(page.locator(SUBMIT_APPLICATION_POPUP_LOCATOR));
    }

    @Then("the user clicks on the Submit button")
    public void the_user_clicks_SubmitPDFButton() {
        Utils.clickButtonByAriaText("Submit");
    }

    @Then("the file is successfully uploaded")
    public void theFileIsSuccessfullyUploaded() {
        assertThat(page.getByRole(AriaRole.ALERT)).hasText(SUCCESSFUL_REQUEST_SENT_ALERT_MESSAGE);
    }

    @Then("the applicant dataspace participant user can open the request status details dialog pressing See request details button")
    public void the_user_can_see_the_request_status_details_dialog() {
        assertThat(page.locator(SEE_REQUEST_DETAILS_BUTTON_LOCATOR)).isEnabled();
        page.locator(SEE_REQUEST_DETAILS_BUTTON_LOCATOR).click();
        assertElementIsVisibleByLocator(page.locator(REQUEST_STATUS_DETAILS_DIALOG_LOCATOR));
        page.locator(REQUEST_STATUS_DETAILS_DIALOG_LOCATOR).click();
    }

    @Then("the applicant dataspace participant clicks on the \"Download your Credentials\" button")
    public void the_applicant_participant_clicks_or_not_on_the_download_your_credentialsButton() {
        Locator downloadYourCredentialsButton = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("download"));
        assertElementIsVisibleByLocator(downloadYourCredentialsButton);
        assertThat(downloadYourCredentialsButton).isEnabled();
        page.waitForDownload(downloadYourCredentialsButton::click);
    }

    @Then("^the Download Your Credentials button (is visible|is not visible)$")
    public void the_user_does_not_see_the_download_your_credentials_btn(String isTheElementVisible) {
        authorityPage.verifyDownloadCredentialsButtonIsVisibleOrNot(isTheElementVisible);
    }

    @Then("the onboarding requests list shows {string} matching {string}")
    public void the_request_list_display_the_request_status(String filter, String value) {
        assertElementIsVisibleByLocator(page.locator(FILTER_TABLE));
        switch (filter) {
            case "status":
                authorityPage.verifyInRequestTableListDisplayTheFiltersValueSelected(filter, value, page.locator(REQUEST_STATUS_COLUMN_FROM_REQUEST_LIST_TABLE_LOCATOR));
                break;
            case "email":
                authorityPage.verifyInRequestTableListDisplayTheFiltersValueSelected(filter, value, page.locator(REQUEST_EMAIL_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR));
                break;
            default:
                throw new IllegalArgumentException("Unknown filter: ".concat(filter));
        }
    }

    @Then("the user sees an error message should be displayed")
    public void theUserSeesAnErrorMessageShouldBeDisplayed() {
        assert (page.locator(ERROR_MESSAGE_LOCATOR).textContent()).contains(ERROR_MESSAGE);
    }

    @Then("the user verifies the participants information on the Participant Details page")
    public void theParticipantDetailsPageIsDisplayed() {
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_HEADER_LOCATOR));
    }

    @Then("the Participant List page is displayed")
    public void theParticipantListPageIsDisplayed() {
        assertThat(page.getByRole(AriaRole.HEADING)).hasText(PARTICIPANT_LIST_PAGE_HEADER);
    }

    @Then("the following participant details are displayed:")
    public void theFollowingParticipantDetailsAreDisplayed(DataTable dataTable) {
        Map<String, String> map = dataTable.asMap(String.class, String.class);
        HashMap<String, String> details = new HashMap<>(map);
        String identifier = details.get("Identifier");
        String onboardingDate = details.get("Onboarding Date");
        String credentialExpirationDate = details.get("Participant Credentials Expiration Date");

        if ("USER_IDENTIFIER".equals(identifier)) {
            details.put("Identifier", USER_PSO_SIMPL_3049_IDENTIFIER);
        }
        if ("USER_ONBOARDING_DATE".equals(onboardingDate)) {
            details.put("Onboarding Date", USER_PSO_SIMPL_3049_ONBOARDING_DATE);
        }
        if ("USER_CREDENTIALS_EXPIRATION_DATE".equals(credentialExpirationDate)) {
            details.put("Participant Credentials Expiration Date", USER_PSO_SIMPL_3049_CREDENTIALS_EXPIRATION_DATE);
        }

        List<String> expectedValues = new ArrayList<>(details.values());
        List<String> realValues = page.locator(PARTICIPANT_DETAILS_COLUMNS_LOCATOR).allTextContents();
        realValues.replaceAll(String::strip);
        assertEqualCollections(expectedValues, realValues);
    }

    @Then("the list of Identity Attributes for the participant is displayed")
    public void theListOfIdentityAttributesForTheParticipantIsDisplayed() {
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_HEADER_LOCATOR));
        List<String> tableHeaders = page.locator(PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_HEADER_COLUMNS_LOCATOR).allTextContents();
        List<String> expectedHeaders = List.of(PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_NAME_COLUMN, PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_CODE_COLUMN, PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_ASSIGNABLE_COLUMN, PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_ENABLED_COLUMN, PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_CREATION_DATE_COLUMN, PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_ACTIONS, PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_LAST_CHANGE_DATE_COLUMN);
        tableHeaders.replaceAll(String::strip);
        assertEqualCollections(expectedHeaders, tableHeaders);
    }

    @Then("the identity attributes shown have {string} matching {string}")
    public void theIdentityAttributesShownHaveFilterMatchingValue(String filter, String value) {
        assertElementIsVisibleByLocator(page.locator(PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_TABLE_LOCATOR));
        switch (filter) {
            case "Name":
                Locator nameResults = page.locator(PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_NAME_COLUMN_LOCATOR);
                if (nameResults.first().isVisible()) {
                    nameResults.all().forEach(row -> assertThat(row).containsText(value));
                } else {
                    throw new AssertionError("The chosen participant does not have identity attributes assigned.");
                }
                break;
            case "Code":
                Locator codeResults = page.locator(PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_CODE_COLUMN_LOCATOR);
                if (codeResults.first().isVisible()) {
                    codeResults.all().forEach(row -> assertThat(row).containsText(value));
                } else {
                    throw new AssertionError("The chosen participant does not have identity attributes assigned.");
                }
                break;
            case "Last Update Date":
                Locator lastUpdateDateResults = page.locator(PARTICIPANT_DETAILS_IDENTITY_ATTRIBUTES_LAST_UPDATE_DATE_COLUMN_LOCATOR);
                if (lastUpdateDateResults.first().isVisible()) {
                    List<String> actualDates = lastUpdateDateResults.allTextContents();
                    String[] range = value.split("-");
                    LocalDate startDate = LocalDate.parse(range[0].strip(), DATE_CUCUMBER_INPUT);
                    LocalDate endDate = LocalDate.parse(range[1].strip(), DATE_CUCUMBER_INPUT);
                    actualDates.forEach(row -> assertDateIsInRange(startDate, endDate, LocalDate.parse(row.strip(), DateTimeFormatter.ofPattern("LLL d, yyyy", Locale.ENGLISH))));
                } else {
                    throw new AssertionError("The chosen participant does not have identity attributes assigned.");
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown filter: " + filter);
        }
    }

    @Then("the system prompts the user to confirm the {string} action")
    public void the_system_prompts_the_user_to_confirm_the_action(String action) throws InterruptedException {
        assertElementIsVisibleByLocator(page.locator(CONFIRM_DETAILS_DIALOG));
        assertElementIsVisibleByLocator(page.locator(TITLE_DIALOG_LOCATOR));
        assertElementIsVisibleByLocator(page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Cancel")));
        switch (action) {
            case "Reject Request" -> {
                assertElementIsVisibleByLocator(page.locator(REJECT_REQUEST_TYPE_REASON_FIELD_LOCATOR));
                assertElementIsVisibleByLocator(page.locator(MESSAGE_WITHIN_REJECT_DIALOGUE_LOCATOR));
            }
            case "Confirm Approval" ->
                    assertElementIsVisibleByLocator(page.locator(MESSAGE_WITHIN_APPROVAL_DIALOGUE_LOCATOR));
            default -> throw new IllegalArgumentException("Unsupported action: " + action);
        }
        authorityPage.theUserProcessActionValue(action);
    }

    @Then("the success message is displayed")
    public void theSuccessMessageIsDisplayed() {
        assertElementIsVisibleByLocator(page.getByRole(AriaRole.ALERT));
        assertThat(page.getByRole(AriaRole.ALERT)).hasText(SUCCESSFUL_IDENTITY_ATTRIBUTE_CREATION_ALERT_MESSAGE);
    }

    @Then("the onboarding requests list shows a message {string}")
    public void the_request_list_shows_a_message(String messageValue) {
        assertElementIsVisibleByLocator(page.locator(EMPTY_LIST_MESSAGE_LOCATOR));
        String msgValueShown = (page.locator(EMPTY_LIST_MESSAGE_LOCATOR)).innerText();
        if (!messageValue.contains(msgValueShown.trim())) {
            throw new RuntimeException("the message did not match");
        }
    }

    @Then("the onboarding requests list is displayed")
    public void the_request_list_is_displayed() {
        assertElementIsVisibleByLocator(page.getByText(REQUESTS_LIST_TITLE));
        assertElementIsVisibleByLocator(page.getByText(REQUESTS_LIST_TABLE_REQUEST_STATUS_COLUMN));
        assertElementIsVisibleByLocator(page.getByText(REQUESTS_LIST_TABLE_REQUESTER_EMAIL_COLUMN));
        assertElementIsVisibleByLocator(page.getByText(REQUESTS_LIST_TABLE_REQUESTER_EMAIL_COLUMN));
        assertElementIsVisibleByLocator(page.getByText(REQUESTS_LIST_TABLE_PARTICIPANT_TYPE_COLUMN));
        assertElementIsVisibleByLocator(page.getByText(REQUESTS_LIST_TABLE_LAST_CHANGE_DATE_COLUMN));
        authorityPage.verifyThePaginationElementsAreVisible();
    }

    @Then("the onboarding requests list shows the request with \"status\" {string} and \"email\" {string}")
    public void the_request_list_shows_result(String filterValue1, String filterValue2) {
        authorityPage.verifyInRequestTableListDisplayTheFiltersValueSelected("status", filterValue1, page.locator(REQUEST_STATUS_COLUMN_FROM_REQUEST_LIST_TABLE_LOCATOR));
        authorityPage.verifyInRequestTableListDisplayTheFiltersValueSelected("email", filterValue2, page.locator(REQUEST_EMAIL_COLUMN_FORM_REQUEST_LIST_TABLE_LOCATOR));
    }

    @Then("the Identity Attributes Details page is displayed")
    public void theIdentityAttributesDetailsPageIsDisplayed() {
        assertThat(page.getByRole(AriaRole.HEADING)).hasText(IDENTITY_ATTRIBUTES_DETAILS_HEADER);
    }

    @Then("the following identity attribute details are displayed:")
    public void theFollowingIdentityAttributeDetailsAreDisplayed(DataTable dataTable) {
        Map<String, String> map = dataTable.asMap(String.class, String.class);
        HashMap<String, String> details = new HashMap<>(map);

        if ("TODAY".equals(details.get("Creation date"))) {
            LocalDate today = LocalDate.now();
            DateTimeFormatter formatter = CalendarWidget.getDateFormat();
            details.put("Creation date", today.format(formatter));
        }

        if ("TODAY".equals(details.get("Last change date"))) {
            LocalDate today = LocalDate.now();
            DateTimeFormatter formatter = CalendarWidget.getDateFormat();
            details.put("Last change date", today.format(formatter));
        }

        if ("Assignable Test Identity Attribute".equals(details.get("Name")) || "Non-Assignable Test Identity Attribute".equals(details.get("Name"))) {
            details.put("Name", identityAttribute.getName());
        }

        if ("assignable_test_identity_attribute".equals(details.get("Code")) || "non_assignable_test_identity_attribute".equals(details.get("Code"))) {
            details.put("Code", identityAttribute.getCode());
        }

        if (identityAttribute.getCode() == null) {
            identityAttribute.setCode(details.get("Code"));
            identityAttribute.setName(details.get("Name"));
            identityAttribute.setAssignableToTier1(Boolean.parseBoolean(details.get("Assignable to Tier 1")));
        }

        Locator identityAttributeDetailsColumns = page.locator(IDENTITY_ATTRIBUTE_DETAILS_COLUMNS_LOCATOR);
        assertElementIsVisibleByLocator(identityAttributeDetailsColumns.first());
        List<String> realValues = identityAttributeDetailsColumns.allTextContents();
        realValues.replaceAll(String::strip);

        for (Map.Entry<String, String> entry : details.entrySet()) {
            String key = entry.getKey();
            String expectedValue = entry.getValue();
            if (key.equals("Code")) {
                assertTrue(String.format("Expected %s to contain %s, but was %s", key, expectedValue, realValues),
                        realValues.stream().anyMatch(value -> value.contains(expectedValue)));
            } else {
                assertElementIsPresentInCollection(expectedValue, realValues);
            }
        }
    }

    @Then("the Identity Attribute Creation success message is displayed")
    public void theIdentityAttributeCreationSuccessMessageIsDisplayed() {
        Locator alert = page.getByRole(AriaRole.ALERT);
        assertElementIsVisibleByLocator(alert);
        assertThat(alert).hasText(SUCCESSFUL_IDENTITY_ATTRIBUTE_CREATION_ALERT_MESSAGE);
        clickButtonByLocator(CLOSE_ALERT_MESSAGE);
    }

    @Then("the user is redirected to the Identity Attributes Details page")
    public void theUserIsRedirectedToTheIdentityAttributesManagementPage() {
        assertElementIsVisibleByLocator(page.getByText(IDENTITY_ATTRIBUTES_DETAILS_HEADER));
    }

    @Then("the user is redirected to the Onboarding Request Form page")
    public void theUserIsRedirectedToTheOnboardingRequestFormPage() {
        assertElementIsVisibleByLocator(page.locator(ONBOARDING_FORM_LOCATOR));
    }

    @Then("the user is redirected to the {string} page")
    public void theUserIsRedirectedToThePage(String pageName) {
        if ("Identity Attribute Edition".equals(pageName)) {
            assertThat(page.locator(IDENTITY_ATTRIBUTE_NAME_FIELD_LOCATOR)).isEnabled();
            assertThat(page.locator(IDENTITY_ATTRIBUTE_CODE_FIELD_LOCATOR)).isEnabled();
            assertThat(page.locator(IDENTITY_ATTRIBUTE_ASSIGNABLE_TO_TIER_1_CHECKBOX_LOCATOR)).isEnabled();
            assertThat(page.locator(IDENTITY_ATTRIBUTE_EDITION_ENABLED_CHECKBOX_LOCATOR)).isEnabled();
        } else {
            throw new IllegalArgumentException("Unknown page: " + pageName);
        }
    }

    @Then("the Identity Attribute Edition success message is displayed")
    public void theIdentityAttributeEditionSuccessMessageIsDisplayed() {
        Locator updateAlert = page.getByRole(AriaRole.ALERT);
        assertElementIsVisibleByLocator(updateAlert);
        assertThat(updateAlert).hasText(SUCCESSFUL_IDENTITY_ATTRIBUTE_EDITION_ALERT_MESSAGE);
        clickButtonByLocator(CLOSE_ALERT_MESSAGE);
    }

    @Then("the successful message is displayed")
    public void the_successful_message_is_displayed() {
        assertElementIsVisibleByLocator(page.locator(SUCCESSFUL_MESSAGE_LOCATOR));
    }

    @Then("the error message is displayed")
    public void the_error_message_is_displayed() {
        assertElementIsVisibleByLocator(page.getByText(ALERT_ERROR_CREATING_CREDENTIAL_LOCATOR));
    }

    @Then("the user can see the submission form button")
    public void the_user_can_see_submission_form_button() {
        assertElementIsVisibleByLocator(page.locator(SUBMISSION_FORM_BUTTON_LOCATOR));
    }

    @Then("the user can see the access credentials")
    public void the_user_can_see_the_credentials() {
        String usernameGenerated = page.locator(USER_GENEREATED_LOCATOR).textContent();
        String passwordGenerated = page.locator(PASSWORD_GENEREATED_LOCATOR).textContent();
        if (!usernameGenerated.contains(NEW_USER)) {
            throw new RuntimeException("the users did not match");
        }
        if (!passwordGenerated.contains(PASSWORD_INPUT_VALUE)) {
            throw new RuntimeException("the passwords did not match");
        }
    }

    @Then("the system displays {string} as the selected option")
    public void theSystemDisplaysConsumerAsTheSelectedOption(String participantType) {
        assertThat(page.locator(PARTICIPANT_TYPE_SELECTED_LOCATOR)).hasText(participantType);

    }

    @Then("the system displays {string} as the input value")
    public void theSystemDisplaysConsumerAsTheInputValue(String participantType) {
        Utils.checkInputFieldHasText(page, ORGANISATION_INPUT_LOCATOR, participantType);
    }

    @Then("the {string} is sorted successfully by {string} {string}")
    public void verify_the_dates_values_of_the_column_were_sorted(String nameSpace, String sortDirection, String columnToSort) {
        authorityPage.verifyColumnSorting(sortDirection, columnToSort);
    }

    @Then("the sorting button {string} is enabled")
    public void theSortingButtonIsEnabled(String button) {
        assertThat(page.getByText(button)).isEnabled();
    }

    @Then("^the user updates the (request list|participant list|identity attributes list) by trying to click the \"(Next page|Previous page)\" button$")
    public void theUserUpdatesTheRequestListTryingToClickTheButton(String list, String button) {
        authorityPage.clickNextOrPreviousPageButton(button, list, true);
    }

    @Then("the page displays a table with the following column headings:")
    public void the_page_displays_a_table_with_the_following_column_headings(List<String> expectedHeadings) {
        page.waitForSelector(COLUMN_HEADING_LOCATOR);
        List<String> actualHeadings = page.locator(COLUMN_HEADING_LOCATOR).allTextContents();
        actualHeadings.replaceAll(String::trim);

        for (String expectedHeading : expectedHeadings) {
            if (!actualHeadings.contains(expectedHeading)) {
                throw new RuntimeException(String.format("Expected heading '%s' not found in the table. Actual headings: %s", expectedHeading, actualHeadings));
            }
        }
    }

    @Then("the access is denied")
    public void theAccessIsDenied() {
        assertElementIsVisibleByLocator(page.getByText(ROLE_RESTRICTED));
    }

    @Then("the user clicks the Logout button")
    public void theUserClicksTheLogoutButton() {
        clickButtonByLocator(ACCOUNT_CIRCLE_ICON);
        clickButtonByLocator(LOGOUT_BUTTON);
    }

    @Then("the list displays {string} for each Identity Attribute in the {string} column")
    public void validate_column_value_for_each_identity_attribute(String columnValue, String columnName) {
        authorityPage.validateColumnValueForEachIdentityAttribute(columnName);
    }

    @Then("the dialog for the delete action is displayed successfully")
    public void theDialogIsDisplayedSuccessfully() {
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_TITLE_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_DELETE_DIALOG_MESSAGE_LOCATOR));
        assertElementIsVisibleByLocator(page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Delete")));
        assertElementIsVisibleByLocator(page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Cancel")));
    }

    @Then("the list of Identity Attributes is displayed")
    public void theListOfIdentityAttributesIsDisplayed() {
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_IDENTIFIER_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_CODE_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_ASSIGNABLE_TO_ROLE_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_ENABLED_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_IN_USE_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_CREATION_DATE_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_LAST_CHANGE_DATE_HEADER_LOCATOR));
        assertElementIsVisibleByLocator(page.locator(IDENTITY_ATTRIBUTE_LIST_ACTIONS_HEADER_LOCATOR));
    }

    @Then("the user resets the {string} with the value {string} applied successfully")
    public void the_user_resets_the_filter_applied_successfully(String filter, String valueFilter) {
        authorityPage.verifyFilterIsAppied(filter, valueFilter, datePasedToString);
        page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON).click();
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_RESET_FILTER_BUTTON));
        page.getByText(PARTICIPANT_PAGE_RESET_FILTER_BUTTON).click();
        assertElementIsNotVisibleByLocator(page.locator(FILTER_APPLIED_VALUE_LOCATOR));
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON));
    }

    @Then("the user verifies the {string} with value {string} and {string} with value {string} are applied")
    public void the_user_verifies_filters_applied_successfully(String filter1, String valueFilter1, String filter2, String valueFilter2) {
        authorityPage.verifyFiltersAreAppied(filter1, valueFilter1, filter2, valueFilter2);
    }

    @Then("the user resets the multiples filters applied successfully")
    public void the_user_resets_the_filters_applied_successfully() {
        page.getByText(PARTICIPANT_PAGE_FILTER_BUTTON).click();
        assertElementIsVisibleByLocator(page.getByText(PARTICIPANT_PAGE_RESET_FILTER_BUTTON));
        page.getByText(PARTICIPANT_PAGE_RESET_FILTER_BUTTON).click();
        assertElementIsNotVisibleByLocator(page.locator(FILTER_APPLIED_VALUE_LOCATOR));
    }

    @Then("the user verifies the following participant details match with the values obtained from the row")
    public void theUserVerifiesTheElementsInParticipantDetailsPage(DataTable dataTable) {
        authorityPage.verifyTheValuesObtainedMatchWithValueDisplayed(dataTable, participantDetailNameValue, participantDetailTypeValue, participantDetailOnboardingValue);
    }

    @Then("the system removes the Identity Attribute from the list")
    public void theSystemRemovesTheIdentityAttributeFromTheList() {
        assertElementIsVisibleByLocator(page.locator(EMPTY_LIST_MESSAGE_LOCATOR));
    }
}
