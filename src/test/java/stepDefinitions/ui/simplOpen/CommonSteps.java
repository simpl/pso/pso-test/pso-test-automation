package stepDefinitions.ui.simplOpen;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.LoadState;
import framework.ui.helpers.UiSetup;
import framework.ui.helpers.Utils;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static configuration.ui.data.simplOpen.Authority.*;
import static configuration.ui.data.simplOpen.Participant.*;
import static framework.ui.locators.simplOpen.Authority.*;

public class CommonSteps {

    private final Page page = UiSetup.getPage();
    private final String ERROR_MESSAGE = "Invalid username or password.";

    @When("the (dataspace governance authority )user navigates to the {string} page")
    public void the_user_navigates_to_the_page(String pageName) {
        getURLByPageName(pageName);
    }

    @Then("the user is redirected to the {string} keycloak")
    public void theUserIsRedirectedToTheKeycloak(String keycloak) {
        switch (keycloak) {
            case "Governance Authority":
                page.waitForURL(URL_KEYCLOAK_AUTHORITY + "/**");
                break;
            case "Consumer":
                page.waitForURL(URL_KEYCLOAK_CONSUMER + "/**");
                break;
            case "Data Provider":
                page.waitForURL(URL_KEYCLOAK_DATA_PROVIDER + "/**");
                break;
            default:
                throw new IllegalArgumentException("Unknown keycloak: " + keycloak);
        }
    }

    @When("the user logs in with a user with {string} role")
    public void the_user_logs_in_with_a_user_with_role(String role) {
        Utils.LogInDetails userAndPassword = Utils.getUserAndPasswordByRole(role);
        keycloakLogin(userAndPassword);
        page.waitForLoadState(LoadState.NETWORKIDLE);
    }

    @When("the user tries to log in with invalid login credentials")
    public void theUserTriesToLogInWithInvalidLoginCredentials() {
        Utils.LogInDetails invalidCredentials = Utils.getUserNameAndPassword("INVALID");
        keycloakLogin(invalidCredentials);
    }

    @When("the user clicks on the first result from {string}")
    public void theUserClicksOnTheFirstResult(String listName) {
        switch (listName) {
            case "Participant List":
                page.locator(PARTICIPANT_TYPE_COLUMN_FROM_PARTICIPANT_LIST_TABLE_LOCATOR).first().click();
                break;
            case "Request List":
                page.locator(REQUEST_STATUS_COLUMN_FROM_REQUEST_LIST_TABLE_LOCATOR).first().click();
                break;
            case "Identity Attributes List":
                page.locator(LIST_IDENTITY_ATTRIBUTE_ID_LOCATOR).first().click();
                break;
            default:
                throw new IllegalArgumentException("Unknown List name: ".concat(listName));
        }
    }

    @Then("the user sees an error message displayed")
    public void theUserSeesAnErrorMessageDisplayed() {
        assert (page.locator(ERROR_MESSAGE_LOCATOR).textContent()).contains(ERROR_MESSAGE);
    }

    public void keycloakLogin(Utils.LogInDetails userAndPassword) {
        page.locator(KEYCLOAK_USERNAME_INPUT_LOCATOR).fill(userAndPassword.getUsername());
        page.locator(KEYCLOAK_PASSWORD_INPUT_LOCATOR).fill(userAndPassword.getPassword());
        Locator signInButton = page.locator(KEYCLOAK_SIGN_IN_BUTTON_LOCATOR);
        if (signInButton.isEnabled()) {
            signInButton.click();
        } else {
            throw new RuntimeException("Keycloak's sign in button was not enabled.");
        }
    }

    private void getURLByPageName(String pageName) {
        switch (pageName) {
            case "Welcome to Dataspace Info":
                page.navigate(URL_AUTHORITY.concat(HOST_ONBOARDING).concat(HOST_APPLICATION));
                break;
            case "Participant Management":
                page.navigate(URL_AUTHORITY.concat(HOST_ONBOARDING).concat(HOST_ADMINISTRATION).concat("/").concat(HOST_MANAGEMENT_PARTICIPANT));
                break;
            case "Onboarding Status":
                page.navigate(URL_AUTHORITY.concat(HOST_ONBOARDING).concat(HOST_APPLICATION_ONBOARDING_STATUS));
                break;
            case "Additional Request":
                page.navigate(URL_AUTHORITY.concat(HOST_ONBOARDING).concat(HOST_ADDITIONAL_REQUEST));
                break;
            case "Dashboard Requests List":
                page.navigate(URL_AUTHORITY.concat(HOST_ONBOARDING).concat(HOST_ADMINISTRATION));
                break;
            case "Consumer Participant Utility":
                page.navigate(URL_CONSUMER);
                break;
            case "Data Provider Participant Utility":
                page.navigate(URL_DATA_PROVIDER);
                break;
            case "Identity Attributes":
                page.navigate(URL_AUTHORITY.concat(HOST_IDENTITY_ATTRIBUTE));
                break;
            case "Identity Attributes Creation":
                page.navigate(URL_AUTHORITY.concat(HOST_IDENTITY_ATTRIBUTE).concat(HOST_IDENTITY_ATTRIBUTE_CREATION));
                break;
            default:
                throw new IllegalArgumentException("Unknown page: " + pageName);
        }
    }

}
