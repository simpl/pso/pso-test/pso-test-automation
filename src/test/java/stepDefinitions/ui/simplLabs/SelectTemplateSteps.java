package stepDefinitions.ui.simplLabs;

import framework.ui.helpers.UiSetup;
import framework.ui.pages.simplLabs.SelectTemplatePage;
import framework.ui.pages.simplLabs.SetupDataSpacePage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static framework.ui.helpers.UiSetup.page;

public class SelectTemplateSteps {
    private final SelectTemplatePage selectTemplatePage = new SelectTemplatePage(page);
    private final SetupDataSpacePage setupDataSpacePage = new SetupDataSpacePage(page);


    @When("the user selects a template")
    public void theUserSelectsATemplate() {
        selectTemplatePage.selectFirstTemplate();
        selectTemplatePage.clickUseThisTemplateButton();
    }

    @When("clicks the button 'Next'")
    public void clicksTheButtonNext() {
        selectTemplatePage.clickButtonNext();
    }

    @Then("the system should open the page 'Set up Data Space'")
    public void thePageSetupDataSpaceOpened() {
        setupDataSpacePage.checkSetupDataSpacePageOpened();
        UiSetup.attachScreenshot();
    }

    @Then("the page 'Select a Template' should be opened")
    public void thePageSelectATemplateShouldBeOpened() {
        selectTemplatePage.checkThePageSelectTemplateOpened();
        UiSetup.attachScreenshot();
    }
}
