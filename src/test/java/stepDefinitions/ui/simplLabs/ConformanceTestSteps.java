package stepDefinitions.ui.simplLabs;

import framework.ui.pages.simplLabs.ConformanceTestPage;
import framework.ui.pages.simplLabs.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static configuration.ui.data.simplLabs.LoginConfig.*;
import static framework.ui.helpers.UiSetup.page;

public class ConformanceTestSteps {
    private final ConformanceTestPage conformanceTestPage = new ConformanceTestPage(page);
    private final LoginPage loginPage = new LoginPage(page);

    @Given("the user is on the Components page")
    public void theUserIsOnTheComponentsPage() {
        page.navigate(URL);
        loginPage.loginIntoApplication(USERNAME, PASSWORD);
        conformanceTestPage.clickConformanceMenuItem();
    }

    @When("the user opens a custom component")
    public void theUserOpensACustomComponent() {
        conformanceTestPage.openFirstComponentConformanceTestPage();
    }

    @When("the user clicks on a checkbox for a test case")
    public void theUserClicksOnACheckboxForATestCase() {
        conformanceTestPage.clickCheckboxForTestcase();
    }

    @When("the user clicks on the button 'Start conformance test'")
    public void theUserClicksOnTheButton() {
        conformanceTestPage.clickStartConformanceTestButton();
    }

    @Then("the system should open the page 'Conformance test'")
    public void theSystemShouldOpenThePage() {
        conformanceTestPage.checkConformanceTestPageOpened();
    }
}
