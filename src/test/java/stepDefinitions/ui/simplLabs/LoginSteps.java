package stepDefinitions.ui.simplLabs;

import framework.ui.pages.simplLabs.DataSpacesPage;
import framework.ui.pages.simplLabs.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static configuration.ui.data.simplLabs.LoginConfig.*;
import static framework.ui.helpers.UiSetup.page;

public class LoginSteps {
    private final LoginPage login = new LoginPage(page);
    private final DataSpacesPage dataSpacesPage = new DataSpacesPage(page);

    @Given("user is on the Keycloak login page")
    public void userIsOnTheKeycloakLoginPage() {
        page.navigate(URL);
    }

    @When("user logs in with username and password")
    public void userLogsInWithUsernameAndPassword() {
        login.loginIntoApplication(USERNAME, PASSWORD);
    }

    @Then("user should be logged in successfully")
    public void userShouldBeLoggedInSuccessfully() {
        dataSpacesPage.checkTheUserIsLoggedIn();
    }
}
