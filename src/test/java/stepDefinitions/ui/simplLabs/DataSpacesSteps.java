package stepDefinitions.ui.simplLabs;

import framework.ui.helpers.UiSetup;
import framework.ui.pages.simplLabs.DataSpacesPage;
import framework.ui.pages.simplLabs.LoginPage;
import framework.ui.pages.simplLabs.SetupDataSpacePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

import static configuration.ui.data.simplLabs.LoginConfig.*;
import static framework.ui.helpers.UiSetup.page;

public class DataSpacesSteps {

    private final DataSpacesPage dataSpacesPage = new DataSpacesPage(page);
    private final LoginPage login = new LoginPage(page);
    private final SetupDataSpacePage setupDataSpacePage = new SetupDataSpacePage(page);


    @Given("the user is on the 'Data spaces' page")
    public void theUserIsOnTheDataSpacesPage() {
        page.navigate(URL);
        login.loginIntoApplication(USERNAME, PASSWORD);
        dataSpacesPage.checkTheUserIsLoggedIn();
    }

    @When("the user selects the status filter {string}")
    public void theUserSelectsTheStatusFilter(String status) {
        dataSpacesPage.selectStatusFilter(status);
        UiSetup.attachScreenshot();
    }

    @When("the user clicks on the 'Log out' button next to the users email")
    public void theUserClicksOnTheButtonNextToTheUsersEmail() {
        dataSpacesPage.clickLogoutButton();
    }

    @When("the user clicks on 'Create Data Space' button")
    public void theUserClicksOnCreateDataSpaceButton() {
        dataSpacesPage.clickCreateDataSpaceButton();
    }

    @When("the user clicks on 'Interact' for a data space with status RUNNING")
    public void theUserClicksOnInteractButton() {
        dataSpacesPage.clickInteractButton();
    }

    @Then("the system should open the page 'Data Space architecture'")
    public void theSystemShouldOpenThePageDataSpaceArchitecture() {
        setupDataSpacePage.checkThePageDataSpaceArchitectureOpened();
        UiSetup.attachScreenshot();
    }

    @Then("the system should display data spaces with the status {string}")
    public void theSystemShouldDisplayDataSpacesWithTheStatus(String status) {
        dataSpacesPage.checkDataSpacesDisplayedWithStatus(status);
        UiSetup.attachScreenshot();
    }

    @Then("the login page is displayed")
    public void theLoginPageIsDisplayed() {
        login.checkTheLoginButtonIsVisible();
        UiSetup.attachScreenshot();
    }
}
