package stepDefinitions.ui.simplLabs;

import framework.ui.pages.simplLabs.DataSpacesPage;
import framework.ui.pages.simplLabs.LoginPage;
import framework.ui.pages.simplLabs.SetupDataSpacePage;
import com.microsoft.playwright.Page;
import framework.ui.helpers.UiSetup;
import framework.ui.pages.simplLabs.*;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;

import static configuration.ui.data.simplLabs.LoginConfig.*;

public class CreateDataSpaceSteps {
    private final Page page = UiSetup.getPage();
    private final LoginPage login = new LoginPage(page);
    private final DataSpacesPage dataSpacesPage = new DataSpacesPage(page);
    private final SelectTemplatePage selectTemplatePage = new SelectTemplatePage(page);
    private final SetupDataSpacePage setupDataSpacePage = new SetupDataSpacePage(page);
    private final DataSpaceDetailsPage dataSpaceDetailsPage = new DataSpaceDetailsPage(page);
    private final DataSpaceConditionsPage dataSpaceConditionsPage = new DataSpaceConditionsPage(page);

    @Given("the user is on the 'Conditions' page")
    public void theUserIsOnTheConditionsPage() {
        page.navigate(URL);
        login.loginIntoApplication(USERNAME, PASSWORD);
        dataSpacesPage.clickCreateDataSpaceButton();
        selectTemplatePage.clickButtonNext();
        setupDataSpacePage.clickButtonNext();
        dataSpaceDetailsPage.clickButtonNext();
    }

    @Given("the user is on the 'Data Space details' page")
    public void theUserIsOnTheDataSpaceDetailsPage() {
        page.navigate(URL);
        login.loginIntoApplication(USERNAME, PASSWORD);
        dataSpacesPage.clickCreateDataSpaceButton();
        selectTemplatePage.clickButtonNext();
        setupDataSpacePage.clickButtonNext();
    }

    @Given("the user is on the 'Set up Data Space' page")
    public void theUserIsOnTheSetupDataSpacePage() {
        page.navigate(URL);
        login.loginIntoApplication(USERNAME, PASSWORD);
        dataSpacesPage.clickCreateDataSpaceButton();
        selectTemplatePage.clickButtonNext();
    }

    @When("the user accepts not all conditions for selected components")
    public void theUserAcceptsNotAllConditionsForSelectedComponents() {
        dataSpaceConditionsPage.clickNotAllCheckboxes();
    }

    @When("the user accepts all conditions for selected components")
    public void theUserAcceptsAllConditionsForSelectedComponents() {
        dataSpaceConditionsPage.clickAcceptAllCheckbox();
    }

    @When("the user clicks the button 'Next'")
    public void theUserClicksTheButtonNext() {
        dataSpaceDetailsPage.clickButtonNext();
    }

    @When("the user clicks the 'Create data space' button")
    public void theUserClicksTheButtonCreateDataSpace() {
        dataSpaceConditionsPage.clickCreateDataSpaceButton();
    }

    @Then("the user should not be able to click the 'Create data space' button")
    public void theUserShouldNotBeAbleToClickTheButton() {
        dataSpaceConditionsPage.checkCreateDataSpaceButtonBlocked();
    }

    @Then("the page 'Conditions' should be opened")
    public void thePageConditionsOpened() {
        dataSpaceConditionsPage.checkDataSpaceConditionsPageOpened();
    }

    @Then("the system should display a confirmation page")
    public void theSystemShouldDisplayAConfirmationPage() {
        dataSpacesPage.checkConfirmationPageDisplayed();
        UiSetup.attachScreenshot();
    }

    @When("the user clicks 'Go back' button")
    public void theUserClicksGoBackButton() {
        dataSpacesPage.clickGoBackButton();
    }

    @When("the user clicks the 'Back' button")
    public void theUserClicksBackButton() {
        setupDataSpacePage.clickBackButton();
    }

    @When("the user clicks the 'Add Nodes' button")
    public void theUserClicksTheAddNodesButton() {
        setupDataSpacePage.clickButtonAddNodes();
    }

    @When("the user clicks the 'Add Nodes' button in 'Add nodes' modal")
    public void theUserClicksTheAddNodesButtonInAddNodesModal() {
        setupDataSpacePage.clickButtonAddNodesInAddNodesModal();
    }

    @When("the user selected {string} and {string} in 'Add nodes' modal")
    public void theUserSelectedNodesInAddNodesModal(String node1, String node2) {
        List<String> nodes = List.of(node1, node2);
        setupDataSpacePage.clickButtonAddNodes();
        setupDataSpacePage.clickSpecificNodes(nodes);
    }

    @Then("the user is allowed to accept all conditions to the data space")
    public void allowTheUserToAddConditionsToTheDataSpace() {
        dataSpaceConditionsPage.clickAcceptAllCheckbox();
    }

    @Then("the user navigates to the data spaces overview page")
    public void theUserNavigatesToTheDataSpacesOverviewPage() {
        dataSpacesPage.checkTheUserIsLoggedIn();
    }

    @Then("the button Next should be enabled")
    public void buttonNextShouldBeEnabled() {
        dataSpaceConditionsPage.checkButtonNextEnabled();
        UiSetup.attachScreenshot();
    }

    @Then("the system should display the newly created data space with the status 'Requested'")
    public void theSystemShouldDisplayTheNewlyCreatedDataSpaceWithTheStatus() {
        dataSpacesPage.checkDataSpacesDisplayedWithStatus("REQUESTED");
        UiSetup.attachScreenshot();
    }

    @Then("the selected nodes should appear in the list of nodes")
    public void theSelectedNodesShouldAppearInTheListOfNodes(List<String> nodes) {
        setupDataSpacePage.checkNodesListContainsSpecificNodes(nodes);
    }

    @Then("the user selects the following nodes from the available list:")
    public void theUserSelectsTheFollowingNodes(List<String> nodes) {
        setupDataSpacePage.clickSpecificNodes(nodes);
        UiSetup.attachScreenshot();
    }

    @Then("the following nodes should be displayed in the Selected nodes list:")
    public void theNodesAreDisplayedInTheSelectedNodesList(List<String> nodes) {
        setupDataSpacePage.checkSelectedNodesContainSpecificNodes(nodes);
    }

    @After(value = "@SIMPL-3693", order = 2)
    public void cleanUpDataSpace() {
        dataSpacesPage.userDeletesDataSpace();
        dataSpacesPage.checkDataSpacesDisplayedWithStatus("REQUESTED_DELETION");
        UiSetup.attachScreenshot();
        int expectedAmount = 1;
        dataSpacesPage.waitDataSpaceDeletion(expectedAmount);
    }
}
